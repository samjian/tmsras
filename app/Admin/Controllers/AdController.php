<?php

namespace App\Admin\Controllers;

use App\Models\Ad;
use App\Models\Platform;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class AdController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header("圖片輪播")
            ->description("列表")
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header(trans('admin.detail'))
            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
        ->header("圖片輪播")
        ->description("編輯")
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
        ->header("圖片輪播")
        ->description("建立")
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Ad);
        
        $grid->model()->orderBy("platform_id", "asc")->orderBy("seq", "asc");
        
        
        $grid->disableExport();
        
        $grid->actions(function ($actions) {
            $actions->disableView();
        });
        
        $grid->filter(function($filter){
            //$filter->expand();
            $filter->disableIdFilter();
            $filter->equal('platform.name',"通路名稱")->select(Platform::all()->pluck('name', 'name'));
            $filter->equal('enable', "是否啟用")->radio([ 1 => "啟用",0 =>"不啟用"]);
            $filter->between("created_at","建立日期區間")->date();
        });
        
        $grid->column('platform.name' ,"通路名稱")->sortable();
        $grid->enable('是否啟用')->bool();
        $grid->seq('順序');
        
        $grid->img('圖片')->image();
        $grid->url('外部連結')->link();
        $grid->created_at(trans('admin.created_at'));
        $grid->updated_at(trans('admin.updated_at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Ad::findOrFail($id));
       
       
        $show->platform_id('platform_id');
        $show->enable('enable');
        $show->url('url');
        $show->img('img')->image();
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Ad);
        
        $form->tools(function (Form\Tools $tools)  {
            $tools->disableView();
        });
        
        $form->footer(function ($footer) {
            $footer->disableViewCheck();
        });
      
        $form->select('platform_id', '通路')->options(Platform::all()->pluck('name', 'id'))->required();
     
     
        $form->url('url', 'URL');
        $form->image('img', '圖片') ->uniqueName()
        ->rules('mimes:jpg,jpeg,png,svg' ,[
            'mimes' => '檔案格式錯誤需為 jpg,jpeg,png,svg',
        ])->required();
        
        $form->number('seq', '順序(越小越前面)')->min(0)->value(0);
        $form->radio("enable",'是否啟用')->options([1 => "是", 0 => "否"])->value(1);

        return $form;
    }
}
