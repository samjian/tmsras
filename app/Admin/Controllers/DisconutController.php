<?php

namespace App\Admin\Controllers;

use App\Models\Disconut;
use App\Models\Platform;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class DisconutController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
        ->header("優惠碼")
        ->description("列表")
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header(trans('admin.detail'))
            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
        ->header("優惠碼")
        ->description("編輯")
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
        ->header("優惠碼")
        ->description("建立")
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Disconut);

        
        $grid->disableExport();
        
        $grid->actions(function ($actions) {
            $actions->disableView();
        });
            
        $grid->filter(function($filter){
            //$filter->expand();
            $filter->disableIdFilter();
            $filter->equal('platform.name',"通路名稱")->select(Platform::all()->pluck('name', 'name'));
           
            $filter->contains('code',"優惠碼");
            $filter->between("start_date","有效開始日期")->date();
            $filter->between("end_date","有效結束日期")->date();
        });
                
                
        $grid->column('platform.name' ,"通路名稱")->sortable();
        
         
        $grid->code('優惠碼');
        $grid->start_date('有效開始日期');
        $grid->end_date('有效結束日期');
        $grid->price('優惠價格');
        
        $grid->created_at(trans('admin.created_at'));
        $grid->updated_at(trans('admin.updated_at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Disconut::findOrFail($id));
   
        $show->support_id('support_id');
        $show->code('code');
        $show->start_date('start_date');
        $show->end_date('end_date');
        $show->price('price');
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Disconut);
        
        $form->tools(function (Form\Tools $tools)  {
            $tools->disableView();
        });
        
        $form->tools(function (Form\Tools $tools) {
            $tools->disableView();
        });
    
        $form->footer(function ($footer) {
            $footer->disableViewCheck();
        });
        
      
        $form->select('platform_id', '通路')->options(Platform::all()->pluck('name', 'id'))->required();
        $form->text('code', '優惠碼')->required();
        $form->dateRange('start_date' ,"end_date", '有效日期')->required();
        
        $form->currency('price', '優惠價格')->symbol('$')->digits(0);
        

        return $form;
    }
}
