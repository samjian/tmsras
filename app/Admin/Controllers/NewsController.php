<?php

namespace App\Admin\Controllers;

use App\Models\News;
use App\Models\Platform;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class NewsController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header("跑馬燈")
            ->description("列表")
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header(trans('admin.detail'))
            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
        ->header("跑馬燈")
        ->description("編輯")
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
        ->header("跑馬燈")
        ->description("建立")
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new News);
       
        
        $grid->disableExport();
        
        $grid->actions(function ($actions) {
            $actions->disableView();
        });
            
        $grid->filter(function($filter){
            //$filter->expand();
            $filter->disableIdFilter();
            $filter->equal('platform.name',"通路名稱")->select(Platform::all()->pluck('name', 'name'));
            
            $filter->contains('title',"標題");
            $filter->contains('content',"內容");
            
            $filter->between("created_at","建立日期區間")->date();
        });
        
           
        $grid->column('platform.name' ,"通路名稱")->sortable();
        $grid->title('標題');
         
        
        $grid->column('內容')->display(function () {
            return "開啟";
        })->modal('最新消息', function ($model) {
            return  $model -> content;
        });
        
        
        $grid->created_at(trans('admin.created_at'));
        $grid->updated_at(trans('admin.updated_at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(News::findOrFail($id));

        
        $show->platform_id('platform_id');
        $show->title('title');
        $show->content('content');
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new News);
        
        $form->tools(function (Form\Tools $tools)  {
            $tools->disableView();
        });
        
        $form->footer(function ($footer) {
            $footer->disableViewCheck();
        });
     
        $form->select('platform_id', '通路')->options(Platform::all()->pluck('name', 'id'))->required();
        $form->text('title', '標題')->required();
        $form->ckeditor('content', '內容');
        return $form;
    }
}
