<?php

namespace App\Admin\Controllers;

use App\Models\Order;
use App\Models\Platform;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class OrderController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header("訂單")
            ->description("列表")
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header(trans('admin.detail'))
            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header(trans('admin.edit'))
            ->description(trans('admin.description'))
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header(trans('admin.create'))
            ->description(trans('admin.description'))
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Order);
        $grid->disableCreateButton();
         
        
        $grid->model()->orderBy("created_at" , "desc");
        
        $grid->actions(function ($actions) {
            $actions->disableView();
            $actions->disableEdit();
        });
        
        
        $grid->filter(function($filter){
            $filter->expand();
            $filter->disableIdFilter();
            $filter->contains('no',"訂單編號");
            $filter->equal('platform.name',"通路名稱")->select(Platform::all()->pluck('name', 'name'));
            $filter->contains('name',"姓名");
            $filter->contains('phone',"連絡電話");
            $filter->contains('car_no',"車牌");
            $filter->contains('car_brand',"廠牌");
            $filter->contains('car_brand_name',"廠牌名稱");
            $filter->contains('car_type',"車型");
            
            $filter->equal('status', "狀態")->select(Order::STATUS);
            $filter->between("created_at","建立日期區間")->date();
        });
               
        
        $grid->export(function ($export) {
            
            $export->filename('訂單');
            
            $export->except(["img"]);
             
            $export->column('status', function ($value, $original) {
              
                return Order::STATUS[$original];
            });
        });
        
        $grid->no('訂單編號');
        $grid->column('platform.name' ,"通路名稱")->sortable();
        
        $grid->name('姓名');
        $grid->phone('連絡電話');
        $grid->car_no('車號');
        $grid->car_brand('廠牌');
        $grid->car_brand_name('廠牌名稱');
        $grid->car_type('車型');
        $grid->addr('地址');
     
        
        $grid->column("img",'照片')->display(function($img_path)  {
            if (!$img_path) return "尚無檔案";
            
           // $url = config("filesystems.disks.admin.url");
           // $arr = explode(".", $img_path);
            //$path = $arr[0].".".$arr[1];
            return  "
                <a href='$img_path' target='_blank'>
                <img src='$img_path' style='max-width:200px;max-height:200px' class='img img-thumbnail'>
                </a>
            ";
        });
        
        $grid->disconut_code('優惠序號');
        $grid->status('狀態')->using(Order::STATUS)->label([
            1 => 'warning',
            2 => 'info',
            3 => 'default',
            4 => 'success',
            5 => 'danger',
        ]);
     
        $grid->created_at(trans('admin.created_at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Order::findOrFail($id));

       
        $show->platform_id('platform_id');
        $show->name('name');
        $show->phone('phone');
        $show->car_no('car_no');
        $show->addr('addr');
        $show->status('status');
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Order);

        $form->display('ID');
        $form->text('platform_id', 'platform_id');
        $form->text('name', 'name');
        $form->text('phone', 'phone');
        $form->text('car_no', 'car_no');
        $form->text('addr', 'addr');
        $form->text('status', 'status');
        $form->display(trans('admin.created_at'));
        $form->display(trans('admin.updated_at'));

        return $form;
    }
}
