<?php

namespace App\Admin\Controllers;

use App\Models\Platform;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class PlatformController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header("合作通路")
            ->description("列表")
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header(trans('admin.detail'))
            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
        ->header("合作通路")
        ->description("編輯")
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
        ->header("合作通路")
        ->description("建立")
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Platform);
        $grid->disableExport();
        
        $grid->actions(function ($actions) {
            $actions->disableView();
            $actions->disableDelete();
        });
            
        $grid->filter(function($filter){
            //$filter->expand();
            $filter->disableIdFilter();
            $filter->contains('code',"通路代碼");
            $filter->contains('name',"通路名稱");
            $filter->contains('contact',"聯絡人");
            
            $filter->equal('enable', "是否啟用")->radio([ 1 => "啟用",0 =>"不啟用"]);
            $filter->between("created_at","建立日期區間")->date();
        });
            
        $grid->name('通路名稱')->sortable();
        $grid->code('通路代碼');
       
        $grid->contact('聯絡人');
        $grid->phone('聯絡電話');
        $grid->enable('是否啟用')->bool();
        
        
        $grid->created_at(trans('admin.created_at'));
        $grid->updated_at(trans('admin.updated_at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Platform::findOrFail($id));

        
        $show->name('name');
        $show->contact('contact');
        $show->enable('enable');
        $show->code('code');
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));

      
        return $show;
    }

    
    function rand(){
        
        $randArr = array();
        for($i = 0; $i < 6; $i++){
            if ($i <3) {
                $randArr[$i] = chr(rand(0, 25) + 65);
            } else {
                $randArr[$i] = rand(0, 9);
            }
        }
        
        $code = implode('', $randArr);
        
        while(Platform::where("code" , $code)->exists()) {
            $code =  $this->rand();
        }
            
        
        //打亂排序
        //shuffle($randArr);
        return $code;
    }
    
    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Platform);
      
        $form->tools(function (Form\Tools $tools)  {
            $tools->disableView();
            $tools->disableDelete();
        });
        
           
        
        $form->footer(function ($footer) {
            $footer->disableViewCheck();
        });
        
        $code = $this->rand();
        
        
        
        if ($form->isCreating()) {
            $form->text('code', "通路代碼")->required()
            ->creationRules(['required', "unique:platforms"],[
                'unique'   => '通路代碼已存在',
            ]);
        } else {
            $form->text('code', "通路代碼")->required()->readonly();
        }
        
        $form->text('name', '通路名稱')->required();
        $form->text('contact', '聯絡人')->required();
        $form->mobile("phone", '聯絡人電話')->options(['mask' => '9999-999-999']);;
        $form->radio("enable",'是否啟用')->options([1 => "是", 0 => "否"])->value(1);
        
       
            
        return $form;
    }
}
