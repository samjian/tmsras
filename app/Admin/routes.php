<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');
    
    $router->resource('/ad', 'AdController');
    $router->resource('/disconut', 'DisconutController');
    $router->resource('/news', 'NewsController');
    $router->resource('/order', 'OrderController');
    $router->resource('/pay', 'PayController');
    $router->resource('/platform', 'PlatformController');
    $router->resource('/rule', 'RuleController');
  
    
});
