<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Platform;
use App\Models\Order;
use Illuminate\Support\Facades\DB;
 

class PlatformController extends Controller
{
   
    public function callback(Request $request , $orderNo, $status){
        $order = Order::where("no", $orderNo)->first();
        
        if ($order == null) {
            echo json_encode([
                "return_code"=>0,
                "msg"=>"查無訂單資料",
            ],JSON_UNESCAPED_UNICODE);
            return;
        }
        
        $order->status = $status;
        
        $datetime = new \DateTime;
        
        $order->callback_time =  $datetime->format('Y-m-d H:i:s');
      
        $result = $order->save();
        
        echo json_encode([
            "return_code"=>$result ? 1:0,
            "msg"=>"訂單更新成功",
        ],JSON_UNESCAPED_UNICODE);
    }
    
    public function update(Request $request , $orderNo, $phone, $car_no, $brand_id, $brand_name , $vehicle_id,$vehicle_name){
        $order = Order::where("no", $orderNo)->first();
        
        if ($order == null) {
            echo json_encode([
                "return_code"=>0,
                "msg"=>"查無訂單資料",
            ],JSON_UNESCAPED_UNICODE);
            return;
        }
        
        $order->phone = $phone;
        $order->car_no = $car_no;
        
        $order->car_brand =$brand_id;
        $order->car_brand_name =  $brand_name;
        $order->car_type = $vehicle_id;
        $order->car_type_name = $vehicle_name;
        
        $result = $order->save();
         
        echo json_encode([
            "return_code"=>$result ? 1:0,
            "msg"=>"訂單更新成功",
        ],JSON_UNESCAPED_UNICODE);
    }
    
    public function toQuery(Request $request, $shortCode) {
       // DB::connection()->enableQueryLog();
        
        $order = Order::where("short_code",$shortCode)->first();
       
        if ($order ==null) {
            abort(403, '無訂單資料');
        }  
        
        $platform= $order->platform;
       
        
        //$log = DB::getQueryLog();
        
        
        
        $data = [
            "contact_tel"=>$order->phone,
            "plate_number"=>$order->car_no,
        ];
        
        /* for test
         $data = [
         "contact_tel"=>"0900000000",
         "plate_number"=>"AA-88"
         ];
         */
        
        $result= $this->api("Get_Status.asp", $data);
        
        /*
        $result=new \stdClass();
        $result->return_code ="0";
        $result->return_status =1;
        $result->driver_name="test";
        $result->service_car="HABC";
        $result->return_eta="08:00";
        */
        
        if ($result->return_code =="01") {
            abort(403, '01 無訂單資料');
        } else if ($result->return_code =="99") {
            abort(403, '99 無訂單資料');
        } 
         
        if(!isset($result->driver_location)) {
            $result->driver_location ="";
        }
        
        if(!isset($result->return_destination)) {
            $result->return_destination ="";
        }
        
        $this->setOrderStatus($result);
        
        if ( $result->return_status ==4 || $result->return_status ==5) {
            //檢查時間
            if ($order->callback_time ) {
                $callback_time =  strtotime( $order->callback_time);
                
                $now =  strtotime("now");
                
                $time = $now - $callback_time;
                
                if ($time >= 180) {
                    return view("waiting" , ["result"=> $result, "order"=>$order, "code" => $platform->code]);
                }
                
            }
        } else if ($result->return_code =="01" ||  $result->return_code=="99") {
            return view("waiting" , ["result"=> $result, "order"=>$order, "code" => $platform->code]);
        }
         
        return view("schedule" , ["result"=> $result, "order"=>$order, "code" => $platform->code]);
    }
    
    public function apiOrder(Request $request) {
          
        $platform = Platform::where("code" , $request->code)->first();
         
        if ($platform == null) {
            echo json_encode([
                "return_code"=> 0,
                "msg"=>"平台代碼錯誤",
                "url"=>"",
            ],JSON_UNESCAPED_UNICODE);
            return;
        }
        
        $no = $this->rand();
        
        $shortCode = $this->shortCode(6);
        
        while (true) {
            if (Platform::where("code" ,$shortCode)->exists()) {
                $shortCode = $this->shortCode(6);
            } else {
                break;
            }
        }
        
        $now = date("Ymd", time());
         
        $order = new Order;
        $order->platform_id = $platform->id;
        $order->name = $request->name;
        $order->no = $no;
        $order->phone = $request->phone;
        $order->short_code = $shortCode;
        
        if ($request->car_no_1) {
            $order->car_no = $request->car_no_1."-".$request->car_no_2;
        } else {
            $order->car_no="";
        }
        
        $order->addr = $request->addr;
        $order->status = $request->status;
        $order->disconut_code = "";
        $order->discount_price = 0;
        $order->car_brand = $request->car_brand;
        $order->car_brand_name =  $request->car_brand_name;
        $order->car_type = $request->car_type;
        $order->car_type_name =  $request->car_type_name;
        $order->result="api create order";
        
        $result =  $order->save();
       
        if ( $result ==1) {
            $url = env("SHORT_DOMAIN")."/q/".$shortCode;
           
            echo json_encode([
                "return_code"=>1,
                "msg"=>"訂單新增成功",
                "url"=>$url,
            ],JSON_UNESCAPED_UNICODE);
        } else {
            echo json_encode([
                "return_code"=> 0,
                "msg"=>"訂單新增失敗",
                "url"=>"",
            ],JSON_UNESCAPED_UNICODE);
        }
       
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $code)
    {
        
        if ($request->openExternalBrowser == null) {
            return redirect(url("$code/index?openExternalBrowser=1"));
        }
        
        return view("index");
    }
    
    public function application(Request $request , $code)
    {
        $platform = Platform::where("code" , $code)->first();
         
        
        $rule = $platform->rules()->where("enable", "1")->orderBy("updated_at" , "desc")->first();
        
        $ads = $platform->ads()->where("enable", "1")->orderBy("seq" , "asc")->get();
       
     
        //dd($platform->disconut);
        
        return view("application", [ "platform" => $platform , "rule" => $rule, "ads" => $ads]);
    }
    
   
    public function repairInfo(Request $request , $code)
    {     
        session()->forget('order');
         
        $platform = Platform::where("code" , $code)->first();
        
        $brands= $this->api("Get_CarModel.asp", []);
        
        
        usort($brands->brand_list,  function($a, $b){
            return strcmp($a->brand_name, $b->brand_name);
        });
        
        foreach ($brands->brand_list as $brand) {
            usort($brand->vehicle_list, function($a,$b){
                return strcmp($a->vehicle_name, $b->vehicle_name);
            });
        }
        
        return view("repair_info", [ "platform" => $platform , "brands" =>$brands->brand_list, "code"=>$code]);
    }
    
    public function orderCheck(Request $request){
        $code = $request->code;
        $phone = $request ->phone;
        
        $platform = Platform::where("code" , $code)->first();
        
        $order =  Order::where("platform_id", $platform->id)->where("phone", $phone)->whereIn("status", [1,2,3])->get();
        
        $isCheck =  sizeof($order) > 0;
        
        echo $isCheck;
    }
    
    public function comfirm(Request $request , $code){
        $order = null;
        $no = $this->rand();
         
        $file = $request->file;
        $url = "";
       
        if ($file != null) {
            $extension  =  strtolower($file->getClientOriginalExtension())  ?:  'png';
            $url = env("ASSET_URL")."/uploads/user/$no.$extension";
            $file -> move(public_path() . "/uploads/user/", "$no.$extension");
        }
        
        
        
        $platform = Platform::where("code" , $code)->first();
        
        $now = date("Ymd", time());
        
        $discount =  $platform->disconuts()
                              ->where("code",   $request->disconut_code)
                             
                              ->whereRaw("? >= start_date  AND  ? <= end_date",[$now,  $now])
                              ->orderBy("updated_at" , "desc")
                              ->first();
        
        $discount_code ="";
        $discount_price =0;
        
        if ($discount == null) {
            $discount_code =$request->disconut_code;
        } else {
            $discount_code = $discount->code;
            $discount_price = $discount->price;
        }
        
        $order = new Order;
        $order->platform_id = $request->platform_id;
        $order->name = $request->name;
        $order->no = $no;
        $order->phone = $request->phone;
        if ($request->car_no_1) {
            $order->car_no = $request->car_no_1."-".$request->car_no_2;
        } else {
            $order->car_no="";
        }
        $order->addr = $request->addr;
        $order->status = 1;
        $order->disconut_code = $discount_code;
        $order->discount_price = $discount_price;
        $order->car_brand = $request->car_brand;
        $order->car_brand_name =  $request->car_brand_name;
        $order->car_type = $request->car_type;
        $order->car_type_name =  $request->car_type_name;
        $order->img = $url;
         
        session(["order" => $order]);
        
        $pay = Platform::where("code" , $code)->first()->pays()->where("enable" , "1")->orderBy("updated_at" , "desc")->first();
         
        return view("comfirm",["order" =>$order , "pay" =>$pay]);
    }
    
    public function api($serviceName , $data) {
        //dd(http_build_query($data));
        $url = env("API_URL").$serviceName;
        $this_header = array(
            "content-type: application/x-www-form-urlencoded;
             charset=UTF-8"
        );
        
        $curl = curl_init($url);
        curl_setopt($curl,CURLOPT_HTTPHEADER,$this_header);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data, JSON_UNESCAPED_UNICODE));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);
        //dd($response);
        return json_decode($response);
    }
    
    public function finish(Request $request , $code) {
        
        $order = session("order");
        
        if ($order == null) {
            return redirect("$code/index");
        } 
         
      
         
        $data = [
            "create_time"=> date('Y-m-d H:i:s'),   //string 30 送出 API 時間 yyyy-MM-dd HH:mm:ss
            "order_number"=> $order->no ,  //string 16 訂單編號
            "customer_id"=> $order->platform->code ,     // string 3 客戶代號
            "contact_name"=> $order->name   ,// string 20 姓名
            "plate_number"=>  $order->car_no   ,// string 8 車號
            "brand_id"=> $order->car_brand   ,// string 3 廠牌代號
            "vehicle_id"=>  $order->car_type  ,// string 5 車型代號
            "contact_tel"=> $order->phone   ,// string 13 聯絡電話
            "fault_address"=>$order->addr   , // string 100 故障地址 郵遞區號+地址
            "coupon_code"=>    $order->disconut_code ,// string 20 優惠碼
            "coupon_money"=>   $order->discount_price, //
        ];
        
        
        $result= $this->api("Post_RSA.asp", $data);
         
        $order->result = json_encode($result, JSON_UNESCAPED_UNICODE);
        $order->save();
        
       // dd($result);
        //@測試先 不刪除 session
        session()->forget('order');
        return view("finish" , ["order"=>$order, "code"=>$code]);
    }
    
    public function schedule(Request $request , $code, $order_no){
        $order = Order::where("no",$order_no)->first();
        
        $platform= $order->platform;
        
      
        
        $data = [
            "contact_tel"=>$order->phone,
            "plate_number"=>$order->car_no,
        ];
        
        /* for test
         $data = [
         "contact_tel"=>"0900000000",
         "plate_number"=>"AA-88"
         ];
        */
        
        $result= $this->api("Get_Status.asp", $data);
        
        if ($result->return_code =="01") {
            return redirect()->back()->with("msg", "無訂單資料")->withinput();
        } else if ($result->return_code =="99") {
            return redirect()->back()->with("msg", "傳送資料有誤")->withinput();
        }
         
        if(!isset($result->driver_location)) {
            $result->driver_location ="";
        }
        
        if(!isset($result->return_destination)) {
            $result->return_destination ="";
        }
        
        $this->setOrderStatus($result);
        
        if ( $result->return_status ==4 || $result->return_status ==5) {
            //檢查時間
            if ($order->callback_time ) {
                $callback_time =  strtotime( $order->callback_time);
                
                $now =  strtotime("now");
                
                $time = $now - $callback_time;
                
                if ($time >= 180) {
                    return view("waiting" , ["result"=> $result, "order"=>$order, "code" => $code]);
                }
                
            }
        } else if ($result->return_code =="01" ||  $result->return_code=="99") {
            return view("waiting" , ["result"=> $result, "order"=>$order, "code" => $code]);
        } 
        
        return view("schedule" , ["result"=> $result, "order"=>$order, "code" => $code]);
    }
    
    public function query_schedule(Request $request , $code){
        $car_no = $request->car_no_1 ."-" . $request->car_no_2;
        
        $order = Order::where("phone",$request->phone)->where("car_no", $car_no)->orderBy("created_at", "desc") ->first();
       
        if ($order == null) {
            return redirect()->back()->with("msg", "無訂單資料")->withinput();
        }
        
         $platform= $order->platform;
        
         $data = [
            "contact_tel"=>$order->phone,
            "plate_number"=>$order->car_no,
         ];
         
         /*
         $data = [
             "contact_tel"=>"0900000000",
             "plate_number"=>"AA-88"
         ];*/ 
         
        
         
        
        $result= $this->api("Get_Status.asp", $data);
      
        
        if ($result->return_code =="01") {
            return redirect()->back()->with("msg", "無訂單資料")->withinput();
        } else if ($result->return_code =="99") {
            return redirect()->back()->with("msg", "傳送資料有誤")->withinput();
        }
        
        if(!isset($result->driver_location)) {
            $result->driver_location ="";
        }
        
        if(!isset($result->return_destination)) {
            $result->return_destination ="";
        }
        //$result->driver_location ="";
        
        $this->setOrderStatus($result);
         
       
        if ( $result->return_status ==4 || $result->return_status ==5) {
            //檢查時間
            if ($order->callback_time ) {
                $callback_time =  strtotime( $order->callback_time);
                
                $now =  strtotime("now");
                
                $time = $now - $callback_time;
                
                if ($time >= 180) {
                    return view("waiting" , ["result"=> $result, "order"=>$order, "code" => $code]);
                }
                
            }
        } else if ($result->return_code =="01" ||  $result->return_code=="99") {
            return view("waiting" , ["result"=> $result, "order"=>$order, "code" => $code]);
        } 
        
        return view("schedule" , ["result"=> $result, "order"=>$order, "code" => $code]);
    }
    
    
    public function waiting(Request $request , $code, $order_no){
        
        $order = Order::where("no",$order_no)->first();
        
        $platform= $order->platform;
        
        /* test data
        $data = [
            "contact_tel"=>"0900000000",
            "plate_number"=>"AA-88"
        ];*/
        
        $data = [
            "contact_tel"=>$order->phone,
            "plate_number"=>$order->car_no,
        ];
        
        $result= $this->api("Get_Status.asp", $data);
       
        $this->setOrderStatus($result);
        
        
        return view("waiting" , ["result" =>$result,"code"=>$code]);
    }
    
    
    public function setOrderStatus($obj) {
        
        $obj->return_status_name  ="";
       
        if ($obj->return_code =="01") {
            $obj->return_status = -1;
            $obj->return_status_name =  "無訂單 ";
        } else if ($obj->return_code =="99") {
            $obj->return_status = 99;
            $obj->return_status_name =  "傳送資料有誤";
        } else {
            switch ($obj->return_status) {
                case 1: 
                    $obj->return_status_name =  "訂單成立";
                    break;
                case 2:
                    $obj->return_status_name =  "已派技師";
                    break;
                case 3:
                    $obj->return_status_name =  "抵達現場";
                    break;
                case 4:
                    $obj->return_status_name =  "完成服務";
                    break;
                case 5:
                    $obj->return_status_name =  "取消服務";
                    break;
            }
        }
    }
     
    public function query(Request $request , $code){   
        return view("query" , []);
    }
    
    
    function shortCode($length){
        $pattern = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLOMNOPQRSTUVWXYZ';
        $key = "";
        for($i=0;$i<$length;$i++)
        {
            $key .= $pattern{mt_rand(0,strlen($pattern)-1)};    //生成php隨機數
        }
        return $key;   
    }
    
    function rand(){
       
        $str = "";
        for($i = 0; $i < 2; $i++){
            $str.=  chr(rand(0, 25) + 65);
        }
        
        for($i = 0; $i <10; $i++){
            $str.=  mt_rand(0,9);
        }
                
        return $str;
    }
    
    public function discountCheck(Request $request , $code){
        $discount_code = $request ->disconut_code;
        
        if (!$request->disconut_code) {
            echo "true";
            return;
        }
        
        $platform = Platform::where("code" , $code)->first();
        
        $now = date("Ymd", time());
       
        $discount =  $platform->disconuts()
        ->where("code",  $discount_code)
        
        ->whereRaw("? >= start_date  AND  ? <= end_date",[$now,  $now])
        ->orderBy("updated_at" , "desc")
        ->first();
        
        if ($discount ==null) {
            echo "false";
        } else {
            echo "true";
        }
    }
}
