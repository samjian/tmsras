<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use App\Models\Platform;


class PlatformMiddleware
{
    /**
     * Admin   Middleware
     * @param unknown $request
     * @param Closure $next
     * @return unknown
     */
    public function handle($request, Closure $next)
    {
         DB::connection()->enableQueryLog();
         
         $pathInfo = $request->getPathInfo();
         $temp = explode("/", $pathInfo);
        
         $code = $temp[1];
       
         $platform = Platform::where("code" , $code)->first();
        
         if ($platform == null) {
             abort(403, '禁止訪問，平台代碼錯誤');
         }
         
         if ($platform->enable ==0) {
             abort(403, '禁止訪問，平台已停用');
         }
         
         
         return $next($request);
    }
    
   
}