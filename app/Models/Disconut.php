<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Disconut extends BaseModel
{
     
    public function platform() {
        return $this->hasOne(Platform::class,   "id" , "platform_id");
    }
    
}
