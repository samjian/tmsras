<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends BaseModel
{
    /**
     * 取得用戶
     */
    public function platform() {
        return $this->hasOne(Platform::class,   "id" , "platform_id");
    }
}
