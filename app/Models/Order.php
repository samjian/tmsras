<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends BaseModel
{
    use SoftDeletes;
    
    const STATUS = [
        1=>"訂單成立",
        2 =>"已派技師",
        3 => "抵達現場",
        4 => "完成服務",
        5 => "取消服務",
    ]; 
     
    public function platform() {
        return $this->hasOne(Platform::class,   "id" , "platform_id");
    }
}
