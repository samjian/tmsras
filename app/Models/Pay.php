<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pay extends BaseModel
{
    public function platform() {
        return $this->hasOne(Platform::class,   "id" , "platform_id");
    }
    
}
