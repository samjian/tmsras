<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Platform extends BaseModel
{
    use SoftDeletes;   
    
    public function news()
    {
        return $this->hasMany(News::class,   "platform_id");
    }
    
    public function ads()
    {
        return $this->hasMany(Ad::class,   "platform_id");
    }
    
    public function rules()
    {
        return $this->hasMany(Rule::class,   "platform_id");
    }
    
    public function disconuts()
    {
        return $this->hasMany(Disconut::class,   "platform_id");
    }
    
    public function pays()
    {
        return $this->hasMany(Pay::class,   "platform_id");
    }
    
    
    
}
