<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisconutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disconuts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('support_id');
            $table->string('code')->comment('折扣代碼');
            $table->date('start_date')->comment('有效開始日期');
            $table->date('end_date')->comment('有效結束日期');
            $table->integer('price')->comment('折扣金額');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disconuts');
    }
}
