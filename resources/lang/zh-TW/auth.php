<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => '無此帳號!',
    'password' => '密碼錯誤!',
    'throttle' => '登入太多次錯誤,請 :seconds 秒後再嘗試!',

];
