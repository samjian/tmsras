@extends('layouts.head')


@section('content') 

<style>
    h2{
        font-weight: bold;
    }
    li{
        list-style-type: square;
    }
</style>

<script>
	$(function(){
		 var swiper = new Swiper(".mySwiper", {
		        loop: true,
		        autoplay: {
		            delay: 3000
		        },
		        direction: "vertical",
		    });

		    var swiper2 = new Swiper(".mySwiper2", {
		        loop: true,
		        autoplay: {
		            delay: 5000
		        },
		        pagination: {
		            el: ".swiper-pagination"
		        }
		    });
		    
		    $(".modal-content").mCustomScrollbar({
		        autoHideScrollbar: true,
		        theme:"dark"
		    });

		    $("#close").on("click", (function() {
	    		$("#modalBg").css("display", "none"), $("body").removeClass("modal-open")
	    	}))
    });

    function openAd(target){
    

    	var html = $("#" + target).html();
    	console.log(html);
    	$("#news_content").html( html );

    	$("#modalBg").css("display", "block");
    	$("body").addClass("modal-open");
    	
    }
   
</script>

   <!-- <a href="#" id="modalbtn">同意書</a> -->
<div id="modalBg">
    <div id="myModal" class="modal">
        <!-- Modal content -->
        <div class="closebtn" id="close">
            <div class="close"></div>
        </div>
        <div class="modal-content">
            <div class="editor_Content">
                <div id="news_content" class="editor_box">
                      
                </div>
            </div> 
        </div> 
    </div>
</div>

    <main class="application">

        <div class="container" data-aos="fade-up" data-aos-duration="1000">
            <a href="index">
                <div class="s-logo">
                        <img src="{{env('ASSET_URL')}}/dist/images/logo_tms2.png" alt="Logo">
                </div>
            </a>
        </div>

        <div class="bg-banner container">
            <h1 class="title" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">道路救援服務</h1>
        </div>
        
        <div class="container news-box row f14-28">
            <p class="news-txt" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">NEWS</p>
            <div class="swiper mySwiper" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">
                <div class="swiper-wrapper">
                
                    @foreach($platform->news as $news)
                        <div class="swiper-slide">
                            <a href="javascript:openAd('news_content_{{$news->id}}');" class="modalbtn">{{$news->title}}</a>
                            <div id="news_content_{{$news->id}}" style="display: none;">
                            	{!! $news->content !!}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <section class="section">
            <div class="bgcover"></div>
            <div class="container" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="600">
                <h2 class="title">
                    救援服務條款
                </h2>
                <div class="editor_Content">
                    @if($rule != null)
                    	{!! $rule->content !!}
                    @endif
                 
                </div>  

                <div class="slick-ad">
                    <div class="swiper mySwiper2">
                        <div class="swiper-wrapper">
                             @foreach($ads as $ad)
                               <div class="swiper-slide">
                                    <a href="{{$ad->url}}" target="blank"
                                        style="background-image: url('{{config('filesystems.disks.admin.url')}}/{{$ad->img}}');">
                                    </a>
                                </div>
                             @endforeach
                           
                             
                        </div>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>

                <div class="btn blue">
                    <a href="repair_info?openExternalBrowser=1">同意服務  </a>
                </div>
            </div>
        </section>        
    </main>
 


 @endsection  