@extends('layouts.head')

@section('content') 
<script>
    
	$(function(){
		 
    });
</script>
<style>
    /* .int i{
        text-decoration: underline;
        text-decoration-color: #00458d;
        text-decoration-line: underline;
        text-decoration-style: double;
    } */
    li{
        list-style-type: square;
    }
</style>
 
    <main class="comfirm">

        <div class="container bg-white" data-aos="fade-up" data-aos-duration="1000">
            <a href="index">
                <div class="s-logo">
                        <img src="{{env('ASSET_URL')}}/dist/images/logo_tms2.png" alt="Logo">
                </div>
            </a>
        </div>
        <div class="bg-banner container">
            <h1 class="title" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">計價說明與確認</h1>
        </div>

        <section class="section" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">
        <div class="bgcover"></div>
            <div class="container">
                <h2 class="title mb38">
                    您的案件資訊
                </h2>
                <table>
                    <tbody>
                        <tr>
                            <td>姓名</td>
                            <td>{{$order->name}}</td>
                        </tr>
                        
                        @if ($order->car_no)
                        <tr>
                            <td>車牌號碼</td>
                            <td>{{$order->car_no}}</td>
                        </tr>
                        @endif
                        
                        @if ($order->car_brand_name)
                        <tr>
                            <td>廠牌</td>
                            <td>{{$order->car_brand_name}}</td>
                        </tr>
                       @endif
                        @if ($order->car_type_name) 
                        <tr>
                            <td>車型</td>
                            <td>{{$order->car_type_name}}</td>
                        </tr>
                        @endif
                        <tr>
                            <td>聯絡電話</td>
                            <td>{{$order->phone}}</td>
                        </tr>
                        <tr>
                            <td>車輛故障位置</td>
                            <td>{{$order->addr}}</td>
                        </tr>
                        <tr>
                            <td>優惠序號</td>
                            <td>{{$order->disconut_code}}</td>
                        </tr>
                         @if ($order->img) 
                        <tr>
                        	 <td>車輛照片</td>
                              <td>    
                              	  <div class="pic-show">
                                    <img src="{{$order->img}}">
                                  </div>
                              </td>
                        </tr>
                         @endif
                    </tbody>
                </table>

                <h2 class="title">
                    收費說明
                </h2>
                <div class="int mb38 f12-24">
                    <span>詳細作業細則及收費方式依<i>全鋒客服</i>中心線上說明為準。</span>
                </div>
                <div class="editor_Content">
                     @if($pay != null)
                        {!! $pay->content !!}
                     @endif
                </div>  

                <div class="btn-box mt67">
                    <div class="btn blue-line">
                        <a href="javascript:history.go(-1);">
                            上一步
                        </a>
                    </div>
                    <div class="btn blue">
                        <a href="finish">
                            確認送出
                        </a>
                    </div>
                </div>

            </div>
        </section>
        
        
    
    </main>
@endsection  