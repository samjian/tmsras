@extends('layouts.head')

@section('content') 
    <main class="finish bgcover">
        <div class="container bg-white" data-aos="fade-up" data-aos-duration="1000">
            <a href="index">
                <div class="s-logo">
                    <img src="{{env('ASSET_URL')}}/dist/images/logo_tms2.png" alt="Logo">
                </div>
            </a>
        </div>

        <div class="bg-banner container">
            <h1 class="title" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">您的案件已成功送出</h1>
        </div>

        <section class="finish-contain" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">
            <div class="container">
                <div class="order-finish">
                    <p>訂單
                        <font class="co-blue">{{$order->no}}</font>
                        已送出，請您手機保持暢通，稍後會有客服與您電話聯繫，說明收費項目及內容，接下來您可以前往
                        <a href="schedule/{{$order->no}}" class="co-blue">服務進度查詢</a>
                        ，關注案件進度。</p>
                    <p>若您需要其他協助，歡迎您透過
                        <a href="tel:0800010010" class="co-blue" >幫助中心</a>
                        與全鋒客服團隊聯繫。
                    </p>

                    <div class="btn-box">
                        <div class="btn blue-line">
                            <a href="schedule/{{$order->no}}">
                                查看案件
                            </a>
                        </div>
                        <div class="btn blue">
                            <a href='index'>
                                回首頁
                            </a>
                        </div>
                    </div>
                
                </div>

            </div>
        </section>
        
        
    
    </main>
@endsection  