
@extends('layouts.head')


@section('content') 

<main>
    <section class="index container">
        <div class="txt-box">
            <div class="logo" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="100">
                <img src="{{env('ASSET_URL')}}/dist/images/logo_tms.png" alt="Logo">
            </div>
            <div class="welcome-txt" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                <span class="welcome">歡迎使用</span>
                <h1>全鋒道路救援</h1>
            </div>
        </div>
        <ul class="button-box" >
            <li><a href="application" class="f18-36">道路救援申請</a></li>
            <li><a href="query" class="f18-36">服務進度查詢</a></li>
        </ul>
    </section>

</main>

@endsection  
 