<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
    <meta http-equiv="X-ua-compatible" content="IE=edge">
    <meta http-equiv="x-ua-compatible" content="IE=9,10,11">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta charset="UTF-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="copyright" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:image" content="{{env('ASSET_URL')}}/dist/images/ogimg.jpg" />
    <!-- <link rel="shortcut icon" href="dist/images/foodgerman_ico.ico" type="image/x-icon" /> -->
    <meta name="format-detection" content="telephone=no">
    
    <title>全鋒道路救援</title>

    <link rel="stylesheet" href="{{env('ASSET_URL')}}/dist/css/aos.css">
    <link rel="stylesheet" href="{{env('ASSET_URL')}}/dist/css/main.css">
 
 	<link rel="stylesheet" href="{{env('ASSET_URL')}}/dist/css/swiper-bundle.min.css">
	<link rel="stylesheet" href="{{env('ASSET_URL')}}/dist/css/application.css">
 
	<link rel="stylesheet" href="{{env('ASSET_URL')}}/dist/css/index.css">
	
	<script src="{{env('ASSET_URL')}}/dist/js/jquery.min.js"></script>
    <script src="{{env('ASSET_URL')}}/dist/js/aos.js"></script>
	
    <script src="{{env('ASSET_URL')}}/dist/js/swiper-bundle.min.js"></script>
    <script src="{{env('ASSET_URL')}}/dist/js/jquery.mCustomScrollbar.concat.min.js"></script>
     
    
    <script src="{{env('ASSET_URL')}}/dist/js/jquery-validation-1.19.3/jquery.validate.min.js"></script>
    <script src="{{env('ASSET_URL')}}/dist/js/jquery-validation-1.19.3/localization/messages_zh_TW.js"></script>
    
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    
    <style>
        .error{
        	color:red;
        }
    </style> 
    <script type="text/javascript">
        window.alert = function(msg) {
        	Swal.fire({
          		  icon: 'warning',
        		  text: msg,
            });
        };
        
        @if (session('msg'))
        	  $(function(){
        		  alert("{{ session('msg') }}");
                  {{session()->forget('msg') }}
        	  });
        @endif
    </script>
</head>
<body class="lang_tw">
    @yield('content')
    
   <script src="{{env('ASSET_URL')}}/dist/js/main.js"></script>
</body>

</html>