@extends('layouts.head')

@section('content') 

<script src="https://maps.google.com/maps/api/js?key={{env('MAP_KEY')}}&language=zh-TW"></script>
  
<script type="text/javascript"> 
	var addr = "";
	function check (){
		window.opener.mapReturn(addr); 	
	    window.close();
	}

	var map, geocoder;

	 

	function initMap(position) {
	 
    
      const latlng = {
		    lat: position.coords.latitude,
		    lng:  position.coords.longitude,
		  };
     
      
	  geocoder = new google.maps.Geocoder();
	  const infowindow = new google.maps.InfoWindow();
     
       map = new google.maps.Map(document.getElementById('map'), {
          center: latlng,
          zoom: 17
       });


      
     

       geocoder.geocode({ location: latlng })
         .then((response) => {
        	 console.log(response);
        	  
           if (response.results[0]) {
             const marker = new google.maps.Marker({
               position: latlng,
               map: map,
             });
             
            

             addr = response.results[0].formatted_address;
              
             infowindow.setContent(response.results[0].formatted_address);
             infowindow.open(map, marker);
           } else {
             window.alert("No results found");
           }
         })
         .catch((e) => window.alert("Geocoder failed due to: " + e));
	  
	} 

 

	  $(function(){
		  
		  navigator.geolocation.getCurrentPosition(initMap,function(err){
			  console.log(err);
		  });  
	  })

</script>

    <main class="map">

        <div class="container" >
            <a href="index">
                <div class="s-logo">
                        <img src="{{env('ASSET_URL')}}/dist/images/logo_tms2.png" alt="Logo">
                </div>
            </a>
        </div>
        <div class="map-box">
            <div id="map" style="width: 100%; height: 450px;"></div>
        </div>
        <div class="container">
            <div class="btn-box mt67">
                <div class="btn blue-line">
                    <a href="javascript:;">
                        清除
                    </a>
                </div>
                <div class="btn blue"> 
                    <a href="javascript:check()">
                        確認
                    </a>
                </div>
            </div>
        </div>
        
    </main>

@endsection  