 
@extends('layouts.head')

@section('content') 
<script type="text/javascript">
    $(function(){
      	 $("#form").validate({
  			 rules: {
  			 	phone: {
  	 			      required: true,
  	      		      minlength:10,
  	      		      maxlength:10
  	 			},
  			  
                  
  			 },

  		     messages: {
  		    	 phone :"電話號碼錯誤"
  			 }
      	 });
    });

    function send() {
    	$("#form").submit();
    }

   
</script>
    <main class="login">
		
            <div class="container bg-white" data-aos="fade-up" data-aos-duration="1000">
                <a href="index">
                    <div class="s-logo">
                           <img src="{{env('ASSET_URL')}}/dist/images/logo_tms2.png" alt="Logo">
                    </div>
                </a>
            </div>
    
            <div class="bg-banner container">
                <h1 class="title" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">服務進度查詢</h1>
            </div>
    
            <section data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">
                <div class="bgcover"></div>
                <div class="container">
                    <p class="login-txt f14-28">
                        感謝您使用線上道路救援服務系統，查詢案件服務進度，請輸入報修時登記之連絡電話與車牌號碼。<br>
                        若您需要客服中心協助，請撥打24小時服務專線:
                        <a href="tel:0800010010">0800-010-010</a>
                    </p>
    
                    <div class="bgwhite">
                        <form id="form" action="query_schedule" class="login-form bgwhite" method="get">
                            <div>
                                <div class="form-br">
                                    <label for="phone">電話</label>
                                    <span class="required co-blue">(必填)</span>
                                </div>
                                <div>
                                    <div class="phone-icon"></div>
                                    <input type="tel" id="phone" name="phone" value="{{ old('phone') }}" placeholder="請輸入電話" maxlength="10" required>
                                </div>
                            </div>
    
                            <div>
                                <div class="form-br">
                                    <label for="cartNum">車號</label>
                                    <span class="required co-blue">(必填)</span>
                                </div>
                                <div class="input-row">
                                    <div class="cart-icon"></div>
                                    <input class="cart-input input1" type="text" id="car_no_1" name="car_no_1" value="{{old('car_no_1')}}" placeholder="ABCD" maxlength="4" required>
                                    <span>－</span>
                                    <input class="cart-input input2" type="text"  id="car_no_2" name="car_no_2" value="{{old('car_no_2')}}" placeholder="1234" maxlength="4" required>
                                </div>
                            </div>
                        </form>
                        <div class="btn blue">
                             <a href="javascript:send()">
                                查詢
                            </a>
                        </div>
                    
                    </div>
    
                </div>
            </section>
        
        
    
    </main>
@endsection 