 
@extends('layouts.head')

@section('content') 

<script src="https://maps.googleapis.com/maps/api/js?key={{env('MAP_KEY')}}&libraries=places"></script>
 
  
<script>
    var brands = {!! json_encode($brands) !!}
	var type = "enter";
	
	$(function(){
		$("#car_brand").change(function(){
			$('#car_type').children().remove().end().append('<option value="" selected>請選擇車型</option>');

			var val = $(this).val();
			
		 
			var brand = null;
			  
		    for(var i=0; i <brands.length ; i++) {
				if (brands[i].brand_id == val ) {
					brand = brands[i];
			    }
		    }
 
			if (brand == null) return;

            $.each(brand.vehicle_list, function(i, item) {
                $('#car_type').append($('<option>', {
                    value: item.vehicle_id,
                    text: item.vehicle_name
                }));
            })  
		});

	
		 $("#enter_form").validate({
				   
	 		rules: {
	 			phone: {
 			      required: true,
      		      minlength:10,
      		      maxlength:10
 			    },
	 			disconut_code: {
                     remote: '{{url("/$code/discountCheck")}}',
                 },
		 	},

		 	messages: {
		 		disconut_code :"優惠序號錯誤",
		 		phone :"電話號碼錯誤"
			},
			submitHandler: function(form) {      
			     $.get("{{url('orderCheck')}}", {"code" : '{{$code}}', 'phone': $("#phone").val()},function(result){
					 if (result ==1) {
						 var yes = confirm('親愛的車主您好，您目前有案件正在服務中，請確認是否再提交一筆新報修資料?');

						 if (yes) {
							form.submit();
						 } 
				     } else{
				    	 form.submit();
				     }
			    	 
				 }) 
			 },  
		 });
		 
		 $("#camera_form").validate({
			 rules: {
			 	file : "required", 
			 	phone: {
	 			      required: true,
	      		      minlength:10,
	      		      maxlength:10
	 			},
			 	disconut_code: {
                    remote: '{{url("/$code/discountCheck")}}',
                },
                
			 },

		     messages: {
		    	 file :"請上傳照片",
		    	 disconut_code :"優惠序號錯誤",
		    	 phone :"電話號碼錯誤"
			 },
			 
			 submitHandler: function(form) {      
			     $.get("{{url('orderCheck')}}", {"code" : '{{$code}}', 'phone': $("#camera_phone").val()},function(result){
					 if (result ==1) {
						 var yes = confirm('親愛的車主您好，您目前有案件正在服務中，請確認是否再提交一筆新報修資料?');

						 if (yes) {
							form.submit();
						 } 
				     } else{
				    	 form.submit();
				     }
			    	 
				 }) 
			 },  
			
		 });

		 input = document.getElementById("addr");
		 const options = {
			 componentRestrictions: { country: 'tw' } // 限制在台灣範圍
		 };

		 input2 = document.getElementById("camera_addr");
		  
		 const autocomplete = new google.maps.places.Autocomplete(input, options);
		 const autocomplete2 = new google.maps.places.Autocomplete(input2, options);
    });

	function openMap(){
	    window.open('{{url("map")}}', '_blank');
	}

	function mapReturn(val){

		 if (type == "enter") {
			 $("#addr").val(val);
		 } else {
			 $("#camera_addr").val(val);
		 }
		  
	}
    
    function send() {

        if ($("#car_brand").val()) {
        	$("#car_brand_name").val($("#car_brand").find("option:selected").text());
        }

        if ($("#car_type").val()) {
        	$("#car_type_name").val($("#car_type").find("option:selected").text());
        }

		$("#car_no_1").val($("#car_no_1").val().toUpperCase());
		$("#car_no_2").val($("#car_no_2").val().toUpperCase());
         
    	$("#"+ type + "_form").submit();
    }

    function openCity(cityName) {
        var i;
        var x = document.getElementsByClassName("form");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";  
        }
        document.getElementById(cityName).style.display = "block";  

        type = cityName;
    }

    
</script>
 
    <main class="repair-info">

        <div class="container" data-aos="fade-up" data-aos-duration="1000">
            <a href="index">
                <div class="s-logo">
                        <img src="{{env('ASSET_URL')}}/dist/images/logo_tms2.png" alt="Logo">
                </div>
            </a>
        </div>

        <div class="bg-banner container">
            <h1 class="title" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">報修資訊</h1>
        </div>

        <section class="section">
            <div class="bgcover"></div>
            <div class="container">
                <h2 class="title mb38" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">
                    報修<span>3</span>步驟
                </h2>
                <div>
                    <div class="step row aic" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="500">
                        <div class="step-icon" style="background: url('{{env('ASSET_URL')}}/dist/images/icon1.jpg') center / cover no-repeat;"></div>
                        <div class="step-txt">
                            <p>STEP 1</p>
                            <p>報修資料輸入或車輛拍照</p>
                        </div>
                    </div>
                    <div class="step row aic" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="600">
                        <div class="step-icon" style="background: url('{{env('ASSET_URL')}}/dist/images/icon2.jpg') center / cover no-repeat;"></div>
                        <div class="step-txt">
                            <p>STEP 2</p>
                            <p>確認需求送出</p>
                        </div>
                    </div>
                    <div class="step row aic" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="700">
                        <div class="step-icon" style="background: url('{{env('ASSET_URL')}}/dist/images/icon3.jpg') center / cover no-repeat;"></div>
                        <div class="step-txt">
                            <p>STEP 3</p>
                            <p>客服與您電話聯繫說明收費項目<br>請放心，此時您享有最終服務選擇權</p>
                        </div>
                    </div>
                </div>
                
                <div class="tabs" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">
                    <div class="btn-box-tab">
                        <button class="enter active" onclick="openCity('enter')">
                            <span class="infoIcon"></span>
                            <span>填寫輸入</span>
                        </button>
                        <button class="camera" onclick="openCity('camera')">
                            <span class="cameraIcon"></span>
                            <span>拍照輸入</span>
                        </button>
                    </div>

                    <div id="enter" class="form">
                        <form id="enter_form" action="comfirm" class="login-form bgwhite" method="post">
                        	 @csrf
                        	<input type="hidden" name="platform_id" value="{{$platform->id}}"/>
                        	<input type="hidden" name="type"  value="enter"/>
                        	
                            <input type="hidden" id="car_brand_name" name="car_brand_name"/>
                            <input type="hidden" id="car_type_name" name="car_type_name"/>
                        	
                            <div>
                                <div class="form-br">
                                    <label for="name">姓名</label>
                                    <span class="required co-blue">(必填)</span>
                                </div>
                                <div>
                                    <input type="text" id="name" name="name" placeholder="請輸入姓名"  required>
                                </div>
                            </div>

                            <div>
                                <div class="form-br">
                                    <label for="cartNum">車號</label>
                                    <span class="required co-blue">(必填)</span>
                                </div>
                                <div class="input-row">
                                    <input class="cart-input input1" type="text" id="car_no_1" name="car_no_1"  placeholder="ABCD" maxlength="4" required>
                                    <span>－</span>
                                    <input class="cart-input input2" type="text" id="car_no_2" name="car_no_2" placeholder="1234" maxlength="4" required>
                                </div>

                            </div>

                            <div>
                                <div class="form-br">
                                    <label for="car_brand">廠牌</label>
                                      <span class="required co-blue">(必填)</span>
                                </div>
                                <select class="brand theme_select" id="car_brand" name="car_brand" required>
                                    <option value="" selected >請選擇廠牌</option>
                                    @foreach($brands as $brand)
                                    	<option value='{{$brand->brand_id}}'>{{$brand->brand_name}}</option>
                                    @endforeach	
                                </select>
                                
                            </div>

                            <div>
                                <div class="form-br">
                                    <label for="car_type">車型</label>
                                      <span class="required co-blue">(必填)</span>
                                </div>
                                <select class="car theme_select" id="car_type" name="car_type" required>
                                    <option  value="" selected>請選擇車型</option>
                                                       
                                </select>
                            </div>

                            <div>
                                <div class="form-br">
                                    <label for="phone">聯絡電話</label>
                                    <span class="required co-blue">(必填)</span>
                                </div>
                                <div>
                                    <input type="tel" id="phone" name="phone" placeholder="請輸入聯絡電話" maxlength="10" required>
                                </div>
                            </div>

                            <div>
                                <div class="form-br">
                                    <label for="location">位置設定</label>
                                    <span class="required co-blue">(必填)</span>
                                </div>
                                <div>
                                    <input type="text" id="addr" name="addr" placeholder="請輸入車輛故障位置"  ref="site" v-model="site"  required>
                                </div>
                                <div class="row-end">
                                    <div class="sentloca">
                                        <a href="javascript:openMap();">
                                            <span class="location"></span>
                                            <span>傳送位置</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <div class="form-br">
                                    <label for="number" class="no-be">優惠序號</label>
                                </div>
                                <div>
                                    <input type="text" id="disconut_code" name="disconut_code" placeholder="請輸入優惠序號">
                                </div>
                            </div>
                            
                        </form>
                    </div>

                    <div id="camera" class="form" style="display:none">
                        <form id="camera_form" action="comfirm" class="login-form" method="post" enctype="multipart/form-data">
                        	 @csrf
                        	<input type="hidden" name="platform_id" value="{{$platform->id}}"/>
                        	<input type="hidden" name="type"  value="camera"/>
                        	
                            <div>
                                <div class="form-br">
                                    <label for="name">姓名</label>
                                    <span class="required co-blue">(必填)</span>
                                </div>
                                <div>
                                    <input type="text" id="name" name="name" placeholder="請輸入姓名" required>
                                </div>
                            </div>

                            <div>
                                <div class="form-br">
                                    <label for="phone">聯絡電話</label>
                                    <span class="required co-blue">(必填)</span>
                                </div>
                                <div>
                                    <input type="tel" id="camera_phone" name="phone" placeholder="請輸入聯絡電話" maxlength="10" required>
                                </div>
                            </div>

                            <div>
                                <div class="form-br">
                                    <label for="location">位置設定</label>
                                    <span class="required co-blue">(必填)</span>
                                </div>
                                <div>
                                    <input type="text"  id="camera_addr" name="addr" placeholder="請輸入車輛故障位置"   required>
                                </div>
                                <div class="row-end">
                                    <div class="sentloca">
                                         <a href="javascript:openMap();">
                                            <span class="location"></span>
                                            <span>傳送位置</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <div class="form-br">
                                    <label for="number" class="no-be">優惠序號</label>
                                </div>
                                <div>
                                    <input type="text" id="camera_disconut_code" name="disconut_code" placeholder="請輸入優惠序號">
                                </div>
                            </div>

                            <div>
                                <div class="form-br">
                                    <label>車輛照片</label>
                                    <span class="required co-blue">(必填)</span>
                                    <p class="form-int">請拍攝車尾、車牌、廠牌及型號</p>
                                </div>
                                <div class="pic-show">
                                    <img src="{{env('ASSET_URL')}}/dist/images/car.jpg" id="demo">
                                </div>
                                <div class="file-placeholder">
                                    <input type="file" class="fileUpload" id="file" name="file" required>
                                    <div class="file-browse">
                                        <span class="file-browse-txt">IMG1234.jpg</span>
                                        <span class="browse">拍攝照片</span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="btn-box mt67">
                    <div class="btn blue-line">
                        <a href="application">
                            上一步
                        </a>
                    </div>
                    <div class="btn blue"> 
                        <a href="javascript:send()">
                            下一步
                        </a>
                    </div>
                </div>

            </div>
        </section>
        
        
    
    </main>
    
   

@endsection  