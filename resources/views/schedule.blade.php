@extends('layouts.head')

@section('content') 

<script src="https://maps.google.com/maps/api/js?key={{env('MAP_KEY')}}&language=zh-TW"></script>

<script type="text/javascript">
 
	$(function(){

		console.log('{!! json_encode($result, JSON_UNESCAPED_UNICODE) !!}');
		
		var status =  "{{$result->return_status}}";
 
		switch (status) {
			case "1": 
				$("#status_1").attr("class","co-blue");
				break;
			case "2":
				$("#status_1").attr("class","finish");
				$("#status_2").attr("class","co-blue");
			 
				break;
			case "3":
				$("#status_1").attr("class","finish");
				$("#status_2").attr("class","finish");
				$("#status_3").attr("class","co-blue");
				break;
			case "4": 
				$("#status_1").attr("class","finish");
				$("#status_2").attr("class","finish");
				$("#status_3").attr("class","finish");
				$("#status_4").attr("class","co-blue");
				break;
				
		}

		initMap();
		
		window.setTimeout(function(){
		  	  location.reload();
		}, 1000 *60 * 3);
    });

	function initMap() {
       var addr ="{{$result->return_destination}}";	 //目的地位置	 
	   var addr2 = "{{$result->driver_location}}"; //技師位置
	   var addr3 ="{{$order->addr}}";	 //車輛故障位置
	   
	   geocoder = new google.maps.Geocoder();
	  
       map = new google.maps.Map(document.getElementById('map'), {
          //center: addr,
          zoom: 11
       });

       var  size = new google.maps.Size(80, 50);

 	   if (addr) {
			 geocoder.geocode( { 'address': addr}, function(results, status) {
					const marker = new google.maps.Marker({
						 position: results[0].geometry.location,
						 map: map,
						 icon: {
						    url: '/tmsras/public/dist/images/09_schedule_06.png',
						    scaledSize: new google.maps.Size(70, 45), // scaled size
					     },
					 	 
					});
					const infowindow = new google.maps.InfoWindow({
						content: "目的地  <br>" + results[0].formatted_address,
				    });

					marker.addListener("click", () => {
						infowindow.open(map, marker);
					});
			 
				});     
		 }

		 if (addr2) {
				geocoder.geocode( { 'address': addr2}, function(results, status) {
					const marker = new google.maps.Marker({
						 position: results[0].geometry.location,
						 map: map,
						 icon: {
							    url: '/tmsras/public/dist/images/09_schedule_11.png',
							    scaledSize: new google.maps.Size(80, 50), // scaled size
					     },
						 
					});
					
					const infowindow = new google.maps.InfoWindow({
					    content: "技師位置  <br>" + results[0].formatted_address,
					});

					marker.addListener("click", () => {
						infowindow.open(map, marker);
					});
						
			    });
		}
			if (addr3) {
				geocoder.geocode( { 'address': addr3}, function(results, status) {
					 map.setCenter(results[0].geometry.location);
					 const marker = new google.maps.Marker({
						 position: results[0].geometry.location,
						 map: map,
						 icon: {
						    url: '/tmsras/public/dist/images/09_schedule_03.png',
						    scaledSize: new google.maps.Size(80, 45), // scaled size
						 },
						  
					});

					const infowindow = new google.maps.InfoWindow({
					    content: "愛車位置   <br>" +results[0].formatted_address,
					});

					marker.addListener("click", () => {
						infowindow.open(map, marker);
					});	
			});     		
  	    }
	 }

	  
	
</script>

    <main class="schedule">
        <div class="bsho"> 
            <div class="container bg-white" data-aos="fade-up" data-aos-duration="1000">
               <a href='{{url("/$code/index")}}'>
                    <div class="s-logo">
                            <img src="{{env('ASSET_URL')}}/dist/images/logo_tms2.png" alt="Logo">
                    </div>
                </a>
            </div>
                
            <div class="container nopa bg-white" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                <div class="row service-now">
                    <div class="f18-36">
                        <span>服務進度：</span>
                        <span>{{$result->return_status_name}}</span>
                    </div>
                    <div>
                        <a href="tel:0800010010">幫助中心</a>
                    </div>
                </div>
            </div> 

            <div class="container bg-white" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">
                <!-- 完成[finish]
                正在[co-blue]
                未完成[undone] -->
                <ul class="service-stap row f14-28">
                    <li  id="status_1"  class="undone">
                        <i>1</i>
                        <span>訂單成立</span>
                    </li>
                    <li id="status_2" class="undone">
                        <i>2</i>
                        <span>已派技師</span>
                    </li>
                    <li id="status_3" class="undone">
                        <i>3</i>
                        <span>抵達現場</span>
                    </li>
                    <li id="status_4" class="undone">
                        <i>4</i>
                        <span>完成服務</span>
                    </li>
                </ul>
            </div>
        </div>
        
        <div data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">
			<!-- 
            <div class="map f12-24">
                <img src="{{env('ASSET_URL')}}/dist/images/bg.jpg">
                <span class="user-location">使用者位置</span>
                <span class="site-location">目的地</span>
                <span class="tech-location">技師所在位置</span>
            </div>
             -->
             
              <div class="map f12-24">
                <div id="map" style="width: 100%; height: 450px;"></div>
            </div>

            <div class="container tech">
                <div class="bgwhite row info-tech">
                    <div class="info-padd info-tech-icon"></div>
                    <div class="info-padd info-txt">
                        <p class="mb20 f12-24">技師資訊</p>
                        <div class="f18-36 co-blue" >
                             <span>
                                 @if ($result->return_status != 5)
                                 	{{$result->driver_name}}  {{$result->service_car}}
                                 @endif
                             </span> 
                            
                        </div>
                    </div>
                    <div class="info-padd">
                        <p class="mb20 f12-24">預計抵達時間</p>
                        <p class="co-blue f18-36">{{$result->return_eta}}</p>
                    </div>
                </div>
            </div>

            <section class="section">
                <div class="" >
                    <h2 class="title mb38">
                        您的案件資訊
                    </h2>
                    <table>
                        <tbody>
                            <tr>
                                <td>姓名</td>
                                <td>{{$order->name}}</td>
                            </tr>
                            <tr>
                                <td>車牌號碼</td>
                                <td>{{$order->car_no}}</td>
                            </tr>
                            <tr>
                                <td>廠牌</td>
                                <td>{{$order->car_brand_name}}</td>
                            </tr>
                            <tr>
                                <td>車型</td>
                                <td>{{$order->car_type_name}}</td>
                            </tr>
                            <tr>
                                <td>聯絡電話</td>
                                <td>{{$order->phone}}</td>
                            </tr>
                            <tr>
                                <td>車輛故障位置</td>
                                <td>{{$order->addr}}</td>
                            </tr>
                            <tr>
                                <td>優惠序號</td>
                                <td>{{$order->disconut_code}}</td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="btn blue">
                        <a href='{{url("$code/index")}}'>
                            回首頁
                        </a>
                    </div>

                </div>
            </section>

        </div>

        
        
        
    
    </main>
  @endsection  