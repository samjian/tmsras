 
@extends('layouts.head')

@section('content') 
    <main class="waiting">

        <div class="container" data-aos="fade-up" data-aos-duration="1000">
            <a href='{{url("/$code/index")}}'>
                <div class="s-logo">
                      <img src="{{env('ASSET_URL')}}/dist/images/logo_tms2.png" alt="Logo">
                </div>
            </a>
        </div>
        <div class="bgcover container">
            <div class="waiting-txt">
				@if ($result->return_status ==4)
					<!--完成服務 -->
					<div class="cart-wait" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200"></div>
			    @elseif($result->return_status ==5)
					<!--取消服務 -->
					<div class="cart-wait" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200"></div>
				@endif
                
				<h2 class="dispatch" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">{{$result->return_status_name}}</h2>
                <p class="wait-txt f14-28" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="600">
                    <span>親愛的車主您好 </span>
                    <span>感謝您使用道援申報服務平台</span>
					 <span>祝您 行車平安順心</span>
                </p>
            </div>
        </div>
        
    
    </main>
@endsection 