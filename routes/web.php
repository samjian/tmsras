<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


Route::get('/', function () {
    return view('HomeController@index');
});
*/

Route::group([ 'middleware' => ["platform"]], function(){
   
    Route::get('/{code}/index', "PlatformController@index");
    Route::get('/{code}/application', "PlatformController@application");
    Route::get('/{code}/repair_info', "PlatformController@repairInfo");
    Route::post('/{code}/comfirm', "PlatformController@comfirm");
    Route::get('/{code}/finish', "PlatformController@finish");
    Route::get('/{code}/schedule/{order_no}', "PlatformController@schedule");
    Route::get('/{code}/waiting/{order_no}', "PlatformController@waiting");
    Route::get('/{code}/query', "PlatformController@query");
    Route::get('/{code}/query_schedule', "PlatformController@query_schedule");
    Route::get('/{code}/discountCheck', "PlatformController@discountCheck");
    
});

//外部直接連結
Route::get('/q/{no}', 'PlatformController@toQuery');
    
/**
 * API
 */
Route::post('/api/order', 'PlatformController@apiOrder');
Route::get('/orderCheck', 'PlatformController@orderCheck');
Route::get('/callback/{orderNo}/{status}', "PlatformController@callback");
Route::get('/update/{orderNo}/{phone}/{car_no}/{brand_id}/{brand_name}/{vehicle_id}/{vehicle_name}', "PlatformController@update");

Route::get('/map', function () {
    return view('map');
});