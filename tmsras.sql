/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 100422
 Source Host           : localhost:3306
 Source Schema         : tmsras

 Target Server Type    : MySQL
 Target Server Version : 100422
 File Encoding         : 65001

 Date: 28/02/2022 17:05:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin_menu
-- ----------------------------
DROP TABLE IF EXISTS `admin_menu`;
CREATE TABLE `admin_menu`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `order` int(11) NOT NULL DEFAULT 0,
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `permission` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_menu
-- ----------------------------
INSERT INTO `admin_menu` VALUES (2, 0, 9, 'Admin', 'fa-tasks', NULL, NULL, NULL, '2022-02-02 21:40:14');
INSERT INTO `admin_menu` VALUES (3, 0, 8, '使用者管理', 'fa-users', 'auth/users', NULL, NULL, '2022-02-01 19:25:36');
INSERT INTO `admin_menu` VALUES (4, 2, 10, 'Roles', 'fa-user', 'auth/roles', NULL, NULL, '2022-02-01 19:25:36');
INSERT INTO `admin_menu` VALUES (5, 2, 11, 'Permission', 'fa-ban', 'auth/permissions', NULL, NULL, '2022-02-01 19:25:36');
INSERT INTO `admin_menu` VALUES (6, 2, 12, 'Menu', 'fa-bars', 'auth/menu', NULL, NULL, '2022-02-01 19:25:36');
INSERT INTO `admin_menu` VALUES (7, 2, 13, 'Operation log', 'fa-history', 'auth/logs', NULL, NULL, '2022-02-01 19:25:36');
INSERT INTO `admin_menu` VALUES (9, 0, 14, 'Helpers', 'fa-gears', NULL, '*', '2022-02-01 09:34:08', '2022-02-01 19:25:36');
INSERT INTO `admin_menu` VALUES (10, 9, 15, 'Scaffold', 'fa-keyboard-o', 'helpers/scaffold', NULL, '2022-02-01 09:34:08', '2022-02-01 19:25:36');
INSERT INTO `admin_menu` VALUES (11, 9, 16, 'Database terminal', 'fa-database', 'helpers/terminal/database', NULL, '2022-02-01 09:34:08', '2022-02-01 19:25:36');
INSERT INTO `admin_menu` VALUES (12, 9, 17, 'Laravel artisan', 'fa-terminal', 'helpers/terminal/artisan', NULL, '2022-02-01 09:34:08', '2022-02-01 19:25:36');
INSERT INTO `admin_menu` VALUES (13, 9, 18, 'Routes', 'fa-list-alt', 'helpers/routes', NULL, '2022-02-01 09:34:08', '2022-02-01 19:25:36');
INSERT INTO `admin_menu` VALUES (14, 0, 2, '合作通路管理', 'fa-automobile', '/platform', '*', '2022-02-01 18:40:55', '2022-02-02 21:41:00');
INSERT INTO `admin_menu` VALUES (15, 0, 1, '訂單管理', 'fa-book', '/order', '*', '2022-02-01 18:44:05', '2022-02-02 21:41:00');
INSERT INTO `admin_menu` VALUES (16, 0, 7, '優惠碼管理', 'fa-yen', 'disconut', '*', '2022-02-01 18:45:26', '2022-02-02 21:41:00');
INSERT INTO `admin_menu` VALUES (17, 0, 3, '跑馬燈訊息管理', 'fa-bell-o', '/news', '*', '2022-02-01 19:18:24', '2022-02-02 21:41:00');
INSERT INTO `admin_menu` VALUES (18, 0, 4, '救援服務條款上架管理', 'fa-bolt', '/rule', '*', '2022-02-01 19:20:10', '2022-02-02 21:41:00');
INSERT INTO `admin_menu` VALUES (19, 0, 5, '收費說明上架管理', 'fa-credit-card', '/pay', '*', '2022-02-01 19:24:01', '2022-02-02 21:41:00');
INSERT INTO `admin_menu` VALUES (20, 0, 6, '廣告輪播管理', 'fa-file-photo-o', '/ad', '*', '2022-02-01 19:24:53', '2022-02-02 21:41:00');

-- ----------------------------
-- Table structure for admin_operation_log
-- ----------------------------
DROP TABLE IF EXISTS `admin_operation_log`;
CREATE TABLE `admin_operation_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `input` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `admin_operation_log_user_id_index`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1384 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_operation_log
-- ----------------------------
INSERT INTO `admin_operation_log` VALUES (1, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 08:49:09', '2022-02-01 08:49:09');
INSERT INTO `admin_operation_log` VALUES (2, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 08:53:46', '2022-02-01 08:53:46');
INSERT INTO `admin_operation_log` VALUES (3, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 08:54:25', '2022-02-01 08:54:25');
INSERT INTO `admin_operation_log` VALUES (4, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 08:54:27', '2022-02-01 08:54:27');
INSERT INTO `admin_operation_log` VALUES (5, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 08:56:38', '2022-02-01 08:56:38');
INSERT INTO `admin_operation_log` VALUES (6, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 08:59:06', '2022-02-01 08:59:06');
INSERT INTO `admin_operation_log` VALUES (7, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:01:14', '2022-02-01 09:01:14');
INSERT INTO `admin_operation_log` VALUES (8, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:01:16', '2022-02-01 09:01:16');
INSERT INTO `admin_operation_log` VALUES (9, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:02:15', '2022-02-01 09:02:15');
INSERT INTO `admin_operation_log` VALUES (10, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:02:16', '2022-02-01 09:02:16');
INSERT INTO `admin_operation_log` VALUES (11, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:02:16', '2022-02-01 09:02:16');
INSERT INTO `admin_operation_log` VALUES (12, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:04:30', '2022-02-01 09:04:30');
INSERT INTO `admin_operation_log` VALUES (13, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:05:09', '2022-02-01 09:05:09');
INSERT INTO `admin_operation_log` VALUES (14, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:05:10', '2022-02-01 09:05:10');
INSERT INTO `admin_operation_log` VALUES (15, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:05:11', '2022-02-01 09:05:11');
INSERT INTO `admin_operation_log` VALUES (16, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:09:10', '2022-02-01 09:09:10');
INSERT INTO `admin_operation_log` VALUES (17, 1, 'admin/exceptions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 09:09:12', '2022-02-01 09:09:12');
INSERT INTO `admin_operation_log` VALUES (18, 1, 'admin/exceptions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 09:09:14', '2022-02-01 09:09:14');
INSERT INTO `admin_operation_log` VALUES (19, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 09:09:18', '2022-02-01 09:09:18');
INSERT INTO `admin_operation_log` VALUES (20, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:09:38', '2022-02-01 09:09:38');
INSERT INTO `admin_operation_log` VALUES (21, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:09:51', '2022-02-01 09:09:51');
INSERT INTO `admin_operation_log` VALUES (22, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:09:52', '2022-02-01 09:09:52');
INSERT INTO `admin_operation_log` VALUES (23, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:09:52', '2022-02-01 09:09:52');
INSERT INTO `admin_operation_log` VALUES (24, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:10:30', '2022-02-01 09:10:30');
INSERT INTO `admin_operation_log` VALUES (25, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:10:31', '2022-02-01 09:10:31');
INSERT INTO `admin_operation_log` VALUES (26, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:10:31', '2022-02-01 09:10:31');
INSERT INTO `admin_operation_log` VALUES (27, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:10:31', '2022-02-01 09:10:31');
INSERT INTO `admin_operation_log` VALUES (28, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:10:32', '2022-02-01 09:10:32');
INSERT INTO `admin_operation_log` VALUES (29, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:10:39', '2022-02-01 09:10:39');
INSERT INTO `admin_operation_log` VALUES (30, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:10:40', '2022-02-01 09:10:40');
INSERT INTO `admin_operation_log` VALUES (31, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:10:49', '2022-02-01 09:10:49');
INSERT INTO `admin_operation_log` VALUES (32, 1, 'admin/exceptions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 09:10:52', '2022-02-01 09:10:52');
INSERT INTO `admin_operation_log` VALUES (33, 1, 'admin/exceptions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 09:10:53', '2022-02-01 09:10:53');
INSERT INTO `admin_operation_log` VALUES (34, 1, 'admin/exceptions', 'GET', '127.0.0.1', '[]', '2022-02-01 09:11:16', '2022-02-01 09:11:16');
INSERT INTO `admin_operation_log` VALUES (35, 1, 'admin/exceptions', 'GET', '127.0.0.1', '[]', '2022-02-01 09:11:17', '2022-02-01 09:11:17');
INSERT INTO `admin_operation_log` VALUES (36, 1, 'admin/exceptions', 'GET', '127.0.0.1', '[]', '2022-02-01 09:11:18', '2022-02-01 09:11:18');
INSERT INTO `admin_operation_log` VALUES (37, 1, 'admin/exceptions', 'GET', '127.0.0.1', '[]', '2022-02-01 09:11:31', '2022-02-01 09:11:31');
INSERT INTO `admin_operation_log` VALUES (38, 1, 'admin/exceptions', 'GET', '127.0.0.1', '[]', '2022-02-01 09:11:32', '2022-02-01 09:11:32');
INSERT INTO `admin_operation_log` VALUES (39, 1, 'admin/exceptions', 'GET', '127.0.0.1', '[]', '2022-02-01 09:11:39', '2022-02-01 09:11:39');
INSERT INTO `admin_operation_log` VALUES (40, 1, 'admin/exceptions', 'GET', '127.0.0.1', '[]', '2022-02-01 09:11:40', '2022-02-01 09:11:40');
INSERT INTO `admin_operation_log` VALUES (41, 1, 'admin/exceptions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 09:11:43', '2022-02-01 09:11:43');
INSERT INTO `admin_operation_log` VALUES (42, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:11:48', '2022-02-01 09:11:48');
INSERT INTO `admin_operation_log` VALUES (43, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:11:55', '2022-02-01 09:11:55');
INSERT INTO `admin_operation_log` VALUES (44, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:13:27', '2022-02-01 09:13:27');
INSERT INTO `admin_operation_log` VALUES (45, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:13:28', '2022-02-01 09:13:28');
INSERT INTO `admin_operation_log` VALUES (46, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:13:29', '2022-02-01 09:13:29');
INSERT INTO `admin_operation_log` VALUES (47, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:13:29', '2022-02-01 09:13:29');
INSERT INTO `admin_operation_log` VALUES (48, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:13:30', '2022-02-01 09:13:30');
INSERT INTO `admin_operation_log` VALUES (49, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:13:30', '2022-02-01 09:13:30');
INSERT INTO `admin_operation_log` VALUES (50, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:13:30', '2022-02-01 09:13:30');
INSERT INTO `admin_operation_log` VALUES (51, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:13:31', '2022-02-01 09:13:31');
INSERT INTO `admin_operation_log` VALUES (52, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:13:40', '2022-02-01 09:13:40');
INSERT INTO `admin_operation_log` VALUES (53, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:13:44', '2022-02-01 09:13:44');
INSERT INTO `admin_operation_log` VALUES (54, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:13:45', '2022-02-01 09:13:45');
INSERT INTO `admin_operation_log` VALUES (55, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:13:54', '2022-02-01 09:13:54');
INSERT INTO `admin_operation_log` VALUES (56, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:14:02', '2022-02-01 09:14:02');
INSERT INTO `admin_operation_log` VALUES (57, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:14:03', '2022-02-01 09:14:03');
INSERT INTO `admin_operation_log` VALUES (58, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:34:25', '2022-02-01 09:34:25');
INSERT INTO `admin_operation_log` VALUES (59, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:37:02', '2022-02-01 09:37:02');
INSERT INTO `admin_operation_log` VALUES (60, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:37:03', '2022-02-01 09:37:03');
INSERT INTO `admin_operation_log` VALUES (61, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:37:07', '2022-02-01 09:37:07');
INSERT INTO `admin_operation_log` VALUES (62, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:37:30', '2022-02-01 09:37:30');
INSERT INTO `admin_operation_log` VALUES (63, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:37:32', '2022-02-01 09:37:32');
INSERT INTO `admin_operation_log` VALUES (64, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:37:47', '2022-02-01 09:37:47');
INSERT INTO `admin_operation_log` VALUES (65, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:37:48', '2022-02-01 09:37:48');
INSERT INTO `admin_operation_log` VALUES (66, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:37:49', '2022-02-01 09:37:49');
INSERT INTO `admin_operation_log` VALUES (67, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:37:49', '2022-02-01 09:37:49');
INSERT INTO `admin_operation_log` VALUES (68, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:38:12', '2022-02-01 09:38:12');
INSERT INTO `admin_operation_log` VALUES (69, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:38:13', '2022-02-01 09:38:13');
INSERT INTO `admin_operation_log` VALUES (70, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:38:15', '2022-02-01 09:38:15');
INSERT INTO `admin_operation_log` VALUES (71, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:38:42', '2022-02-01 09:38:42');
INSERT INTO `admin_operation_log` VALUES (72, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:38:43', '2022-02-01 09:38:43');
INSERT INTO `admin_operation_log` VALUES (73, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:38:43', '2022-02-01 09:38:43');
INSERT INTO `admin_operation_log` VALUES (74, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:39:02', '2022-02-01 09:39:02');
INSERT INTO `admin_operation_log` VALUES (75, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:39:03', '2022-02-01 09:39:03');
INSERT INTO `admin_operation_log` VALUES (76, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:39:13', '2022-02-01 09:39:13');
INSERT INTO `admin_operation_log` VALUES (77, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:39:14', '2022-02-01 09:39:14');
INSERT INTO `admin_operation_log` VALUES (78, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:39:25', '2022-02-01 09:39:25');
INSERT INTO `admin_operation_log` VALUES (79, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:39:26', '2022-02-01 09:39:26');
INSERT INTO `admin_operation_log` VALUES (80, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:40:12', '2022-02-01 09:40:12');
INSERT INTO `admin_operation_log` VALUES (81, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:40:13', '2022-02-01 09:40:13');
INSERT INTO `admin_operation_log` VALUES (82, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:40:14', '2022-02-01 09:40:14');
INSERT INTO `admin_operation_log` VALUES (83, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:40:14', '2022-02-01 09:40:14');
INSERT INTO `admin_operation_log` VALUES (84, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:40:15', '2022-02-01 09:40:15');
INSERT INTO `admin_operation_log` VALUES (85, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:40:37', '2022-02-01 09:40:37');
INSERT INTO `admin_operation_log` VALUES (86, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:40:54', '2022-02-01 09:40:54');
INSERT INTO `admin_operation_log` VALUES (87, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:42:04', '2022-02-01 09:42:04');
INSERT INTO `admin_operation_log` VALUES (88, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:42:05', '2022-02-01 09:42:05');
INSERT INTO `admin_operation_log` VALUES (89, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:42:05', '2022-02-01 09:42:05');
INSERT INTO `admin_operation_log` VALUES (90, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:42:05', '2022-02-01 09:42:05');
INSERT INTO `admin_operation_log` VALUES (91, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:42:05', '2022-02-01 09:42:05');
INSERT INTO `admin_operation_log` VALUES (92, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 09:42:06', '2022-02-01 09:42:06');
INSERT INTO `admin_operation_log` VALUES (93, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 09:42:08', '2022-02-01 09:42:08');
INSERT INTO `admin_operation_log` VALUES (94, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 09:42:10', '2022-02-01 09:42:10');
INSERT INTO `admin_operation_log` VALUES (95, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 09:42:11', '2022-02-01 09:42:11');
INSERT INTO `admin_operation_log` VALUES (96, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 09:42:11', '2022-02-01 09:42:11');
INSERT INTO `admin_operation_log` VALUES (97, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 09:42:12', '2022-02-01 09:42:12');
INSERT INTO `admin_operation_log` VALUES (98, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 09:42:20', '2022-02-01 09:42:20');
INSERT INTO `admin_operation_log` VALUES (99, 1, 'admin/auth/menu/8', 'DELETE', '127.0.0.1', '{\"_method\":\"delete\",\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\"}', '2022-02-01 09:42:39', '2022-02-01 09:42:39');
INSERT INTO `admin_operation_log` VALUES (100, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 09:42:39', '2022-02-01 09:42:39');
INSERT INTO `admin_operation_log` VALUES (101, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 17:43:58', '2022-02-01 17:43:58');
INSERT INTO `admin_operation_log` VALUES (102, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:44:00', '2022-02-01 17:44:00');
INSERT INTO `admin_operation_log` VALUES (103, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:44:01', '2022-02-01 17:44:01');
INSERT INTO `admin_operation_log` VALUES (104, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:44:04', '2022-02-01 17:44:04');
INSERT INTO `admin_operation_log` VALUES (105, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:44:16', '2022-02-01 17:44:16');
INSERT INTO `admin_operation_log` VALUES (106, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:44:37', '2022-02-01 17:44:37');
INSERT INTO `admin_operation_log` VALUES (107, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:44:38', '2022-02-01 17:44:38');
INSERT INTO `admin_operation_log` VALUES (108, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:44:38', '2022-02-01 17:44:38');
INSERT INTO `admin_operation_log` VALUES (109, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:44:39', '2022-02-01 17:44:39');
INSERT INTO `admin_operation_log` VALUES (110, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:44:41', '2022-02-01 17:44:41');
INSERT INTO `admin_operation_log` VALUES (111, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:44:41', '2022-02-01 17:44:41');
INSERT INTO `admin_operation_log` VALUES (112, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:44:43', '2022-02-01 17:44:43');
INSERT INTO `admin_operation_log` VALUES (113, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:44:44', '2022-02-01 17:44:44');
INSERT INTO `admin_operation_log` VALUES (114, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:44:45', '2022-02-01 17:44:45');
INSERT INTO `admin_operation_log` VALUES (115, 1, 'admin/auth/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:44:54', '2022-02-01 17:44:54');
INSERT INTO `admin_operation_log` VALUES (116, 1, 'admin/auth/users', 'POST', '127.0.0.1', '{\"username\":\"root\",\"name\":\"\\u7ba1\\u7406\\u8005\",\"password\":\"tooy\",\"password_confirmation\":\"root\",\"roles\":[\"1\",null],\"permissions\":[\"1\",null],\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/auth\\/users\"}', '2022-02-01 17:45:37', '2022-02-01 17:45:37');
INSERT INTO `admin_operation_log` VALUES (117, 1, 'admin/auth/users/create', 'GET', '127.0.0.1', '[]', '2022-02-01 17:45:37', '2022-02-01 17:45:37');
INSERT INTO `admin_operation_log` VALUES (118, 1, 'admin/auth/users', 'POST', '127.0.0.1', '{\"username\":\"root\",\"name\":\"\\u7ba1\\u7406\\u8005\",\"password\":\"1qaz@WSX\",\"password_confirmation\":\"1qaz@WSX\",\"roles\":[\"1\",null],\"permissions\":[\"1\",null],\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\"}', '2022-02-01 17:45:49', '2022-02-01 17:45:49');
INSERT INTO `admin_operation_log` VALUES (119, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-01 17:45:49', '2022-02-01 17:45:49');
INSERT INTO `admin_operation_log` VALUES (120, 2, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 17:46:07', '2022-02-01 17:46:07');
INSERT INTO `admin_operation_log` VALUES (121, 2, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:46:09', '2022-02-01 17:46:09');
INSERT INTO `admin_operation_log` VALUES (122, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:46:17', '2022-02-01 17:46:17');
INSERT INTO `admin_operation_log` VALUES (123, 1, 'admin/auth/roles/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:46:19', '2022-02-01 17:46:19');
INSERT INTO `admin_operation_log` VALUES (124, 1, 'admin/auth/roles', 'POST', '127.0.0.1', '{\"slug\":\"system\",\"name\":\"\\u7db2\\u7ad9\\u7ba1\\u7406\\u54e1\",\"permissions\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",null],\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/auth\\/roles\"}', '2022-02-01 17:46:45', '2022-02-01 17:46:45');
INSERT INTO `admin_operation_log` VALUES (125, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '[]', '2022-02-01 17:46:45', '2022-02-01 17:46:45');
INSERT INTO `admin_operation_log` VALUES (126, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:46:52', '2022-02-01 17:46:52');
INSERT INTO `admin_operation_log` VALUES (127, 1, 'admin/auth/users/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:46:55', '2022-02-01 17:46:55');
INSERT INTO `admin_operation_log` VALUES (128, 1, 'admin/auth/users/2', 'PUT', '127.0.0.1', '{\"username\":\"root\",\"name\":\"\\u7ba1\\u7406\\u8005\",\"password\":\"$2y$10$vwi\\/1X33SL4yJYBLuxI\\/m.rW1P72mY6FToynvg2k7jOQLYQf74Vtm\",\"password_confirmation\":\"$2y$10$vwi\\/1X33SL4yJYBLuxI\\/m.rW1P72mY6FToynvg2k7jOQLYQf74Vtm\",\"roles\":[\"2\",null],\"permissions\":[\"1\",null],\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/auth\\/users\"}', '2022-02-01 17:47:16', '2022-02-01 17:47:16');
INSERT INTO `admin_operation_log` VALUES (129, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-01 17:47:16', '2022-02-01 17:47:16');
INSERT INTO `admin_operation_log` VALUES (130, 2, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 17:47:18', '2022-02-01 17:47:18');
INSERT INTO `admin_operation_log` VALUES (131, 2, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 17:47:19', '2022-02-01 17:47:19');
INSERT INTO `admin_operation_log` VALUES (132, 2, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:47:20', '2022-02-01 17:47:20');
INSERT INTO `admin_operation_log` VALUES (133, 2, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:47:24', '2022-02-01 17:47:24');
INSERT INTO `admin_operation_log` VALUES (134, 2, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:47:26', '2022-02-01 17:47:26');
INSERT INTO `admin_operation_log` VALUES (135, 2, 'admin/helpers/terminal/database', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:47:27', '2022-02-01 17:47:27');
INSERT INTO `admin_operation_log` VALUES (136, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:47:34', '2022-02-01 17:47:34');
INSERT INTO `admin_operation_log` VALUES (137, 1, 'admin/auth/roles/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:47:40', '2022-02-01 17:47:40');
INSERT INTO `admin_operation_log` VALUES (138, 1, 'admin/auth/roles/2', 'PUT', '127.0.0.1', '{\"slug\":\"system\",\"name\":\"\\u7db2\\u7ad9\\u7ba1\\u7406\\u54e1\",\"permissions\":[\"1\",\"2\",\"3\",\"4\",\"5\",null],\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/auth\\/roles\"}', '2022-02-01 17:48:25', '2022-02-01 17:48:25');
INSERT INTO `admin_operation_log` VALUES (139, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '[]', '2022-02-01 17:48:25', '2022-02-01 17:48:25');
INSERT INTO `admin_operation_log` VALUES (140, 2, 'admin/helpers/terminal/database', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:48:31', '2022-02-01 17:48:31');
INSERT INTO `admin_operation_log` VALUES (141, 2, 'admin/helpers/terminal/database', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:48:33', '2022-02-01 17:48:33');
INSERT INTO `admin_operation_log` VALUES (142, 2, 'admin/helpers/terminal/database', 'GET', '127.0.0.1', '[]', '2022-02-01 17:48:35', '2022-02-01 17:48:35');
INSERT INTO `admin_operation_log` VALUES (143, 2, 'admin/helpers/terminal/database', 'GET', '127.0.0.1', '[]', '2022-02-01 17:48:36', '2022-02-01 17:48:36');
INSERT INTO `admin_operation_log` VALUES (144, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:48:52', '2022-02-01 17:48:52');
INSERT INTO `admin_operation_log` VALUES (145, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:48:55', '2022-02-01 17:48:55');
INSERT INTO `admin_operation_log` VALUES (146, 1, 'admin/auth/menu/9/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:48:59', '2022-02-01 17:48:59');
INSERT INTO `admin_operation_log` VALUES (147, 1, 'admin/auth/menu/9', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"Helpers\",\"icon\":\"fa-gears\",\"uri\":null,\"roles\":[\"1\",null],\"permission\":\"*\",\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/auth\\/menu\"}', '2022-02-01 17:49:08', '2022-02-01 17:49:08');
INSERT INTO `admin_operation_log` VALUES (148, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 17:49:09', '2022-02-01 17:49:09');
INSERT INTO `admin_operation_log` VALUES (149, 2, 'admin/helpers/terminal/database', 'GET', '127.0.0.1', '[]', '2022-02-01 17:49:11', '2022-02-01 17:49:11');
INSERT INTO `admin_operation_log` VALUES (150, 2, 'admin/helpers/terminal/database', 'GET', '127.0.0.1', '[]', '2022-02-01 17:49:12', '2022-02-01 17:49:12');
INSERT INTO `admin_operation_log` VALUES (151, 2, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:49:13', '2022-02-01 17:49:13');
INSERT INTO `admin_operation_log` VALUES (152, 2, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:49:14', '2022-02-01 17:49:14');
INSERT INTO `admin_operation_log` VALUES (153, 2, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:49:19', '2022-02-01 17:49:19');
INSERT INTO `admin_operation_log` VALUES (154, 2, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:49:23', '2022-02-01 17:49:23');
INSERT INTO `admin_operation_log` VALUES (155, 1, 'admin/auth/menu/3/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:49:32', '2022-02-01 17:49:32');
INSERT INTO `admin_operation_log` VALUES (156, 1, 'admin/auth/menu/3', 'PUT', '127.0.0.1', '{\"parent_id\":\"2\",\"title\":\"\\u4f7f\\u7528\\u8005\\u7ba1\\u7406\",\"icon\":\"fa-users\",\"uri\":\"auth\\/users\",\"roles\":[\"1\",\"2\",null],\"permission\":\"*\",\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/auth\\/menu\"}', '2022-02-01 17:49:52', '2022-02-01 17:49:52');
INSERT INTO `admin_operation_log` VALUES (157, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 17:49:52', '2022-02-01 17:49:52');
INSERT INTO `admin_operation_log` VALUES (158, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:49:56', '2022-02-01 17:49:56');
INSERT INTO `admin_operation_log` VALUES (159, 1, 'admin/auth/users/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:50:07', '2022-02-01 17:50:07');
INSERT INTO `admin_operation_log` VALUES (160, 1, 'admin/auth/users/2', 'PUT', '127.0.0.1', '{\"username\":\"system\",\"name\":\"\\u7ba1\\u7406\\u8005\",\"password\":\"$2y$10$vwi\\/1X33SL4yJYBLuxI\\/m.rW1P72mY6FToynvg2k7jOQLYQf74Vtm\",\"password_confirmation\":\"$2y$10$vwi\\/1X33SL4yJYBLuxI\\/m.rW1P72mY6FToynvg2k7jOQLYQf74Vtm\",\"roles\":[\"2\",null],\"permissions\":[\"1\",null],\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/auth\\/users\"}', '2022-02-01 17:50:18', '2022-02-01 17:50:18');
INSERT INTO `admin_operation_log` VALUES (161, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-01 17:50:18', '2022-02-01 17:50:18');
INSERT INTO `admin_operation_log` VALUES (162, 1, 'admin/auth/users/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 17:50:28', '2022-02-01 17:50:28');
INSERT INTO `admin_operation_log` VALUES (163, 1, 'admin/auth/users/2', 'PUT', '127.0.0.1', '{\"username\":\"system\",\"name\":\"\\u7cfb\\u7d71\\u7ba1\\u7406\\u8005\",\"password\":\"$2y$10$vwi\\/1X33SL4yJYBLuxI\\/m.rW1P72mY6FToynvg2k7jOQLYQf74Vtm\",\"password_confirmation\":\"$2y$10$vwi\\/1X33SL4yJYBLuxI\\/m.rW1P72mY6FToynvg2k7jOQLYQf74Vtm\",\"roles\":[\"2\",null],\"permissions\":[\"1\",null],\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/auth\\/users\"}', '2022-02-01 17:50:34', '2022-02-01 17:50:34');
INSERT INTO `admin_operation_log` VALUES (164, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-01 17:50:34', '2022-02-01 17:50:34');
INSERT INTO `admin_operation_log` VALUES (165, 2, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 17:50:37', '2022-02-01 17:50:37');
INSERT INTO `admin_operation_log` VALUES (166, 2, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 17:50:38', '2022-02-01 17:50:38');
INSERT INTO `admin_operation_log` VALUES (167, 2, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 17:51:10', '2022-02-01 17:51:10');
INSERT INTO `admin_operation_log` VALUES (168, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:00:15', '2022-02-01 18:00:15');
INSERT INTO `admin_operation_log` VALUES (169, 1, 'admin/helpers/terminal/database', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:00:16', '2022-02-01 18:00:16');
INSERT INTO `admin_operation_log` VALUES (170, 1, 'admin/helpers/terminal/database', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:00:17', '2022-02-01 18:00:17');
INSERT INTO `admin_operation_log` VALUES (171, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:00:17', '2022-02-01 18:00:17');
INSERT INTO `admin_operation_log` VALUES (172, 1, 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"supports\",\"model_name\":\"App\\\\Models\\\\Support\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\SupportController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"id\",\"type\":\"string\",\"key\":\"index\",\"default\":null,\"comment\":null},{\"name\":\"news\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":\"\\u6700\\u65b0\\u6d88\\u606f\"},{\"name\":\"server_rule\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":\"\\u670d\\u52d9\\u689d\\u6b3e\"},{\"name\":\"pay_desc\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":\"\\u6536\\u8cbb\\u8aaa\\u660e\"},{\"name\":null,\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\"}', '2022-02-01 18:08:49', '2022-02-01 18:08:49');
INSERT INTO `admin_operation_log` VALUES (173, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2022-02-01 18:08:49', '2022-02-01 18:08:49');
INSERT INTO `admin_operation_log` VALUES (174, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2022-02-01 18:11:22', '2022-02-01 18:11:22');
INSERT INTO `admin_operation_log` VALUES (175, 1, 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"supports\",\"model_name\":\"App\\\\Models\\\\Support\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\SupportController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"id\",\"type\":\"string\",\"key\":\"index\",\"default\":null,\"comment\":null},{\"name\":\"news\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":\"\\u6700\\u65b0\\u6d88\\u606f\"},{\"name\":\"server_rule\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":\"\\u670d\\u52d9\\u689d\\u6b3e\"},{\"name\":\"pay_desc\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":\"\\u6536\\u8cbb\\u8aaa\\u660e\"}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\"}', '2022-02-01 18:12:25', '2022-02-01 18:12:25');
INSERT INTO `admin_operation_log` VALUES (176, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2022-02-01 18:12:25', '2022-02-01 18:12:25');
INSERT INTO `admin_operation_log` VALUES (177, 1, 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"supports\",\"model_name\":\"App\\\\Models\\\\Support\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\SupportController\",\"create\":[\"migration\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"id\",\"type\":\"string\",\"key\":\"index\",\"default\":null,\"comment\":null},{\"name\":\"news\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":\"\\u6700\\u65b0\\u6d88\\u606f\"},{\"name\":\"server_rule\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":\"\\u670d\\u52d9\\u689d\\u6b3e\"},{\"name\":\"pay_desc\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":\"\\u6536\\u8cbb\\u8aaa\\u660e\"}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\"}', '2022-02-01 18:12:58', '2022-02-01 18:12:58');
INSERT INTO `admin_operation_log` VALUES (178, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2022-02-01 18:12:58', '2022-02-01 18:12:58');
INSERT INTO `admin_operation_log` VALUES (179, 1, 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"supports\",\"model_name\":\"App\\\\Models\\\\Support\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\SupportController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"id\",\"type\":\"string\",\"key\":\"index\",\"default\":null,\"comment\":null},{\"name\":\"news\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":\"\\u6700\\u65b0\\u6d88\\u606f\"},{\"name\":\"server_rule\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":\"\\u670d\\u52d9\\u689d\\u6b3e\"},{\"name\":\"pay_desc\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":\"\\u6536\\u8cbb\\u8aaa\\u660e\"}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\"}', '2022-02-01 18:13:16', '2022-02-01 18:13:16');
INSERT INTO `admin_operation_log` VALUES (180, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2022-02-01 18:13:16', '2022-02-01 18:13:16');
INSERT INTO `admin_operation_log` VALUES (181, 1, 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"supports\",\"model_name\":\"App\\\\Models\\\\Support\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\SupportController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"id\",\"type\":\"string\",\"key\":\"unique\",\"default\":null,\"comment\":null},{\"name\":\"news\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":\"\\u6700\\u65b0\\u6d88\\u606f\"},{\"name\":\"server_rule\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":\"\\u670d\\u52d9\\u689d\\u6b3e\"},{\"name\":\"pay_desc\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":\"\\u6536\\u8cbb\\u8aaa\\u660e\"}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\"}', '2022-02-01 18:13:59', '2022-02-01 18:13:59');
INSERT INTO `admin_operation_log` VALUES (182, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2022-02-01 18:13:59', '2022-02-01 18:13:59');
INSERT INTO `admin_operation_log` VALUES (183, 1, 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"supports\",\"model_name\":\"App\\\\Models\\\\Support\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\SupportController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"id\",\"type\":\"string\",\"nullable\":\"on\",\"key\":\"index\",\"default\":null,\"comment\":null},{\"name\":\"news\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":\"\\u6700\\u65b0\\u6d88\\u606f\"},{\"name\":\"server_rule\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":\"\\u670d\\u52d9\\u689d\\u6b3e\"},{\"name\":\"pay_desc\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":\"\\u6536\\u8cbb\\u8aaa\\u660e\"}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\"}', '2022-02-01 18:14:08', '2022-02-01 18:14:08');
INSERT INTO `admin_operation_log` VALUES (184, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2022-02-01 18:14:09', '2022-02-01 18:14:09');
INSERT INTO `admin_operation_log` VALUES (185, 1, 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"supports\",\"model_name\":\"App\\\\Models\\\\Support\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\SupportController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"id\",\"type\":\"string\",\"key\":\"index\",\"default\":null,\"comment\":null},{\"name\":\"news\",\"type\":\"string\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":\"\\u6700\\u65b0\\u6d88\\u606f\"},{\"name\":\"server_rule\",\"type\":\"text\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":\"\\u670d\\u52d9\\u689d\\u6b3e\"},{\"name\":\"pay_desc\",\"type\":\"text\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":\"\\u6536\\u8cbb\\u8aaa\\u660e\"}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\"}', '2022-02-01 18:14:28', '2022-02-01 18:14:28');
INSERT INTO `admin_operation_log` VALUES (186, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2022-02-01 18:14:29', '2022-02-01 18:14:29');
INSERT INTO `admin_operation_log` VALUES (187, 1, 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"supports\",\"model_name\":\"App\\\\Models\\\\Support\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\SupportController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":{\"1\":{\"name\":\"news\",\"type\":\"string\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":\"\\u6700\\u65b0\\u6d88\\u606f\"},\"2\":{\"name\":\"server_rule\",\"type\":\"text\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":\"\\u670d\\u52d9\\u689d\\u6b3e\"},\"3\":{\"name\":\"pay_desc\",\"type\":\"text\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":\"\\u6536\\u8cbb\\u8aaa\\u660e\"}},\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\"}', '2022-02-01 18:14:46', '2022-02-01 18:14:46');
INSERT INTO `admin_operation_log` VALUES (188, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2022-02-01 18:14:47', '2022-02-01 18:14:47');
INSERT INTO `admin_operation_log` VALUES (189, 1, 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"ads\",\"model_name\":\"App\\\\Models\\\\AD\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\AdController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"support_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"url\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":\"\\u7db2\\u5740\"},{\"name\":\"ad_order\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":\"\\u6392\\u5e8f\"}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\"}', '2022-02-01 18:19:16', '2022-02-01 18:19:16');
INSERT INTO `admin_operation_log` VALUES (190, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2022-02-01 18:19:16', '2022-02-01 18:19:16');
INSERT INTO `admin_operation_log` VALUES (191, 1, 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"disconuts\",\"model_name\":\"App\\\\Models\\\\Disconut\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\DisconutController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"support_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"code\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":\"\\u6298\\u6263\\u4ee3\\u78bc\"},{\"name\":\"start_date\",\"type\":\"date\",\"key\":null,\"default\":null,\"comment\":\"\\u6709\\u6548\\u958b\\u59cb\\u65e5\\u671f\"},{\"name\":\"end_date\",\"type\":\"date\",\"key\":null,\"default\":null,\"comment\":\"\\u6709\\u6548\\u7d50\\u675f\\u65e5\\u671f\"},{\"name\":\"price\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":\"\\u6298\\u6263\\u91d1\\u984d\"}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\"}', '2022-02-01 18:22:34', '2022-02-01 18:22:34');
INSERT INTO `admin_operation_log` VALUES (192, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2022-02-01 18:22:34', '2022-02-01 18:22:34');
INSERT INTO `admin_operation_log` VALUES (193, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 18:24:25', '2022-02-01 18:24:25');
INSERT INTO `admin_operation_log` VALUES (194, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 18:26:42', '2022-02-01 18:26:42');
INSERT INTO `admin_operation_log` VALUES (195, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 18:26:43', '2022-02-01 18:26:43');
INSERT INTO `admin_operation_log` VALUES (196, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:26:52', '2022-02-01 18:26:52');
INSERT INTO `admin_operation_log` VALUES (197, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:27:49', '2022-02-01 18:27:49');
INSERT INTO `admin_operation_log` VALUES (198, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:27:52', '2022-02-01 18:27:52');
INSERT INTO `admin_operation_log` VALUES (199, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:27:54', '2022-02-01 18:27:54');
INSERT INTO `admin_operation_log` VALUES (200, 1, 'admin/auth/menu/1', 'DELETE', '127.0.0.1', '{\"_method\":\"delete\",\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\"}', '2022-02-01 18:27:59', '2022-02-01 18:27:59');
INSERT INTO `admin_operation_log` VALUES (201, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:27:59', '2022-02-01 18:27:59');
INSERT INTO `admin_operation_log` VALUES (202, 1, 'admin/auth/menu/2', 'DELETE', '127.0.0.1', '{\"_method\":\"delete\",\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\"}', '2022-02-01 18:28:49', '2022-02-01 18:28:49');
INSERT INTO `admin_operation_log` VALUES (203, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:28:49', '2022-02-01 18:28:49');
INSERT INTO `admin_operation_log` VALUES (204, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 18:28:55', '2022-02-01 18:28:55');
INSERT INTO `admin_operation_log` VALUES (205, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 18:28:57', '2022-02-01 18:28:57');
INSERT INTO `admin_operation_log` VALUES (206, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 18:34:00', '2022-02-01 18:34:00');
INSERT INTO `admin_operation_log` VALUES (207, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:34:10', '2022-02-01 18:34:10');
INSERT INTO `admin_operation_log` VALUES (208, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 18:35:31', '2022-02-01 18:35:31');
INSERT INTO `admin_operation_log` VALUES (209, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 18:35:32', '2022-02-01 18:35:32');
INSERT INTO `admin_operation_log` VALUES (210, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:35:33', '2022-02-01 18:35:33');
INSERT INTO `admin_operation_log` VALUES (211, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:35:35', '2022-02-01 18:35:35');
INSERT INTO `admin_operation_log` VALUES (212, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:35:36', '2022-02-01 18:35:36');
INSERT INTO `admin_operation_log` VALUES (213, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:35:38', '2022-02-01 18:35:38');
INSERT INTO `admin_operation_log` VALUES (214, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:35:38', '2022-02-01 18:35:38');
INSERT INTO `admin_operation_log` VALUES (215, 1, 'admin/auth/menu/3/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:35:53', '2022-02-01 18:35:53');
INSERT INTO `admin_operation_log` VALUES (216, 1, 'admin/auth/menu/3', 'PUT', '127.0.0.1', '{\"parent_id\":\"2\",\"title\":\"\\u4f7f\\u7528\\u8005\\u7ba1\\u7406\",\"icon\":\"fa-users\",\"uri\":\"auth\\/users\",\"roles\":[\"1\",\"2\",null],\"permission\":null,\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/auth\\/menu\"}', '2022-02-01 18:36:03', '2022-02-01 18:36:03');
INSERT INTO `admin_operation_log` VALUES (217, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 18:36:04', '2022-02-01 18:36:04');
INSERT INTO `admin_operation_log` VALUES (218, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 18:36:06', '2022-02-01 18:36:06');
INSERT INTO `admin_operation_log` VALUES (219, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 18:36:09', '2022-02-01 18:36:09');
INSERT INTO `admin_operation_log` VALUES (220, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\",\"_order\":\"[{\\\"id\\\":3},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7}]},{\\\"id\\\":9,\\\"children\\\":[{\\\"id\\\":10},{\\\"id\\\":11},{\\\"id\\\":12},{\\\"id\\\":13}]}]\"}', '2022-02-01 18:36:15', '2022-02-01 18:36:15');
INSERT INTO `admin_operation_log` VALUES (221, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:36:15', '2022-02-01 18:36:15');
INSERT INTO `admin_operation_log` VALUES (222, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:36:27', '2022-02-01 18:36:27');
INSERT INTO `admin_operation_log` VALUES (223, 1, 'admin/auth/roles/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:36:30', '2022-02-01 18:36:30');
INSERT INTO `admin_operation_log` VALUES (224, 1, 'admin/auth/roles', 'POST', '127.0.0.1', '{\"slug\":\"user\",\"name\":\"\\u4f7f\\u7528\\u8005\",\"permissions\":[\"1\",null],\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/auth\\/roles\"}', '2022-02-01 18:38:08', '2022-02-01 18:38:08');
INSERT INTO `admin_operation_log` VALUES (225, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '[]', '2022-02-01 18:38:08', '2022-02-01 18:38:08');
INSERT INTO `admin_operation_log` VALUES (226, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:38:19', '2022-02-01 18:38:19');
INSERT INTO `admin_operation_log` VALUES (227, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:38:21', '2022-02-01 18:38:21');
INSERT INTO `admin_operation_log` VALUES (228, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:38:22', '2022-02-01 18:38:22');
INSERT INTO `admin_operation_log` VALUES (229, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:38:23', '2022-02-01 18:38:23');
INSERT INTO `admin_operation_log` VALUES (230, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:38:24', '2022-02-01 18:38:24');
INSERT INTO `admin_operation_log` VALUES (231, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:38:50', '2022-02-01 18:38:50');
INSERT INTO `admin_operation_log` VALUES (232, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:38:51', '2022-02-01 18:38:51');
INSERT INTO `admin_operation_log` VALUES (233, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:38:52', '2022-02-01 18:38:52');
INSERT INTO `admin_operation_log` VALUES (234, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:38:53', '2022-02-01 18:38:53');
INSERT INTO `admin_operation_log` VALUES (235, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:38:53', '2022-02-01 18:38:53');
INSERT INTO `admin_operation_log` VALUES (236, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:38:54', '2022-02-01 18:38:54');
INSERT INTO `admin_operation_log` VALUES (237, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:38:57', '2022-02-01 18:38:57');
INSERT INTO `admin_operation_log` VALUES (238, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:38:59', '2022-02-01 18:38:59');
INSERT INTO `admin_operation_log` VALUES (239, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:39:00', '2022-02-01 18:39:00');
INSERT INTO `admin_operation_log` VALUES (240, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:39:01', '2022-02-01 18:39:01');
INSERT INTO `admin_operation_log` VALUES (241, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 18:39:04', '2022-02-01 18:39:04');
INSERT INTO `admin_operation_log` VALUES (242, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:39:08', '2022-02-01 18:39:08');
INSERT INTO `admin_operation_log` VALUES (243, 1, 'admin/auth/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:39:17', '2022-02-01 18:39:17');
INSERT INTO `admin_operation_log` VALUES (244, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:39:33', '2022-02-01 18:39:33');
INSERT INTO `admin_operation_log` VALUES (245, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:39:36', '2022-02-01 18:39:36');
INSERT INTO `admin_operation_log` VALUES (246, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-01 18:39:38', '2022-02-01 18:39:38');
INSERT INTO `admin_operation_log` VALUES (247, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:39:40', '2022-02-01 18:39:40');
INSERT INTO `admin_operation_log` VALUES (248, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:39:41', '2022-02-01 18:39:41');
INSERT INTO `admin_operation_log` VALUES (249, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:39:42', '2022-02-01 18:39:42');
INSERT INTO `admin_operation_log` VALUES (250, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:39:45', '2022-02-01 18:39:45');
INSERT INTO `admin_operation_log` VALUES (251, 1, 'admin/helpers/terminal/database', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:39:46', '2022-02-01 18:39:46');
INSERT INTO `admin_operation_log` VALUES (252, 1, 'admin/helpers/terminal/artisan', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:39:47', '2022-02-01 18:39:47');
INSERT INTO `admin_operation_log` VALUES (253, 1, 'admin/helpers/routes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:39:48', '2022-02-01 18:39:48');
INSERT INTO `admin_operation_log` VALUES (254, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:39:50', '2022-02-01 18:39:50');
INSERT INTO `admin_operation_log` VALUES (255, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:39:52', '2022-02-01 18:39:52');
INSERT INTO `admin_operation_log` VALUES (256, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:40:03', '2022-02-01 18:40:03');
INSERT INTO `admin_operation_log` VALUES (257, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:40:04', '2022-02-01 18:40:04');
INSERT INTO `admin_operation_log` VALUES (258, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u9053\\u8def\\u6551\\u63f4\\u5e73\\u53f0\\u7ba1\\u7406\",\"icon\":\"fa-bars\",\"uri\":\"\\/support\",\"roles\":[\"1\",\"2\",\"3\",null],\"permission\":\"*\",\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\"}', '2022-02-01 18:40:55', '2022-02-01 18:40:55');
INSERT INTO `admin_operation_log` VALUES (259, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 18:40:55', '2022-02-01 18:40:55');
INSERT INTO `admin_operation_log` VALUES (260, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 18:41:05', '2022-02-01 18:41:05');
INSERT INTO `admin_operation_log` VALUES (261, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u8a02\\u55ae\\u7ba1\\u7406\",\"icon\":\"fa-book\",\"uri\":\"\\/order\",\"roles\":[\"1\",\"2\",\"3\",null],\"permission\":\"*\",\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\"}', '2022-02-01 18:44:05', '2022-02-01 18:44:05');
INSERT INTO `admin_operation_log` VALUES (262, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 18:44:06', '2022-02-01 18:44:06');
INSERT INTO `admin_operation_log` VALUES (263, 1, 'admin/auth/menu/14/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:44:12', '2022-02-01 18:44:12');
INSERT INTO `admin_operation_log` VALUES (264, 1, 'admin/auth/menu/14', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u9053\\u8def\\u6551\\u63f4\\u5e73\\u53f0\\u7ba1\\u7406\",\"icon\":\"fa-automobile\",\"uri\":\"\\/support\",\"roles\":[\"1\",\"2\",\"3\",null],\"permission\":\"*\",\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/auth\\/menu\"}', '2022-02-01 18:44:21', '2022-02-01 18:44:21');
INSERT INTO `admin_operation_log` VALUES (265, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 18:44:21', '2022-02-01 18:44:21');
INSERT INTO `admin_operation_log` VALUES (266, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 18:44:23', '2022-02-01 18:44:23');
INSERT INTO `admin_operation_log` VALUES (267, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u512a\\u60e0\\u78bc\\u7ba1\\u7406\",\"icon\":\"fa-bars\",\"uri\":\"disconut\",\"roles\":[\"1\",\"2\",\"3\",null],\"permission\":\"*\",\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\"}', '2022-02-01 18:45:26', '2022-02-01 18:45:26');
INSERT INTO `admin_operation_log` VALUES (268, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 18:45:27', '2022-02-01 18:45:27');
INSERT INTO `admin_operation_log` VALUES (269, 1, 'admin/auth/menu/16/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:46:06', '2022-02-01 18:46:06');
INSERT INTO `admin_operation_log` VALUES (270, 1, 'admin/auth/menu/16', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u512a\\u60e0\\u78bc\\u7ba1\\u7406\",\"icon\":\"fa-yen\",\"uri\":\"disconut\",\"roles\":[\"1\",\"2\",\"3\",null],\"permission\":\"*\",\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/auth\\/menu\"}', '2022-02-01 18:47:33', '2022-02-01 18:47:33');
INSERT INTO `admin_operation_log` VALUES (271, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 18:47:33', '2022-02-01 18:47:33');
INSERT INTO `admin_operation_log` VALUES (272, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 18:47:36', '2022-02-01 18:47:36');
INSERT INTO `admin_operation_log` VALUES (273, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\",\"_order\":\"[{\\\"id\\\":14},{\\\"id\\\":16},{\\\"id\\\":15},{\\\"id\\\":3},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7}]},{\\\"id\\\":9,\\\"children\\\":[{\\\"id\\\":10},{\\\"id\\\":11},{\\\"id\\\":12},{\\\"id\\\":13}]}]\"}', '2022-02-01 18:47:46', '2022-02-01 18:47:46');
INSERT INTO `admin_operation_log` VALUES (274, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:47:47', '2022-02-01 18:47:47');
INSERT INTO `admin_operation_log` VALUES (275, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 18:47:50', '2022-02-01 18:47:50');
INSERT INTO `admin_operation_log` VALUES (276, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 18:49:18', '2022-02-01 18:49:18');
INSERT INTO `admin_operation_log` VALUES (277, 1, 'admin/support', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:49:21', '2022-02-01 18:49:21');
INSERT INTO `admin_operation_log` VALUES (278, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-01 18:49:24', '2022-02-01 18:49:24');
INSERT INTO `admin_operation_log` VALUES (279, 1, 'admin/support', 'GET', '127.0.0.1', '[]', '2022-02-01 18:49:24', '2022-02-01 18:49:24');
INSERT INTO `admin_operation_log` VALUES (280, 1, 'admin/disconut', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:49:27', '2022-02-01 18:49:27');
INSERT INTO `admin_operation_log` VALUES (281, 1, 'admin/disconut', 'GET', '127.0.0.1', '[]', '2022-02-01 18:49:30', '2022-02-01 18:49:30');
INSERT INTO `admin_operation_log` VALUES (282, 1, 'admin/support', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 18:49:32', '2022-02-01 18:49:32');
INSERT INTO `admin_operation_log` VALUES (283, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 19:17:13', '2022-02-01 19:17:13');
INSERT INTO `admin_operation_log` VALUES (284, 1, 'admin/auth/menu/14/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 19:17:16', '2022-02-01 19:17:16');
INSERT INTO `admin_operation_log` VALUES (285, 1, 'admin/auth/menu/14', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u5408\\u4f5c\\u901a\\u8def\\u7ba1\\u7406\",\"icon\":\"fa-automobile\",\"uri\":\"\\/support\",\"roles\":[\"1\",\"2\",\"3\",null],\"permission\":\"*\",\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/auth\\/menu\"}', '2022-02-01 19:17:26', '2022-02-01 19:17:26');
INSERT INTO `admin_operation_log` VALUES (286, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 19:17:27', '2022-02-01 19:17:27');
INSERT INTO `admin_operation_log` VALUES (287, 1, 'admin/support', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 19:17:40', '2022-02-01 19:17:40');
INSERT INTO `admin_operation_log` VALUES (288, 1, 'admin/support', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 19:17:41', '2022-02-01 19:17:41');
INSERT INTO `admin_operation_log` VALUES (289, 1, 'admin/disconut', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 19:17:43', '2022-02-01 19:17:43');
INSERT INTO `admin_operation_log` VALUES (290, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 19:17:48', '2022-02-01 19:17:48');
INSERT INTO `admin_operation_log` VALUES (291, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u8dd1\\u99ac\\u71c8\\u8a0a\\u606f\\u7ba1\\u7406\",\"icon\":\"fa-bars\",\"uri\":\"\\/news\",\"roles\":[\"1\",\"2\",\"3\",null],\"permission\":\"*\",\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\"}', '2022-02-01 19:18:24', '2022-02-01 19:18:24');
INSERT INTO `admin_operation_log` VALUES (292, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 19:18:24', '2022-02-01 19:18:24');
INSERT INTO `admin_operation_log` VALUES (293, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u6551\\u63f4\\u670d\\u52d9\\u689d\\u6b3e\\u4e0a\\u67b6\\u7ba1\\u7406\",\"icon\":\"fa-bars\",\"uri\":null,\"roles\":[\"1\",\"2\",\"3\",null],\"permission\":\"*\",\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\"}', '2022-02-01 19:20:10', '2022-02-01 19:20:10');
INSERT INTO `admin_operation_log` VALUES (294, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 19:20:10', '2022-02-01 19:20:10');
INSERT INTO `admin_operation_log` VALUES (295, 1, 'admin/auth/menu/17/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 19:20:23', '2022-02-01 19:20:23');
INSERT INTO `admin_operation_log` VALUES (296, 1, 'admin/auth/menu/17', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u8dd1\\u99ac\\u71c8\\u8a0a\\u606f\\u7ba1\\u7406\",\"icon\":\"fa-bell-o\",\"uri\":\"\\/news\",\"roles\":[\"1\",\"2\",\"3\",null],\"permission\":\"*\",\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/auth\\/menu\"}', '2022-02-01 19:20:39', '2022-02-01 19:20:39');
INSERT INTO `admin_operation_log` VALUES (297, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 19:20:39', '2022-02-01 19:20:39');
INSERT INTO `admin_operation_log` VALUES (298, 1, 'admin/auth/menu/18/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 19:20:50', '2022-02-01 19:20:50');
INSERT INTO `admin_operation_log` VALUES (299, 1, 'admin/auth/menu/18', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u6551\\u63f4\\u670d\\u52d9\\u689d\\u6b3e\\u4e0a\\u67b6\\u7ba1\\u7406\",\"icon\":\"fa-bolt\",\"uri\":null,\"roles\":[\"1\",\"2\",\"3\",null],\"permission\":\"*\",\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/auth\\/menu\"}', '2022-02-01 19:21:20', '2022-02-01 19:21:20');
INSERT INTO `admin_operation_log` VALUES (300, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 19:21:20', '2022-02-01 19:21:20');
INSERT INTO `admin_operation_log` VALUES (301, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\",\"_order\":\"[{\\\"id\\\":17},{\\\"id\\\":18},{\\\"id\\\":14},{\\\"id\\\":16},{\\\"id\\\":15},{\\\"id\\\":3},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7}]},{\\\"id\\\":9,\\\"children\\\":[{\\\"id\\\":10},{\\\"id\\\":11},{\\\"id\\\":12},{\\\"id\\\":13}]}]\"}', '2022-02-01 19:21:23', '2022-02-01 19:21:23');
INSERT INTO `admin_operation_log` VALUES (302, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 19:21:23', '2022-02-01 19:21:23');
INSERT INTO `admin_operation_log` VALUES (303, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 19:21:25', '2022-02-01 19:21:25');
INSERT INTO `admin_operation_log` VALUES (304, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":null,\"icon\":\"fa-bars\",\"uri\":null,\"roles\":[null],\"permission\":null,\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\"}', '2022-02-01 19:21:37', '2022-02-01 19:21:37');
INSERT INTO `admin_operation_log` VALUES (305, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 19:21:37', '2022-02-01 19:21:37');
INSERT INTO `admin_operation_log` VALUES (306, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\",\"_order\":\"[{\\\"id\\\":17},{\\\"id\\\":18},{\\\"id\\\":14},{\\\"id\\\":16},{\\\"id\\\":15},{\\\"id\\\":3},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7}]},{\\\"id\\\":9,\\\"children\\\":[{\\\"id\\\":10},{\\\"id\\\":11},{\\\"id\\\":12},{\\\"id\\\":13}]}]\"}', '2022-02-01 19:21:41', '2022-02-01 19:21:41');
INSERT INTO `admin_operation_log` VALUES (307, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 19:21:41', '2022-02-01 19:21:41');
INSERT INTO `admin_operation_log` VALUES (308, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u6536\\u8cbb\\u8aaa\\u660e\\u4e0a\\u67b6\\u7ba1\\u7406\",\"icon\":\"fa-credit-card\",\"uri\":null,\"roles\":[null],\"permission\":null,\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\"}', '2022-02-01 19:24:01', '2022-02-01 19:24:01');
INSERT INTO `admin_operation_log` VALUES (309, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 19:24:01', '2022-02-01 19:24:01');
INSERT INTO `admin_operation_log` VALUES (310, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u5716\\u7247\\u8f2a\\u64ad\\u7ba1\\u7406\",\"icon\":\"fa-file-photo-o\",\"uri\":null,\"roles\":[null],\"permission\":null,\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\"}', '2022-02-01 19:24:53', '2022-02-01 19:24:53');
INSERT INTO `admin_operation_log` VALUES (311, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 19:24:53', '2022-02-01 19:24:53');
INSERT INTO `admin_operation_log` VALUES (312, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"_token\":\"YZsNqZ3i3TRGIjgzWXHQAFRArfXRmzcwZEVgGVQx\",\"_order\":\"[{\\\"id\\\":14},{\\\"id\\\":17},{\\\"id\\\":18},{\\\"id\\\":19},{\\\"id\\\":20},{\\\"id\\\":16},{\\\"id\\\":15},{\\\"id\\\":3},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7}]},{\\\"id\\\":9,\\\"children\\\":[{\\\"id\\\":10},{\\\"id\\\":11},{\\\"id\\\":12},{\\\"id\\\":13}]}]\"}', '2022-02-01 19:25:36', '2022-02-01 19:25:36');
INSERT INTO `admin_operation_log` VALUES (313, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 19:25:36', '2022-02-01 19:25:36');
INSERT INTO `admin_operation_log` VALUES (314, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 19:25:50', '2022-02-01 19:25:50');
INSERT INTO `admin_operation_log` VALUES (315, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 21:26:39', '2022-02-01 21:26:39');
INSERT INTO `admin_operation_log` VALUES (316, 1, 'admin/helpers/terminal/database', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 21:26:44', '2022-02-01 21:26:44');
INSERT INTO `admin_operation_log` VALUES (317, 1, 'admin/helpers/terminal/artisan', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 21:26:45', '2022-02-01 21:26:45');
INSERT INTO `admin_operation_log` VALUES (318, 1, 'admin/helpers/routes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 21:26:46', '2022-02-01 21:26:46');
INSERT INTO `admin_operation_log` VALUES (319, 1, 'admin/helpers/terminal/artisan', 'GET', '127.0.0.1', '[]', '2022-02-01 21:26:46', '2022-02-01 21:26:46');
INSERT INTO `admin_operation_log` VALUES (320, 1, 'admin/helpers/routes', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 21:26:47', '2022-02-01 21:26:47');
INSERT INTO `admin_operation_log` VALUES (321, 1, 'admin/helpers/routes', 'GET', '127.0.0.1', '[]', '2022-02-01 21:26:47', '2022-02-01 21:26:47');
INSERT INTO `admin_operation_log` VALUES (322, 1, 'admin/helpers/routes', 'GET', '127.0.0.1', '[]', '2022-02-01 21:26:47', '2022-02-01 21:26:47');
INSERT INTO `admin_operation_log` VALUES (323, 1, 'admin/helpers/routes', 'GET', '127.0.0.1', '[]', '2022-02-01 21:26:48', '2022-02-01 21:26:48');
INSERT INTO `admin_operation_log` VALUES (324, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 21:26:48', '2022-02-01 21:26:48');
INSERT INTO `admin_operation_log` VALUES (325, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2022-02-01 21:26:52', '2022-02-01 21:26:52');
INSERT INTO `admin_operation_log` VALUES (326, 1, 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"platforms\",\"model_name\":\"App\\\\Models\\\\Platform\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\PlatformController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":\"\\u540d\\u7a31\"},{\"name\":\"contact\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":\"\\u806f\\u7d61\\u4eba\"},{\"name\":\"enable\",\"type\":\"integer\",\"key\":null,\"default\":\"1\",\"comment\":\"\\u662f\\u5426\\u555f\\u7528\"},{\"name\":\"code\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":\"\\u7cfb\\u7d71\\u7de8\\u78bc\"}],\"timestamps\":\"on\",\"soft_deletes\":\"on\",\"primary_key\":\"id\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\"}', '2022-02-01 21:30:38', '2022-02-01 21:30:38');
INSERT INTO `admin_operation_log` VALUES (327, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2022-02-01 21:30:38', '2022-02-01 21:30:38');
INSERT INTO `admin_operation_log` VALUES (328, 1, 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"news\",\"model_name\":\"App\\\\Models\\\\News\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"platform_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"title\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":\"\\u6a19\\u984c\"},{\"name\":\"content\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":\"\\u6587\\u5b57\\u5167\\u5bb9\"}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\"}', '2022-02-01 21:33:28', '2022-02-01 21:33:28');
INSERT INTO `admin_operation_log` VALUES (329, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2022-02-01 21:33:28', '2022-02-01 21:33:28');
INSERT INTO `admin_operation_log` VALUES (330, 1, 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"rules\",\"model_name\":\"App\\\\Models\\\\Rule\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\RuleController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"platform_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"enable\",\"type\":\"integer\",\"key\":null,\"default\":\"1\",\"comment\":null},{\"name\":\"content\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\"}', '2022-02-01 21:37:32', '2022-02-01 21:37:32');
INSERT INTO `admin_operation_log` VALUES (331, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2022-02-01 21:37:33', '2022-02-01 21:37:33');
INSERT INTO `admin_operation_log` VALUES (332, 1, 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"pays\",\"model_name\":\"App\\\\Models\\\\Pay\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\PayController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"platform_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"e\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\"}', '2022-02-01 21:40:25', '2022-02-01 21:40:25');
INSERT INTO `admin_operation_log` VALUES (333, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2022-02-01 21:40:26', '2022-02-01 21:40:26');
INSERT INTO `admin_operation_log` VALUES (334, 1, 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"pays\",\"model_name\":\"App\\\\Models\\\\Pay\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\PayController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"support_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"enable\",\"type\":\"integer\",\"key\":null,\"default\":\"1\",\"comment\":null},{\"name\":\"content\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\"}', '2022-02-01 21:41:47', '2022-02-01 21:41:47');
INSERT INTO `admin_operation_log` VALUES (335, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2022-02-01 21:41:47', '2022-02-01 21:41:47');
INSERT INTO `admin_operation_log` VALUES (336, 1, 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"pays\",\"model_name\":\"App\\\\Models\\\\Pay\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\PayController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"support_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"enable\",\"type\":\"integer\",\"key\":null,\"default\":\"1\",\"comment\":null},{\"name\":\"content\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\"}', '2022-02-01 21:42:07', '2022-02-01 21:42:07');
INSERT INTO `admin_operation_log` VALUES (337, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2022-02-01 21:42:07', '2022-02-01 21:42:07');
INSERT INTO `admin_operation_log` VALUES (338, 1, 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"Ads\",\"model_name\":\"App\\\\Models\\\\Ad\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\AdController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"platform_id\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"enable\",\"type\":\"integer\",\"key\":null,\"default\":\"1\",\"comment\":null},{\"name\":\"url\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"img\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\"}', '2022-02-01 21:44:35', '2022-02-01 21:44:35');
INSERT INTO `admin_operation_log` VALUES (339, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2022-02-01 21:44:36', '2022-02-01 21:44:36');
INSERT INTO `admin_operation_log` VALUES (340, 1, 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"orders\",\"model_name\":\"App\\\\Models\\\\Order\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\OrderController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"platform_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"phone\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"car_no\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"addr\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"status\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"soft_deletes\":\"on\",\"primary_key\":\"id\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\"}', '2022-02-01 21:46:06', '2022-02-01 21:46:06');
INSERT INTO `admin_operation_log` VALUES (341, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2022-02-01 21:46:06', '2022-02-01 21:46:06');
INSERT INTO `admin_operation_log` VALUES (342, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 21:48:16', '2022-02-01 21:48:16');
INSERT INTO `admin_operation_log` VALUES (343, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 21:48:20', '2022-02-01 21:48:20');
INSERT INTO `admin_operation_log` VALUES (344, 1, 'admin/auth/menu/14/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 21:48:23', '2022-02-01 21:48:23');
INSERT INTO `admin_operation_log` VALUES (345, 1, 'admin/auth/menu/14', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u5408\\u4f5c\\u901a\\u8def\\u7ba1\\u7406\",\"icon\":\"fa-automobile\",\"uri\":\"\\/platform\",\"roles\":[\"1\",\"2\",\"3\",null],\"permission\":\"*\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/auth\\/menu\"}', '2022-02-01 21:48:33', '2022-02-01 21:48:33');
INSERT INTO `admin_operation_log` VALUES (346, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 21:48:33', '2022-02-01 21:48:33');
INSERT INTO `admin_operation_log` VALUES (347, 1, 'admin/auth/menu/17/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 21:48:37', '2022-02-01 21:48:37');
INSERT INTO `admin_operation_log` VALUES (348, 1, 'admin/auth/menu/17', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u8dd1\\u99ac\\u71c8\\u8a0a\\u606f\\u7ba1\\u7406\",\"icon\":\"fa-bell-o\",\"uri\":\"\\/news\",\"roles\":[\"1\",\"2\",\"3\",null],\"permission\":\"*\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/auth\\/menu\"}', '2022-02-01 21:48:44', '2022-02-01 21:48:44');
INSERT INTO `admin_operation_log` VALUES (349, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 21:48:44', '2022-02-01 21:48:44');
INSERT INTO `admin_operation_log` VALUES (350, 1, 'admin/auth/menu/18/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 21:48:48', '2022-02-01 21:48:48');
INSERT INTO `admin_operation_log` VALUES (351, 1, 'admin/auth/menu/18', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u6551\\u63f4\\u670d\\u52d9\\u689d\\u6b3e\\u4e0a\\u67b6\\u7ba1\\u7406\",\"icon\":\"fa-bolt\",\"uri\":\"\\/rule\",\"roles\":[\"1\",\"2\",\"3\",null],\"permission\":\"*\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/auth\\/menu\"}', '2022-02-01 21:49:09', '2022-02-01 21:49:09');
INSERT INTO `admin_operation_log` VALUES (352, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 21:49:09', '2022-02-01 21:49:09');
INSERT INTO `admin_operation_log` VALUES (353, 1, 'admin/auth/menu/19/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 21:49:15', '2022-02-01 21:49:15');
INSERT INTO `admin_operation_log` VALUES (354, 1, 'admin/auth/menu/19', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u6536\\u8cbb\\u8aaa\\u660e\\u4e0a\\u67b6\\u7ba1\\u7406\",\"icon\":\"fa-credit-card\",\"uri\":\"\\/pay\",\"roles\":[\"1\",\"2\",\"3\",null],\"permission\":\"*\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/auth\\/menu\"}', '2022-02-01 21:49:30', '2022-02-01 21:49:30');
INSERT INTO `admin_operation_log` VALUES (355, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 21:49:30', '2022-02-01 21:49:30');
INSERT INTO `admin_operation_log` VALUES (356, 1, 'admin/auth/menu/20/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 21:49:34', '2022-02-01 21:49:34');
INSERT INTO `admin_operation_log` VALUES (357, 1, 'admin/auth/menu/20', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u5716\\u7247\\u8f2a\\u64ad\\u7ba1\\u7406\",\"icon\":\"fa-file-photo-o\",\"uri\":\"\\/ad\",\"roles\":[\"1\",\"2\",\"3\",null],\"permission\":\"*\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/auth\\/menu\"}', '2022-02-01 21:49:56', '2022-02-01 21:49:56');
INSERT INTO `admin_operation_log` VALUES (358, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 21:49:56', '2022-02-01 21:49:56');
INSERT INTO `admin_operation_log` VALUES (359, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-01 21:50:10', '2022-02-01 21:50:10');
INSERT INTO `admin_operation_log` VALUES (360, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 21:50:12', '2022-02-01 21:50:12');
INSERT INTO `admin_operation_log` VALUES (361, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 21:50:13', '2022-02-01 21:50:13');
INSERT INTO `admin_operation_log` VALUES (362, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 21:50:14', '2022-02-01 21:50:14');
INSERT INTO `admin_operation_log` VALUES (363, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 21:50:15', '2022-02-01 21:50:15');
INSERT INTO `admin_operation_log` VALUES (364, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 21:50:15', '2022-02-01 21:50:15');
INSERT INTO `admin_operation_log` VALUES (365, 1, 'admin/disconut', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 21:50:16', '2022-02-01 21:50:16');
INSERT INTO `admin_operation_log` VALUES (366, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 21:50:17', '2022-02-01 21:50:17');
INSERT INTO `admin_operation_log` VALUES (367, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 21:50:18', '2022-02-01 21:50:18');
INSERT INTO `admin_operation_log` VALUES (368, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 21:50:35', '2022-02-01 21:50:35');
INSERT INTO `admin_operation_log` VALUES (369, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 21:51:30', '2022-02-01 21:51:30');
INSERT INTO `admin_operation_log` VALUES (370, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 21:51:46', '2022-02-01 21:51:46');
INSERT INTO `admin_operation_log` VALUES (371, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 21:51:47', '2022-02-01 21:51:47');
INSERT INTO `admin_operation_log` VALUES (372, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 21:51:59', '2022-02-01 21:51:59');
INSERT INTO `admin_operation_log` VALUES (373, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 21:52:00', '2022-02-01 21:52:00');
INSERT INTO `admin_operation_log` VALUES (374, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 21:52:01', '2022-02-01 21:52:01');
INSERT INTO `admin_operation_log` VALUES (375, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 21:52:02', '2022-02-01 21:52:02');
INSERT INTO `admin_operation_log` VALUES (376, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 21:52:08', '2022-02-01 21:52:08');
INSERT INTO `admin_operation_log` VALUES (377, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 21:52:15', '2022-02-01 21:52:15');
INSERT INTO `admin_operation_log` VALUES (378, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 21:52:30', '2022-02-01 21:52:30');
INSERT INTO `admin_operation_log` VALUES (379, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 21:52:51', '2022-02-01 21:52:51');
INSERT INTO `admin_operation_log` VALUES (380, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 21:52:52', '2022-02-01 21:52:52');
INSERT INTO `admin_operation_log` VALUES (381, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 21:52:53', '2022-02-01 21:52:53');
INSERT INTO `admin_operation_log` VALUES (382, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 21:52:53', '2022-02-01 21:52:53');
INSERT INTO `admin_operation_log` VALUES (383, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 21:53:29', '2022-02-01 21:53:29');
INSERT INTO `admin_operation_log` VALUES (384, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 21:53:45', '2022-02-01 21:53:45');
INSERT INTO `admin_operation_log` VALUES (385, 1, 'admin/platform/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 21:53:50', '2022-02-01 21:53:50');
INSERT INTO `admin_operation_log` VALUES (386, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 21:54:28', '2022-02-01 21:54:28');
INSERT INTO `admin_operation_log` VALUES (387, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 21:55:41', '2022-02-01 21:55:41');
INSERT INTO `admin_operation_log` VALUES (388, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 21:55:42', '2022-02-01 21:55:42');
INSERT INTO `admin_operation_log` VALUES (389, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 21:55:44', '2022-02-01 21:55:44');
INSERT INTO `admin_operation_log` VALUES (390, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 21:55:44', '2022-02-01 21:55:44');
INSERT INTO `admin_operation_log` VALUES (391, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 21:55:45', '2022-02-01 21:55:45');
INSERT INTO `admin_operation_log` VALUES (392, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 21:56:30', '2022-02-01 21:56:30');
INSERT INTO `admin_operation_log` VALUES (393, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 21:56:31', '2022-02-01 21:56:31');
INSERT INTO `admin_operation_log` VALUES (394, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 21:57:41', '2022-02-01 21:57:41');
INSERT INTO `admin_operation_log` VALUES (395, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 21:57:42', '2022-02-01 21:57:42');
INSERT INTO `admin_operation_log` VALUES (396, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 21:58:56', '2022-02-01 21:58:56');
INSERT INTO `admin_operation_log` VALUES (397, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 21:59:03', '2022-02-01 21:59:03');
INSERT INTO `admin_operation_log` VALUES (398, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:03:45', '2022-02-01 22:03:45');
INSERT INTO `admin_operation_log` VALUES (399, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:03:53', '2022-02-01 22:03:53');
INSERT INTO `admin_operation_log` VALUES (400, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:03:54', '2022-02-01 22:03:54');
INSERT INTO `admin_operation_log` VALUES (401, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:04:00', '2022-02-01 22:04:00');
INSERT INTO `admin_operation_log` VALUES (402, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:04:01', '2022-02-01 22:04:01');
INSERT INTO `admin_operation_log` VALUES (403, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:04:03', '2022-02-01 22:04:03');
INSERT INTO `admin_operation_log` VALUES (404, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:04:16', '2022-02-01 22:04:16');
INSERT INTO `admin_operation_log` VALUES (405, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:04:24', '2022-02-01 22:04:24');
INSERT INTO `admin_operation_log` VALUES (406, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:04:25', '2022-02-01 22:04:25');
INSERT INTO `admin_operation_log` VALUES (407, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:04:25', '2022-02-01 22:04:25');
INSERT INTO `admin_operation_log` VALUES (408, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:04:26', '2022-02-01 22:04:26');
INSERT INTO `admin_operation_log` VALUES (409, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:04:27', '2022-02-01 22:04:27');
INSERT INTO `admin_operation_log` VALUES (410, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:04:32', '2022-02-01 22:04:32');
INSERT INTO `admin_operation_log` VALUES (411, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:04:33', '2022-02-01 22:04:33');
INSERT INTO `admin_operation_log` VALUES (412, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:04:33', '2022-02-01 22:04:33');
INSERT INTO `admin_operation_log` VALUES (413, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:04:39', '2022-02-01 22:04:39');
INSERT INTO `admin_operation_log` VALUES (414, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:04:40', '2022-02-01 22:04:40');
INSERT INTO `admin_operation_log` VALUES (415, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:04:42', '2022-02-01 22:04:42');
INSERT INTO `admin_operation_log` VALUES (416, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:04:42', '2022-02-01 22:04:42');
INSERT INTO `admin_operation_log` VALUES (417, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:04:43', '2022-02-01 22:04:43');
INSERT INTO `admin_operation_log` VALUES (418, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:04:44', '2022-02-01 22:04:44');
INSERT INTO `admin_operation_log` VALUES (419, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:05:00', '2022-02-01 22:05:00');
INSERT INTO `admin_operation_log` VALUES (420, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:05:00', '2022-02-01 22:05:00');
INSERT INTO `admin_operation_log` VALUES (421, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:12:54', '2022-02-01 22:12:54');
INSERT INTO `admin_operation_log` VALUES (422, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:12:55', '2022-02-01 22:12:55');
INSERT INTO `admin_operation_log` VALUES (423, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:12:56', '2022-02-01 22:12:56');
INSERT INTO `admin_operation_log` VALUES (424, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:12:57', '2022-02-01 22:12:57');
INSERT INTO `admin_operation_log` VALUES (425, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:12:58', '2022-02-01 22:12:58');
INSERT INTO `admin_operation_log` VALUES (426, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:12:59', '2022-02-01 22:12:59');
INSERT INTO `admin_operation_log` VALUES (427, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:13:00', '2022-02-01 22:13:00');
INSERT INTO `admin_operation_log` VALUES (428, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:13:09', '2022-02-01 22:13:09');
INSERT INTO `admin_operation_log` VALUES (429, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:13:19', '2022-02-01 22:13:19');
INSERT INTO `admin_operation_log` VALUES (430, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:13:20', '2022-02-01 22:13:20');
INSERT INTO `admin_operation_log` VALUES (431, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:13:20', '2022-02-01 22:13:20');
INSERT INTO `admin_operation_log` VALUES (432, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:13:21', '2022-02-01 22:13:21');
INSERT INTO `admin_operation_log` VALUES (433, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:13:22', '2022-02-01 22:13:22');
INSERT INTO `admin_operation_log` VALUES (434, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:13:22', '2022-02-01 22:13:22');
INSERT INTO `admin_operation_log` VALUES (435, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:13:24', '2022-02-01 22:13:24');
INSERT INTO `admin_operation_log` VALUES (436, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:13:25', '2022-02-01 22:13:25');
INSERT INTO `admin_operation_log` VALUES (437, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:13:31', '2022-02-01 22:13:31');
INSERT INTO `admin_operation_log` VALUES (438, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:13:32', '2022-02-01 22:13:32');
INSERT INTO `admin_operation_log` VALUES (439, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:13:33', '2022-02-01 22:13:33');
INSERT INTO `admin_operation_log` VALUES (440, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:13:34', '2022-02-01 22:13:34');
INSERT INTO `admin_operation_log` VALUES (441, 1, 'admin/platform', 'POST', '127.0.0.1', '{\"name\":\"test\",\"contact\":\"test\",\"enable\":\"1\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\"}', '2022-02-01 22:14:33', '2022-02-01 22:14:33');
INSERT INTO `admin_operation_log` VALUES (442, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:14:33', '2022-02-01 22:14:33');
INSERT INTO `admin_operation_log` VALUES (443, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:15:56', '2022-02-01 22:15:56');
INSERT INTO `admin_operation_log` VALUES (444, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:15:58', '2022-02-01 22:15:58');
INSERT INTO `admin_operation_log` VALUES (445, 1, 'admin/platform', 'POST', '127.0.0.1', '{\"code\":\"TR694\",\"name\":\"\\u7ba1\\u7406\\u8005\",\"contact\":\"test\",\"enable\":\"1\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\"}', '2022-02-01 22:16:03', '2022-02-01 22:16:03');
INSERT INTO `admin_operation_log` VALUES (446, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:16:03', '2022-02-01 22:16:03');
INSERT INTO `admin_operation_log` VALUES (447, 1, 'admin/platform/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:16:08', '2022-02-01 22:16:08');
INSERT INTO `admin_operation_log` VALUES (448, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:16:11', '2022-02-01 22:16:11');
INSERT INTO `admin_operation_log` VALUES (449, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:16:12', '2022-02-01 22:16:12');
INSERT INTO `admin_operation_log` VALUES (450, 1, 'admin/platform/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:16:14', '2022-02-01 22:16:14');
INSERT INTO `admin_operation_log` VALUES (451, 1, 'admin/platform/1', 'PUT', '127.0.0.1', '{\"code\":\"TR694\",\"name\":\"\\u7ba1\\u7406\\u8005\",\"contact\":\"test\",\"enable\":\"1\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/platform\"}', '2022-02-01 22:16:18', '2022-02-01 22:16:18');
INSERT INTO `admin_operation_log` VALUES (452, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:16:18', '2022-02-01 22:16:18');
INSERT INTO `admin_operation_log` VALUES (453, 1, 'admin/platform/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:16:20', '2022-02-01 22:16:20');
INSERT INTO `admin_operation_log` VALUES (454, 1, 'admin/platform/1/edit', 'GET', '127.0.0.1', '[]', '2022-02-01 22:16:23', '2022-02-01 22:16:23');
INSERT INTO `admin_operation_log` VALUES (455, 1, 'admin/platform/1/edit', 'GET', '127.0.0.1', '[]', '2022-02-01 22:16:24', '2022-02-01 22:16:24');
INSERT INTO `admin_operation_log` VALUES (456, 1, 'admin/platform/1/edit', 'GET', '127.0.0.1', '[]', '2022-02-01 22:16:24', '2022-02-01 22:16:24');
INSERT INTO `admin_operation_log` VALUES (457, 1, 'admin/platform/1/edit', 'GET', '127.0.0.1', '[]', '2022-02-01 22:16:25', '2022-02-01 22:16:25');
INSERT INTO `admin_operation_log` VALUES (458, 1, 'admin/platform/1/edit', 'GET', '127.0.0.1', '[]', '2022-02-01 22:16:25', '2022-02-01 22:16:25');
INSERT INTO `admin_operation_log` VALUES (459, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:16:36', '2022-02-01 22:16:36');
INSERT INTO `admin_operation_log` VALUES (460, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:17:31', '2022-02-01 22:17:31');
INSERT INTO `admin_operation_log` VALUES (461, 1, 'admin/platform/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:17:35', '2022-02-01 22:17:35');
INSERT INTO `admin_operation_log` VALUES (462, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:17:36', '2022-02-01 22:17:36');
INSERT INTO `admin_operation_log` VALUES (463, 1, 'admin/platform', 'POST', '127.0.0.1', '{\"code\":\"FV263\",\"name\":\"test2\",\"contact\":null,\"enable\":\"1\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/platform\"}', '2022-02-01 22:17:45', '2022-02-01 22:17:45');
INSERT INTO `admin_operation_log` VALUES (464, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:17:45', '2022-02-01 22:17:45');
INSERT INTO `admin_operation_log` VALUES (465, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:18:05', '2022-02-01 22:18:05');
INSERT INTO `admin_operation_log` VALUES (466, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:18:06', '2022-02-01 22:18:06');
INSERT INTO `admin_operation_log` VALUES (467, 1, 'admin/platform', 'POST', '127.0.0.1', '{\"code\":\"RT166\",\"name\":\"\\u7ba1\\u7406\\u8005\",\"contact\":\"Sam\",\"enable\":\"1\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\"}', '2022-02-01 22:18:12', '2022-02-01 22:18:12');
INSERT INTO `admin_operation_log` VALUES (468, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:18:12', '2022-02-01 22:18:12');
INSERT INTO `admin_operation_log` VALUES (469, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:20:25', '2022-02-01 22:20:25');
INSERT INTO `admin_operation_log` VALUES (470, 1, 'admin/platform/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:20:27', '2022-02-01 22:20:27');
INSERT INTO `admin_operation_log` VALUES (471, 1, 'admin/platform', 'POST', '127.0.0.1', '{\"code\":\"GB980\",\"name\":\"\\u7ba1\\u7406\\u8005\",\"contact\":\"test\",\"phone\":\"0973011____\",\"enable\":\"1\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/platform\"}', '2022-02-01 22:20:43', '2022-02-01 22:20:43');
INSERT INTO `admin_operation_log` VALUES (472, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:20:43', '2022-02-01 22:20:43');
INSERT INTO `admin_operation_log` VALUES (473, 1, 'admin/_handle_action_', 'POST', '127.0.0.1', '{\"_key\":\"3\",\"_model\":\"App_Models_Platform\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\",\"_action\":\"Encore_Admin_Grid_Actions_Delete\",\"_input\":\"true\"}', '2022-02-01 22:20:53', '2022-02-01 22:20:53');
INSERT INTO `admin_operation_log` VALUES (474, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:20:53', '2022-02-01 22:20:53');
INSERT INTO `admin_operation_log` VALUES (475, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:21:26', '2022-02-01 22:21:26');
INSERT INTO `admin_operation_log` VALUES (476, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:21:27', '2022-02-01 22:21:27');
INSERT INTO `admin_operation_log` VALUES (477, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:21:28', '2022-02-01 22:21:28');
INSERT INTO `admin_operation_log` VALUES (478, 1, 'admin/platform/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:21:39', '2022-02-01 22:21:39');
INSERT INTO `admin_operation_log` VALUES (479, 1, 'admin/platform/2/edit', 'GET', '127.0.0.1', '[]', '2022-02-01 22:22:29', '2022-02-01 22:22:29');
INSERT INTO `admin_operation_log` VALUES (480, 1, 'admin/platform/2/edit', 'GET', '127.0.0.1', '[]', '2022-02-01 22:22:30', '2022-02-01 22:22:30');
INSERT INTO `admin_operation_log` VALUES (481, 1, 'admin/platform/2/edit', 'GET', '127.0.0.1', '[]', '2022-02-01 22:22:47', '2022-02-01 22:22:47');
INSERT INTO `admin_operation_log` VALUES (482, 1, 'admin/platform/2', 'PUT', '127.0.0.1', '{\"code\":\"RT166\",\"name\":\"\\u7ba1\\u7406\\u8005\",\"contact\":\"Sam\",\"phone\":\"1115-555-555\",\"enable\":\"1\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\",\"_method\":\"PUT\"}', '2022-02-01 22:23:04', '2022-02-01 22:23:04');
INSERT INTO `admin_operation_log` VALUES (483, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:23:04', '2022-02-01 22:23:04');
INSERT INTO `admin_operation_log` VALUES (484, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:23:19', '2022-02-01 22:23:19');
INSERT INTO `admin_operation_log` VALUES (485, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:23:20', '2022-02-01 22:23:20');
INSERT INTO `admin_operation_log` VALUES (486, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:23:30', '2022-02-01 22:23:30');
INSERT INTO `admin_operation_log` VALUES (487, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:23:31', '2022-02-01 22:23:31');
INSERT INTO `admin_operation_log` VALUES (488, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:23:32', '2022-02-01 22:23:32');
INSERT INTO `admin_operation_log` VALUES (489, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:23:32', '2022-02-01 22:23:32');
INSERT INTO `admin_operation_log` VALUES (490, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:23:43', '2022-02-01 22:23:43');
INSERT INTO `admin_operation_log` VALUES (491, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:23:44', '2022-02-01 22:23:44');
INSERT INTO `admin_operation_log` VALUES (492, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:24:32', '2022-02-01 22:24:32');
INSERT INTO `admin_operation_log` VALUES (493, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:25:01', '2022-02-01 22:25:01');
INSERT INTO `admin_operation_log` VALUES (494, 1, 'admin/platform/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:25:14', '2022-02-01 22:25:14');
INSERT INTO `admin_operation_log` VALUES (495, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:25:15', '2022-02-01 22:25:15');
INSERT INTO `admin_operation_log` VALUES (496, 1, 'admin/platform/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:25:17', '2022-02-01 22:25:17');
INSERT INTO `admin_operation_log` VALUES (497, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:27:38', '2022-02-01 22:27:38');
INSERT INTO `admin_operation_log` VALUES (498, 1, 'admin/platform', 'POST', '127.0.0.1', '{\"code\":\"TC294\",\"name\":\"test\",\"contact\":\"11\",\"phone\":null,\"enable\":\"1\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/platform\"}', '2022-02-01 22:27:46', '2022-02-01 22:27:46');
INSERT INTO `admin_operation_log` VALUES (499, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:27:47', '2022-02-01 22:27:47');
INSERT INTO `admin_operation_log` VALUES (500, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:29:56', '2022-02-01 22:29:56');
INSERT INTO `admin_operation_log` VALUES (501, 1, 'admin/platform', 'POST', '127.0.0.1', '{\"code\":\"VA192\",\"name\":\"\\u7ba1\\u7406\\u8005\",\"contact\":\"test\",\"phone\":null,\"enable\":\"1\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\"}', '2022-02-01 22:30:01', '2022-02-01 22:30:01');
INSERT INTO `admin_operation_log` VALUES (502, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:30:01', '2022-02-01 22:30:01');
INSERT INTO `admin_operation_log` VALUES (503, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:31:12', '2022-02-01 22:31:12');
INSERT INTO `admin_operation_log` VALUES (504, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:31:44', '2022-02-01 22:31:44');
INSERT INTO `admin_operation_log` VALUES (505, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:31:45', '2022-02-01 22:31:45');
INSERT INTO `admin_operation_log` VALUES (506, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:31:46', '2022-02-01 22:31:46');
INSERT INTO `admin_operation_log` VALUES (507, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:31:46', '2022-02-01 22:31:46');
INSERT INTO `admin_operation_log` VALUES (508, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:31:47', '2022-02-01 22:31:47');
INSERT INTO `admin_operation_log` VALUES (509, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:31:48', '2022-02-01 22:31:48');
INSERT INTO `admin_operation_log` VALUES (510, 1, 'admin/platform', 'POST', '127.0.0.1', '{\"code\":\"FFB264\",\"name\":\"\\u7ba1\\u7406\\u8005\",\"contact\":\"test\",\"phone\":\"0973-011-400\",\"enable\":\"1\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\"}', '2022-02-01 22:32:00', '2022-02-01 22:32:00');
INSERT INTO `admin_operation_log` VALUES (511, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:32:00', '2022-02-01 22:32:00');
INSERT INTO `admin_operation_log` VALUES (512, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:32:20', '2022-02-01 22:32:20');
INSERT INTO `admin_operation_log` VALUES (513, 1, 'admin/platform/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:33:50', '2022-02-01 22:33:50');
INSERT INTO `admin_operation_log` VALUES (514, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:34:37', '2022-02-01 22:34:37');
INSERT INTO `admin_operation_log` VALUES (515, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:34:39', '2022-02-01 22:34:39');
INSERT INTO `admin_operation_log` VALUES (516, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:34:40', '2022-02-01 22:34:40');
INSERT INTO `admin_operation_log` VALUES (517, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 22:34:50', '2022-02-01 22:34:50');
INSERT INTO `admin_operation_log` VALUES (518, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:34:55', '2022-02-01 22:34:55');
INSERT INTO `admin_operation_log` VALUES (519, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:37:59', '2022-02-01 22:37:59');
INSERT INTO `admin_operation_log` VALUES (520, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:38:22', '2022-02-01 22:38:22');
INSERT INTO `admin_operation_log` VALUES (521, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:38:23', '2022-02-01 22:38:23');
INSERT INTO `admin_operation_log` VALUES (522, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:38:25', '2022-02-01 22:38:25');
INSERT INTO `admin_operation_log` VALUES (523, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:38:25', '2022-02-01 22:38:25');
INSERT INTO `admin_operation_log` VALUES (524, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:38:26', '2022-02-01 22:38:26');
INSERT INTO `admin_operation_log` VALUES (525, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:38:27', '2022-02-01 22:38:27');
INSERT INTO `admin_operation_log` VALUES (526, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:38:27', '2022-02-01 22:38:27');
INSERT INTO `admin_operation_log` VALUES (527, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:40:24', '2022-02-01 22:40:24');
INSERT INTO `admin_operation_log` VALUES (528, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:40:24', '2022-02-01 22:40:24');
INSERT INTO `admin_operation_log` VALUES (529, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:43:59', '2022-02-01 22:43:59');
INSERT INTO `admin_operation_log` VALUES (530, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"code\":null,\"name\":null,\"area_tyep\":\"1\",\"startdate\":{\"start\":null,\"end\":null},\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:44:06', '2022-02-01 22:44:06');
INSERT INTO `admin_operation_log` VALUES (531, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"code\":null,\"name\":null,\"area_tyep\":\"1\",\"startdate\":{\"start\":null,\"end\":null}}', '2022-02-01 22:44:52', '2022-02-01 22:44:52');
INSERT INTO `admin_operation_log` VALUES (532, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"area_tyep\":\"1\",\"code\":null,\"name\":null,\"contact\":null,\"enable\":\"1\",\"created_at\":{\"start\":null,\"end\":null},\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:44:56', '2022-02-01 22:44:56');
INSERT INTO `admin_operation_log` VALUES (533, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"area_tyep\":\"1\",\"_pjax\":\"#pjax-container\",\"code\":null,\"name\":null,\"contact\":null,\"enable\":\"0\",\"created_at\":{\"start\":null,\"end\":null}}', '2022-02-01 22:44:59', '2022-02-01 22:44:59');
INSERT INTO `admin_operation_log` VALUES (534, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"area_tyep\":\"1\",\"_pjax\":\"#pjax-container\",\"code\":null,\"name\":null,\"contact\":null,\"enable\":\"1\",\"created_at\":{\"start\":null,\"end\":null}}', '2022-02-01 22:45:00', '2022-02-01 22:45:00');
INSERT INTO `admin_operation_log` VALUES (535, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"area_tyep\":\"1\",\"_pjax\":\"#pjax-container\",\"code\":\"TR\",\"name\":null,\"contact\":null,\"enable\":\"1\",\"created_at\":{\"start\":null,\"end\":null}}', '2022-02-01 22:45:05', '2022-02-01 22:45:05');
INSERT INTO `admin_operation_log` VALUES (536, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"area_tyep\":\"1\",\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:45:07', '2022-02-01 22:45:07');
INSERT INTO `admin_operation_log` VALUES (537, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"area_tyep\":\"1\",\"_pjax\":\"#pjax-container\",\"_export_\":\"all\"}', '2022-02-01 22:45:13', '2022-02-01 22:45:13');
INSERT INTO `admin_operation_log` VALUES (538, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"area_tyep\":\"1\",\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:45:38', '2022-02-01 22:45:38');
INSERT INTO `admin_operation_log` VALUES (539, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"area_tyep\":\"1\"}', '2022-02-01 22:46:35', '2022-02-01 22:46:35');
INSERT INTO `admin_operation_log` VALUES (540, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:46:41', '2022-02-01 22:46:41');
INSERT INTO `admin_operation_log` VALUES (541, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:46:47', '2022-02-01 22:46:47');
INSERT INTO `admin_operation_log` VALUES (542, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:46:55', '2022-02-01 22:46:55');
INSERT INTO `admin_operation_log` VALUES (543, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:46:55', '2022-02-01 22:46:55');
INSERT INTO `admin_operation_log` VALUES (544, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:47:16', '2022-02-01 22:47:16');
INSERT INTO `admin_operation_log` VALUES (545, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:47:24', '2022-02-01 22:47:24');
INSERT INTO `admin_operation_log` VALUES (546, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:47:27', '2022-02-01 22:47:27');
INSERT INTO `admin_operation_log` VALUES (547, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 22:52:51', '2022-02-01 22:52:51');
INSERT INTO `admin_operation_log` VALUES (548, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 22:52:52', '2022-02-01 22:52:52');
INSERT INTO `admin_operation_log` VALUES (549, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 22:53:31', '2022-02-01 22:53:31');
INSERT INTO `admin_operation_log` VALUES (550, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 22:53:32', '2022-02-01 22:53:32');
INSERT INTO `admin_operation_log` VALUES (551, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 22:53:33', '2022-02-01 22:53:33');
INSERT INTO `admin_operation_log` VALUES (552, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 22:53:33', '2022-02-01 22:53:33');
INSERT INTO `admin_operation_log` VALUES (553, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 22:53:33', '2022-02-01 22:53:33');
INSERT INTO `admin_operation_log` VALUES (554, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 22:53:57', '2022-02-01 22:53:57');
INSERT INTO `admin_operation_log` VALUES (555, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 22:53:58', '2022-02-01 22:53:58');
INSERT INTO `admin_operation_log` VALUES (556, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 22:53:58', '2022-02-01 22:53:58');
INSERT INTO `admin_operation_log` VALUES (557, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 22:53:59', '2022-02-01 22:53:59');
INSERT INTO `admin_operation_log` VALUES (558, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 22:53:59', '2022-02-01 22:53:59');
INSERT INTO `admin_operation_log` VALUES (559, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 22:54:00', '2022-02-01 22:54:00');
INSERT INTO `admin_operation_log` VALUES (560, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 22:54:00', '2022-02-01 22:54:00');
INSERT INTO `admin_operation_log` VALUES (561, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 22:54:14', '2022-02-01 22:54:14');
INSERT INTO `admin_operation_log` VALUES (562, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 22:54:15', '2022-02-01 22:54:15');
INSERT INTO `admin_operation_log` VALUES (563, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 22:55:20', '2022-02-01 22:55:20');
INSERT INTO `admin_operation_log` VALUES (564, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 22:55:21', '2022-02-01 22:55:21');
INSERT INTO `admin_operation_log` VALUES (565, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:55:22', '2022-02-01 22:55:22');
INSERT INTO `admin_operation_log` VALUES (566, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:55:22', '2022-02-01 22:55:22');
INSERT INTO `admin_operation_log` VALUES (567, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:55:23', '2022-02-01 22:55:23');
INSERT INTO `admin_operation_log` VALUES (568, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:55:24', '2022-02-01 22:55:24');
INSERT INTO `admin_operation_log` VALUES (569, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 22:57:11', '2022-02-01 22:57:11');
INSERT INTO `admin_operation_log` VALUES (570, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 22:57:12', '2022-02-01 22:57:12');
INSERT INTO `admin_operation_log` VALUES (571, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 22:57:13', '2022-02-01 22:57:13');
INSERT INTO `admin_operation_log` VALUES (572, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 22:57:45', '2022-02-01 22:57:45');
INSERT INTO `admin_operation_log` VALUES (573, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 22:57:47', '2022-02-01 22:57:47');
INSERT INTO `admin_operation_log` VALUES (574, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 22:57:48', '2022-02-01 22:57:48');
INSERT INTO `admin_operation_log` VALUES (575, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 22:57:49', '2022-02-01 22:57:49');
INSERT INTO `admin_operation_log` VALUES (576, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 22:58:07', '2022-02-01 22:58:07');
INSERT INTO `admin_operation_log` VALUES (577, 1, 'admin/news/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:58:14', '2022-02-01 22:58:14');
INSERT INTO `admin_operation_log` VALUES (578, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:59:05', '2022-02-01 22:59:05');
INSERT INTO `admin_operation_log` VALUES (579, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 22:59:47', '2022-02-01 22:59:47');
INSERT INTO `admin_operation_log` VALUES (580, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 22:59:48', '2022-02-01 22:59:48');
INSERT INTO `admin_operation_log` VALUES (581, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"platform\":{\"name\":\"test\"},\"title\":null,\"content\":null,\"created_at\":{\"start\":null,\"end\":null}}', '2022-02-01 22:59:56', '2022-02-01 22:59:56');
INSERT INTO `admin_operation_log` VALUES (582, 1, 'admin/news', 'GET', '127.0.0.1', '{\"platform\":{\"name\":\"test\"},\"title\":null,\"content\":null,\"created_at\":{\"start\":null,\"end\":null}}', '2022-02-01 23:00:55', '2022-02-01 23:00:55');
INSERT INTO `admin_operation_log` VALUES (583, 1, 'admin/news', 'GET', '127.0.0.1', '{\"platform\":{\"name\":\"test\"},\"title\":null,\"content\":null,\"created_at\":{\"start\":null,\"end\":null}}', '2022-02-01 23:00:56', '2022-02-01 23:00:56');
INSERT INTO `admin_operation_log` VALUES (584, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:00:58', '2022-02-01 23:00:58');
INSERT INTO `admin_operation_log` VALUES (585, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 23:00:58', '2022-02-01 23:00:58');
INSERT INTO `admin_operation_log` VALUES (586, 1, 'admin/news', 'GET', '127.0.0.1', '{\"platform\":{\"name\":\"test\"},\"title\":null,\"content\":null,\"created_at\":{\"start\":null,\"end\":null},\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:01:03', '2022-02-01 23:01:03');
INSERT INTO `admin_operation_log` VALUES (587, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:01:48', '2022-02-01 23:01:48');
INSERT INTO `admin_operation_log` VALUES (588, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 23:01:51', '2022-02-01 23:01:51');
INSERT INTO `admin_operation_log` VALUES (589, 1, 'admin/news', 'GET', '127.0.0.1', '{\"platform\":{\"name\":\"test\"},\"title\":null,\"content\":null,\"created_at\":{\"start\":null,\"end\":null},\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:01:56', '2022-02-01 23:01:56');
INSERT INTO `admin_operation_log` VALUES (590, 1, 'admin/news', 'GET', '127.0.0.1', '{\"platform\":{\"name\":\"test\"},\"title\":null,\"content\":null,\"created_at\":{\"start\":null,\"end\":null}}', '2022-02-01 23:02:27', '2022-02-01 23:02:27');
INSERT INTO `admin_operation_log` VALUES (591, 1, 'admin/news', 'GET', '127.0.0.1', '{\"platform\":{\"name\":\"test\"},\"title\":null,\"content\":null,\"created_at\":{\"start\":null,\"end\":null},\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:02:29', '2022-02-01 23:02:29');
INSERT INTO `admin_operation_log` VALUES (592, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:02:31', '2022-02-01 23:02:31');
INSERT INTO `admin_operation_log` VALUES (593, 1, 'admin/news/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:03:03', '2022-02-01 23:03:03');
INSERT INTO `admin_operation_log` VALUES (594, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:03:06', '2022-02-01 23:03:06');
INSERT INTO `admin_operation_log` VALUES (595, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 23:04:18', '2022-02-01 23:04:18');
INSERT INTO `admin_operation_log` VALUES (596, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:04:18', '2022-02-01 23:04:18');
INSERT INTO `admin_operation_log` VALUES (597, 1, 'admin/news/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:04:20', '2022-02-01 23:04:20');
INSERT INTO `admin_operation_log` VALUES (598, 1, 'admin/news/create', 'GET', '127.0.0.1', '[]', '2022-02-01 23:07:13', '2022-02-01 23:07:13');
INSERT INTO `admin_operation_log` VALUES (599, 1, 'admin/news/create', 'GET', '127.0.0.1', '[]', '2022-02-01 23:07:14', '2022-02-01 23:07:14');
INSERT INTO `admin_operation_log` VALUES (600, 1, 'admin/news/create', 'GET', '127.0.0.1', '[]', '2022-02-01 23:07:14', '2022-02-01 23:07:14');
INSERT INTO `admin_operation_log` VALUES (601, 1, 'admin/news/create', 'GET', '127.0.0.1', '[]', '2022-02-01 23:07:26', '2022-02-01 23:07:26');
INSERT INTO `admin_operation_log` VALUES (602, 1, 'admin/news/create', 'GET', '127.0.0.1', '[]', '2022-02-01 23:08:31', '2022-02-01 23:08:31');
INSERT INTO `admin_operation_log` VALUES (603, 1, 'admin/news/create', 'GET', '127.0.0.1', '[]', '2022-02-01 23:08:53', '2022-02-01 23:08:53');
INSERT INTO `admin_operation_log` VALUES (604, 1, 'admin/news/create', 'GET', '127.0.0.1', '[]', '2022-02-01 23:09:14', '2022-02-01 23:09:14');
INSERT INTO `admin_operation_log` VALUES (605, 1, 'admin/news/create', 'GET', '127.0.0.1', '[]', '2022-02-01 23:10:36', '2022-02-01 23:10:36');
INSERT INTO `admin_operation_log` VALUES (606, 1, 'admin/news/create', 'GET', '127.0.0.1', '[]', '2022-02-01 23:10:37', '2022-02-01 23:10:37');
INSERT INTO `admin_operation_log` VALUES (607, 1, 'admin/news/create', 'GET', '127.0.0.1', '[]', '2022-02-01 23:11:01', '2022-02-01 23:11:01');
INSERT INTO `admin_operation_log` VALUES (608, 1, 'admin/news/create', 'GET', '127.0.0.1', '[]', '2022-02-01 23:12:01', '2022-02-01 23:12:01');
INSERT INTO `admin_operation_log` VALUES (609, 1, 'admin/news/create', 'GET', '127.0.0.1', '[]', '2022-02-01 23:12:02', '2022-02-01 23:12:02');
INSERT INTO `admin_operation_log` VALUES (610, 1, 'admin/news/create', 'GET', '127.0.0.1', '[]', '2022-02-01 23:12:49', '2022-02-01 23:12:49');
INSERT INTO `admin_operation_log` VALUES (611, 1, 'admin/news/create', 'GET', '127.0.0.1', '[]', '2022-02-01 23:12:50', '2022-02-01 23:12:50');
INSERT INTO `admin_operation_log` VALUES (612, 1, 'admin/news/create', 'GET', '127.0.0.1', '[]', '2022-02-01 23:14:37', '2022-02-01 23:14:37');
INSERT INTO `admin_operation_log` VALUES (613, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:14:40', '2022-02-01 23:14:40');
INSERT INTO `admin_operation_log` VALUES (614, 1, 'admin/platform/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:14:44', '2022-02-01 23:14:44');
INSERT INTO `admin_operation_log` VALUES (615, 1, 'admin/platform/2', 'PUT', '127.0.0.1', '{\"code\":\"RT166\",\"name\":\"Honda\",\"contact\":\"Sam\",\"phone\":\"1115-555-555\",\"enable\":\"1\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/platform\"}', '2022-02-01 23:14:56', '2022-02-01 23:14:56');
INSERT INTO `admin_operation_log` VALUES (616, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 23:14:56', '2022-02-01 23:14:56');
INSERT INTO `admin_operation_log` VALUES (617, 1, 'admin/platform/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:15:00', '2022-02-01 23:15:00');
INSERT INTO `admin_operation_log` VALUES (618, 1, 'admin/platform/1', 'PUT', '127.0.0.1', '{\"code\":\"TR694\",\"name\":\"\\u5bcc\\u6f58\\u9054\",\"contact\":\"test\",\"phone\":null,\"enable\":\"1\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/platform\"}', '2022-02-01 23:15:26', '2022-02-01 23:15:26');
INSERT INTO `admin_operation_log` VALUES (619, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 23:15:26', '2022-02-01 23:15:26');
INSERT INTO `admin_operation_log` VALUES (620, 1, 'admin/platform/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:15:29', '2022-02-01 23:15:29');
INSERT INTO `admin_operation_log` VALUES (621, 1, 'admin/platform/4', 'PUT', '127.0.0.1', '{\"code\":\"FFB264\",\"name\":\"\\u718a\\u8c93\",\"contact\":\"test\",\"phone\":\"0973-011-400\",\"enable\":\"1\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/platform\"}', '2022-02-01 23:15:34', '2022-02-01 23:15:34');
INSERT INTO `admin_operation_log` VALUES (622, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 23:15:34', '2022-02-01 23:15:34');
INSERT INTO `admin_operation_log` VALUES (623, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 23:15:37', '2022-02-01 23:15:37');
INSERT INTO `admin_operation_log` VALUES (624, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 23:15:39', '2022-02-01 23:15:39');
INSERT INTO `admin_operation_log` VALUES (625, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:15:40', '2022-02-01 23:15:40');
INSERT INTO `admin_operation_log` VALUES (626, 1, 'admin/news/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:15:42', '2022-02-01 23:15:42');
INSERT INTO `admin_operation_log` VALUES (627, 1, 'admin/news', 'POST', '127.0.0.1', '{\"platform\":{\"id\":\"2\"},\"title\":\"\\u6700\\u65b0\\u6d88\\u606f\",\"content\":\"\\u6e2c\\u8a66\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/news\"}', '2022-02-01 23:16:08', '2022-02-01 23:16:08');
INSERT INTO `admin_operation_log` VALUES (628, 1, 'admin/news/create', 'GET', '127.0.0.1', '[]', '2022-02-01 23:16:08', '2022-02-01 23:16:08');
INSERT INTO `admin_operation_log` VALUES (629, 1, 'admin/news/create', 'GET', '127.0.0.1', '[]', '2022-02-01 23:16:52', '2022-02-01 23:16:52');
INSERT INTO `admin_operation_log` VALUES (630, 1, 'admin/news', 'POST', '127.0.0.1', '{\"platform_id\":\"1\",\"title\":\"\\u6e2c\\u8a66\",\"content\":\"\\u9019\\u662f\\u6e2c\\u8a66\\u5167\\u5bb9\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\"}', '2022-02-01 23:17:06', '2022-02-01 23:17:06');
INSERT INTO `admin_operation_log` VALUES (631, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 23:17:06', '2022-02-01 23:17:06');
INSERT INTO `admin_operation_log` VALUES (632, 1, 'admin/news/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:17:11', '2022-02-01 23:17:11');
INSERT INTO `admin_operation_log` VALUES (633, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:17:20', '2022-02-01 23:17:20');
INSERT INTO `admin_operation_log` VALUES (634, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 23:18:58', '2022-02-01 23:18:58');
INSERT INTO `admin_operation_log` VALUES (635, 1, 'admin/news', 'GET', '127.0.0.1', '{\"platform\":{\"name\":\"1\"},\"title\":null,\"content\":null,\"created_at\":{\"start\":null,\"end\":null},\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:19:07', '2022-02-01 23:19:07');
INSERT INTO `admin_operation_log` VALUES (636, 1, 'admin/news', 'GET', '127.0.0.1', '{\"platform\":{\"name\":\"1\"},\"title\":null,\"content\":null,\"created_at\":{\"start\":null,\"end\":null}}', '2022-02-01 23:19:46', '2022-02-01 23:19:46');
INSERT INTO `admin_operation_log` VALUES (637, 1, 'admin/news', 'GET', '127.0.0.1', '{\"platform\":{\"name\":\"\\u5bcc\\u6f58\\u9054\"},\"title\":null,\"content\":null,\"created_at\":{\"start\":null,\"end\":null},\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:19:48', '2022-02-01 23:19:48');
INSERT INTO `admin_operation_log` VALUES (638, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"platform\":{\"name\":\"Honda\"},\"title\":null,\"content\":null,\"created_at\":{\"start\":null,\"end\":null}}', '2022-02-01 23:19:50', '2022-02-01 23:19:50');
INSERT INTO `admin_operation_log` VALUES (639, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"platform\":{\"name\":\"\\u5bcc\\u6f58\\u9054\"},\"title\":null,\"content\":null,\"created_at\":{\"start\":null,\"end\":null}}', '2022-02-01 23:19:52', '2022-02-01 23:19:52');
INSERT INTO `admin_operation_log` VALUES (640, 1, 'admin/news/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:19:58', '2022-02-01 23:19:58');
INSERT INTO `admin_operation_log` VALUES (641, 1, 'admin/news/create', 'GET', '127.0.0.1', '[]', '2022-02-01 23:20:24', '2022-02-01 23:20:24');
INSERT INTO `admin_operation_log` VALUES (642, 1, 'admin/news', 'POST', '127.0.0.1', '{\"platform_id\":\"1\",\"title\":\"\\u8dd1\\u99ac\\u71c82\",\"content\":\"\\u6700\\u65b0\\u6551\\u63f4\\u7279\\u50f9\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/news?content=&created_at%5Bstart%5D=&created_at%5Bend%5D=&platform%5Bname%5D=1&title=\"}', '2022-02-01 23:22:21', '2022-02-01 23:22:21');
INSERT INTO `admin_operation_log` VALUES (643, 1, 'admin/news', 'GET', '127.0.0.1', '{\"content\":null,\"created_at\":{\"start\":null,\"end\":null},\"platform\":{\"name\":\"1\"},\"title\":null}', '2022-02-01 23:22:21', '2022-02-01 23:22:21');
INSERT INTO `admin_operation_log` VALUES (644, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:22:26', '2022-02-01 23:22:26');
INSERT INTO `admin_operation_log` VALUES (645, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:22:28', '2022-02-01 23:22:28');
INSERT INTO `admin_operation_log` VALUES (646, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:22:29', '2022-02-01 23:22:29');
INSERT INTO `admin_operation_log` VALUES (647, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:22:31', '2022-02-01 23:22:31');
INSERT INTO `admin_operation_log` VALUES (648, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:23:00', '2022-02-01 23:23:00');
INSERT INTO `admin_operation_log` VALUES (649, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:23:01', '2022-02-01 23:23:01');
INSERT INTO `admin_operation_log` VALUES (650, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:23:02', '2022-02-01 23:23:02');
INSERT INTO `admin_operation_log` VALUES (651, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:23:03', '2022-02-01 23:23:03');
INSERT INTO `admin_operation_log` VALUES (652, 1, 'admin/news/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:23:05', '2022-02-01 23:23:05');
INSERT INTO `admin_operation_log` VALUES (653, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:23:07', '2022-02-01 23:23:07');
INSERT INTO `admin_operation_log` VALUES (654, 1, 'admin/news/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:23:10', '2022-02-01 23:23:10');
INSERT INTO `admin_operation_log` VALUES (655, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:23:11', '2022-02-01 23:23:11');
INSERT INTO `admin_operation_log` VALUES (656, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:23:13', '2022-02-01 23:23:13');
INSERT INTO `admin_operation_log` VALUES (657, 1, 'admin/_handle_action_', 'POST', '127.0.0.1', '{\"_key\":\"1\",\"_model\":\"App_Models_Platform\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\",\"_action\":\"Encore_Admin_Grid_Actions_Delete\",\"_input\":\"true\"}', '2022-02-01 23:23:19', '2022-02-01 23:23:19');
INSERT INTO `admin_operation_log` VALUES (658, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:23:19', '2022-02-01 23:23:19');
INSERT INTO `admin_operation_log` VALUES (659, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:23:21', '2022-02-01 23:23:21');
INSERT INTO `admin_operation_log` VALUES (660, 1, 'admin/news/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:23:32', '2022-02-01 23:23:32');
INSERT INTO `admin_operation_log` VALUES (661, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:23:35', '2022-02-01 23:23:35');
INSERT INTO `admin_operation_log` VALUES (662, 1, 'admin/news/1,2', 'DELETE', '127.0.0.1', '{\"_method\":\"delete\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\"}', '2022-02-01 23:23:46', '2022-02-01 23:23:46');
INSERT INTO `admin_operation_log` VALUES (663, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:23:46', '2022-02-01 23:23:46');
INSERT INTO `admin_operation_log` VALUES (664, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:23:48', '2022-02-01 23:23:48');
INSERT INTO `admin_operation_log` VALUES (665, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:23:52', '2022-02-01 23:23:52');
INSERT INTO `admin_operation_log` VALUES (666, 1, 'admin/news/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:23:53', '2022-02-01 23:23:53');
INSERT INTO `admin_operation_log` VALUES (667, 1, 'admin/news', 'POST', '127.0.0.1', '{\"platform_id\":\"2\",\"title\":\"\\u8dd1\\u99ac\\u71c81\",\"content\":\"\\u6700\\u65b0\\u512a\\u60e0\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/news\"}', '2022-02-01 23:24:15', '2022-02-01 23:24:15');
INSERT INTO `admin_operation_log` VALUES (668, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 23:24:15', '2022-02-01 23:24:15');
INSERT INTO `admin_operation_log` VALUES (669, 1, 'admin/news/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:24:18', '2022-02-01 23:24:18');
INSERT INTO `admin_operation_log` VALUES (670, 1, 'admin/news', 'POST', '127.0.0.1', '{\"platform_id\":\"2\",\"title\":\"\\u8dd1\\u99ac\\u71c82\",\"content\":\"\\u6700\\u65b0\\u512a\\u60e02\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/news\"}', '2022-02-01 23:24:32', '2022-02-01 23:24:32');
INSERT INTO `admin_operation_log` VALUES (671, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 23:24:32', '2022-02-01 23:24:32');
INSERT INTO `admin_operation_log` VALUES (672, 1, 'admin/news/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:25:04', '2022-02-01 23:25:04');
INSERT INTO `admin_operation_log` VALUES (673, 1, 'admin/news', 'POST', '127.0.0.1', '{\"platform_id\":\"4\",\"title\":\"\\u718a\\u8c93\\u512a\\u60e0\",\"content\":\"\\u5e74\\u5047\\u514d\\u904b\\u8cbb\",\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/news\"}', '2022-02-01 23:25:34', '2022-02-01 23:25:34');
INSERT INTO `admin_operation_log` VALUES (674, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-01 23:25:34', '2022-02-01 23:25:34');
INSERT INTO `admin_operation_log` VALUES (675, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:25:35', '2022-02-01 23:25:35');
INSERT INTO `admin_operation_log` VALUES (676, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:25:44', '2022-02-01 23:25:44');
INSERT INTO `admin_operation_log` VALUES (677, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:25:45', '2022-02-01 23:25:45');
INSERT INTO `admin_operation_log` VALUES (678, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:25:46', '2022-02-01 23:25:46');
INSERT INTO `admin_operation_log` VALUES (679, 1, 'admin/platform/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:25:50', '2022-02-01 23:25:50');
INSERT INTO `admin_operation_log` VALUES (680, 1, 'admin/platform/2/edit', 'GET', '127.0.0.1', '[]', '2022-02-01 23:26:10', '2022-02-01 23:26:10');
INSERT INTO `admin_operation_log` VALUES (681, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:26:24', '2022-02-01 23:26:24');
INSERT INTO `admin_operation_log` VALUES (682, 1, 'admin/platform/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:26:26', '2022-02-01 23:26:26');
INSERT INTO `admin_operation_log` VALUES (683, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:26:49', '2022-02-01 23:26:49');
INSERT INTO `admin_operation_log` VALUES (684, 1, 'admin/platform/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:26:55', '2022-02-01 23:26:55');
INSERT INTO `admin_operation_log` VALUES (685, 1, 'admin/platform/2/edit', 'GET', '127.0.0.1', '[]', '2022-02-01 23:27:18', '2022-02-01 23:27:18');
INSERT INTO `admin_operation_log` VALUES (686, 1, 'admin/platform/2/edit', 'GET', '127.0.0.1', '[]', '2022-02-01 23:27:19', '2022-02-01 23:27:19');
INSERT INTO `admin_operation_log` VALUES (687, 1, 'admin/platform/2/edit', 'GET', '127.0.0.1', '[]', '2022-02-01 23:27:19', '2022-02-01 23:27:19');
INSERT INTO `admin_operation_log` VALUES (688, 1, 'admin/platform/2/edit', 'GET', '127.0.0.1', '[]', '2022-02-01 23:27:19', '2022-02-01 23:27:19');
INSERT INTO `admin_operation_log` VALUES (689, 1, 'admin/platform/2/edit', 'GET', '127.0.0.1', '[]', '2022-02-01 23:27:19', '2022-02-01 23:27:19');
INSERT INTO `admin_operation_log` VALUES (690, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:27:22', '2022-02-01 23:27:22');
INSERT INTO `admin_operation_log` VALUES (691, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:27:22', '2022-02-01 23:27:22');
INSERT INTO `admin_operation_log` VALUES (692, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:27:23', '2022-02-01 23:27:23');
INSERT INTO `admin_operation_log` VALUES (693, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 23:27:43', '2022-02-01 23:27:43');
INSERT INTO `admin_operation_log` VALUES (694, 1, 'admin/platform/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:27:44', '2022-02-01 23:27:44');
INSERT INTO `admin_operation_log` VALUES (695, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 23:28:13', '2022-02-01 23:28:13');
INSERT INTO `admin_operation_log` VALUES (696, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 23:28:33', '2022-02-01 23:28:33');
INSERT INTO `admin_operation_log` VALUES (697, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 23:28:44', '2022-02-01 23:28:44');
INSERT INTO `admin_operation_log` VALUES (698, 1, 'admin/platform/create', 'GET', '127.0.0.1', '[]', '2022-02-01 23:29:19', '2022-02-01 23:29:19');
INSERT INTO `admin_operation_log` VALUES (699, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:29:21', '2022-02-01 23:29:21');
INSERT INTO `admin_operation_log` VALUES (700, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-01 23:29:22', '2022-02-01 23:29:22');
INSERT INTO `admin_operation_log` VALUES (701, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:29:23', '2022-02-01 23:29:23');
INSERT INTO `admin_operation_log` VALUES (702, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:29:37', '2022-02-01 23:29:37');
INSERT INTO `admin_operation_log` VALUES (703, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:29:39', '2022-02-01 23:29:39');
INSERT INTO `admin_operation_log` VALUES (704, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:29:40', '2022-02-01 23:29:40');
INSERT INTO `admin_operation_log` VALUES (705, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:29:42', '2022-02-01 23:29:42');
INSERT INTO `admin_operation_log` VALUES (706, 1, 'admin/disconut', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:29:43', '2022-02-01 23:29:43');
INSERT INTO `admin_operation_log` VALUES (707, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:29:44', '2022-02-01 23:29:44');
INSERT INTO `admin_operation_log` VALUES (708, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:29:45', '2022-02-01 23:29:45');
INSERT INTO `admin_operation_log` VALUES (709, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:29:50', '2022-02-01 23:29:50');
INSERT INTO `admin_operation_log` VALUES (710, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:29:51', '2022-02-01 23:29:51');
INSERT INTO `admin_operation_log` VALUES (711, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:29:52', '2022-02-01 23:29:52');
INSERT INTO `admin_operation_log` VALUES (712, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:29:58', '2022-02-01 23:29:58');
INSERT INTO `admin_operation_log` VALUES (713, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:29:59', '2022-02-01 23:29:59');
INSERT INTO `admin_operation_log` VALUES (714, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:30:00', '2022-02-01 23:30:00');
INSERT INTO `admin_operation_log` VALUES (715, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:30:00', '2022-02-01 23:30:00');
INSERT INTO `admin_operation_log` VALUES (716, 1, 'admin/disconut', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:30:01', '2022-02-01 23:30:01');
INSERT INTO `admin_operation_log` VALUES (717, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:30:01', '2022-02-01 23:30:01');
INSERT INTO `admin_operation_log` VALUES (718, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:30:02', '2022-02-01 23:30:02');
INSERT INTO `admin_operation_log` VALUES (719, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:30:06', '2022-02-01 23:30:06');
INSERT INTO `admin_operation_log` VALUES (720, 1, 'admin/auth/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:30:16', '2022-02-01 23:30:16');
INSERT INTO `admin_operation_log` VALUES (721, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:30:30', '2022-02-01 23:30:30');
INSERT INTO `admin_operation_log` VALUES (722, 1, 'admin/auth/roles/3/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-01 23:30:33', '2022-02-01 23:30:33');
INSERT INTO `admin_operation_log` VALUES (723, 1, 'admin/auth/roles/3', 'PUT', '127.0.0.1', '{\"slug\":\"user\",\"name\":\"\\u4f7f\\u7528\\u8005\",\"permissions\":[\"1\",null],\"_token\":\"BVzfv7XHdbyXb71yEJDbvWp9qYmYhUVMeXBdjkSw\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/auth\\/roles\"}', '2022-02-01 23:30:50', '2022-02-01 23:30:50');
INSERT INTO `admin_operation_log` VALUES (724, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '[]', '2022-02-01 23:30:50', '2022-02-01 23:30:50');
INSERT INTO `admin_operation_log` VALUES (725, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-02 11:09:46', '2022-02-02 11:09:46');
INSERT INTO `admin_operation_log` VALUES (726, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-02 11:09:47', '2022-02-02 11:09:47');
INSERT INTO `admin_operation_log` VALUES (727, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 11:09:58', '2022-02-02 11:09:58');
INSERT INTO `admin_operation_log` VALUES (728, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 11:10:03', '2022-02-02 11:10:03');
INSERT INTO `admin_operation_log` VALUES (729, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 11:10:36', '2022-02-02 11:10:36');
INSERT INTO `admin_operation_log` VALUES (730, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 11:10:38', '2022-02-02 11:10:38');
INSERT INTO `admin_operation_log` VALUES (731, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 11:10:40', '2022-02-02 11:10:40');
INSERT INTO `admin_operation_log` VALUES (732, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 11:10:41', '2022-02-02 11:10:41');
INSERT INTO `admin_operation_log` VALUES (733, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 11:16:01', '2022-02-02 11:16:01');
INSERT INTO `admin_operation_log` VALUES (734, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 11:16:06', '2022-02-02 11:16:06');
INSERT INTO `admin_operation_log` VALUES (735, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 11:16:06', '2022-02-02 11:16:06');
INSERT INTO `admin_operation_log` VALUES (736, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 11:16:07', '2022-02-02 11:16:07');
INSERT INTO `admin_operation_log` VALUES (737, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-02 11:16:18', '2022-02-02 11:16:18');
INSERT INTO `admin_operation_log` VALUES (738, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_sort\":{\"column\":\"platform.name\",\"type\":\"desc\"},\"_pjax\":\"#pjax-container\"}', '2022-02-02 11:16:20', '2022-02-02 11:16:20');
INSERT INTO `admin_operation_log` VALUES (739, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_sort\":{\"column\":\"platform.name\",\"type\":\"asc\"},\"_pjax\":\"#pjax-container\"}', '2022-02-02 11:16:21', '2022-02-02 11:16:21');
INSERT INTO `admin_operation_log` VALUES (740, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 11:16:32', '2022-02-02 11:16:32');
INSERT INTO `admin_operation_log` VALUES (741, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-02 11:17:07', '2022-02-02 11:17:07');
INSERT INTO `admin_operation_log` VALUES (742, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 11:17:08', '2022-02-02 11:17:08');
INSERT INTO `admin_operation_log` VALUES (743, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 11:17:28', '2022-02-02 11:17:28');
INSERT INTO `admin_operation_log` VALUES (744, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 11:17:56', '2022-02-02 11:17:56');
INSERT INTO `admin_operation_log` VALUES (745, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-02 11:21:40', '2022-02-02 11:21:40');
INSERT INTO `admin_operation_log` VALUES (746, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 11:21:41', '2022-02-02 11:21:41');
INSERT INTO `admin_operation_log` VALUES (747, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 11:21:44', '2022-02-02 11:21:44');
INSERT INTO `admin_operation_log` VALUES (748, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:21:47', '2022-02-02 11:21:47');
INSERT INTO `admin_operation_log` VALUES (749, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:21:50', '2022-02-02 11:21:50');
INSERT INTO `admin_operation_log` VALUES (750, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:21:53', '2022-02-02 11:21:53');
INSERT INTO `admin_operation_log` VALUES (751, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:21:55', '2022-02-02 11:21:55');
INSERT INTO `admin_operation_log` VALUES (752, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:21:58', '2022-02-02 11:21:58');
INSERT INTO `admin_operation_log` VALUES (753, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:22:01', '2022-02-02 11:22:01');
INSERT INTO `admin_operation_log` VALUES (754, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:22:04', '2022-02-02 11:22:04');
INSERT INTO `admin_operation_log` VALUES (755, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:22:06', '2022-02-02 11:22:06');
INSERT INTO `admin_operation_log` VALUES (756, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:22:09', '2022-02-02 11:22:09');
INSERT INTO `admin_operation_log` VALUES (757, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:22:12', '2022-02-02 11:22:12');
INSERT INTO `admin_operation_log` VALUES (758, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:22:15', '2022-02-02 11:22:15');
INSERT INTO `admin_operation_log` VALUES (759, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:22:17', '2022-02-02 11:22:17');
INSERT INTO `admin_operation_log` VALUES (760, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:22:20', '2022-02-02 11:22:20');
INSERT INTO `admin_operation_log` VALUES (761, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:22:23', '2022-02-02 11:22:23');
INSERT INTO `admin_operation_log` VALUES (762, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:22:26', '2022-02-02 11:22:26');
INSERT INTO `admin_operation_log` VALUES (763, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:22:28', '2022-02-02 11:22:28');
INSERT INTO `admin_operation_log` VALUES (764, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:22:32', '2022-02-02 11:22:32');
INSERT INTO `admin_operation_log` VALUES (765, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:22:35', '2022-02-02 11:22:35');
INSERT INTO `admin_operation_log` VALUES (766, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:22:37', '2022-02-02 11:22:37');
INSERT INTO `admin_operation_log` VALUES (767, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:22:40', '2022-02-02 11:22:40');
INSERT INTO `admin_operation_log` VALUES (768, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:22:43', '2022-02-02 11:22:43');
INSERT INTO `admin_operation_log` VALUES (769, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:23:16', '2022-02-02 11:23:16');
INSERT INTO `admin_operation_log` VALUES (770, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-02 11:24:02', '2022-02-02 11:24:02');
INSERT INTO `admin_operation_log` VALUES (771, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 11:24:04', '2022-02-02 11:24:04');
INSERT INTO `admin_operation_log` VALUES (772, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 11:30:20', '2022-02-02 11:30:20');
INSERT INTO `admin_operation_log` VALUES (773, 1, 'admin/rule/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 11:30:22', '2022-02-02 11:30:22');
INSERT INTO `admin_operation_log` VALUES (774, 1, 'admin/rule/create', 'GET', '127.0.0.1', '[]', '2022-02-02 11:31:18', '2022-02-02 11:31:18');
INSERT INTO `admin_operation_log` VALUES (775, 1, 'admin/rule/create', 'GET', '127.0.0.1', '[]', '2022-02-02 11:31:45', '2022-02-02 11:31:45');
INSERT INTO `admin_operation_log` VALUES (776, 1, 'admin/rule/create', 'GET', '127.0.0.1', '[]', '2022-02-02 11:40:20', '2022-02-02 11:40:20');
INSERT INTO `admin_operation_log` VALUES (777, 1, 'admin/rule/create', 'GET', '127.0.0.1', '[]', '2022-02-02 11:40:49', '2022-02-02 11:40:49');
INSERT INTO `admin_operation_log` VALUES (778, 1, 'admin/rule', 'POST', '127.0.0.1', '{\"platform_id\":\"2\",\"enable\":\"1\",\"content\":\"<p><strong>\\u6e2c\\u8a66\\u670d\\u52d9\\u689d\\u6b3e<\\/strong><\\/p>\",\"_token\":\"mFvnRqC6ody7XNNmbk3QVUdfRqbc01rmcvUxYe13\"}', '2022-02-02 11:41:07', '2022-02-02 11:41:07');
INSERT INTO `admin_operation_log` VALUES (779, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:41:07', '2022-02-02 11:41:07');
INSERT INTO `admin_operation_log` VALUES (780, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:42:00', '2022-02-02 11:42:00');
INSERT INTO `admin_operation_log` VALUES (781, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:42:14', '2022-02-02 11:42:14');
INSERT INTO `admin_operation_log` VALUES (782, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:42:16', '2022-02-02 11:42:16');
INSERT INTO `admin_operation_log` VALUES (783, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:45:25', '2022-02-02 11:45:25');
INSERT INTO `admin_operation_log` VALUES (784, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:45:26', '2022-02-02 11:45:26');
INSERT INTO `admin_operation_log` VALUES (785, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:45:42', '2022-02-02 11:45:42');
INSERT INTO `admin_operation_log` VALUES (786, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:47:53', '2022-02-02 11:47:53');
INSERT INTO `admin_operation_log` VALUES (787, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:48:41', '2022-02-02 11:48:41');
INSERT INTO `admin_operation_log` VALUES (788, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:49:32', '2022-02-02 11:49:32');
INSERT INTO `admin_operation_log` VALUES (789, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:49:47', '2022-02-02 11:49:47');
INSERT INTO `admin_operation_log` VALUES (790, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:49:49', '2022-02-02 11:49:49');
INSERT INTO `admin_operation_log` VALUES (791, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:49:50', '2022-02-02 11:49:50');
INSERT INTO `admin_operation_log` VALUES (792, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:49:59', '2022-02-02 11:49:59');
INSERT INTO `admin_operation_log` VALUES (793, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:50:30', '2022-02-02 11:50:30');
INSERT INTO `admin_operation_log` VALUES (794, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:50:31', '2022-02-02 11:50:31');
INSERT INTO `admin_operation_log` VALUES (795, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:50:32', '2022-02-02 11:50:32');
INSERT INTO `admin_operation_log` VALUES (796, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:50:32', '2022-02-02 11:50:32');
INSERT INTO `admin_operation_log` VALUES (797, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:50:32', '2022-02-02 11:50:32');
INSERT INTO `admin_operation_log` VALUES (798, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:52:11', '2022-02-02 11:52:11');
INSERT INTO `admin_operation_log` VALUES (799, 1, 'admin/rule/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 11:53:13', '2022-02-02 11:53:13');
INSERT INTO `admin_operation_log` VALUES (800, 1, 'admin/rule', 'POST', '127.0.0.1', '{\"platform_id\":\"2\",\"enable\":\"1\",\"content\":\"<ol>\\r\\n\\t<li>\\u6e2c\\u8a66\\u670d\\u52d9\\u689d\\u6b3e<\\/li>\\r\\n\\t<li>\\u7b2c\\u4e8c\\u7b46\\u6e2c\\u8a66<\\/li>\\r\\n\\t<li>\\u7b2c\\u4e09\\u7b46\\u6e2c\\u8a66<\\/li>\\r\\n<\\/ol>\",\"_token\":\"mFvnRqC6ody7XNNmbk3QVUdfRqbc01rmcvUxYe13\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/rule\"}', '2022-02-02 11:53:44', '2022-02-02 11:53:44');
INSERT INTO `admin_operation_log` VALUES (801, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 11:53:44', '2022-02-02 11:53:44');
INSERT INTO `admin_operation_log` VALUES (802, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"platform\":{\"name\":null},\"content\":\"\\u6e2c\\u8a66\",\"created_at\":{\"start\":null,\"end\":null},\"_pjax\":\"#pjax-container\"}', '2022-02-02 11:54:26', '2022-02-02 11:54:26');
INSERT INTO `admin_operation_log` VALUES (803, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"platform\":{\"name\":\"Honda\"},\"content\":\"\\u6e2c\\u8a66\",\"created_at\":{\"start\":null,\"end\":null}}', '2022-02-02 11:54:30', '2022-02-02 11:54:30');
INSERT INTO `admin_operation_log` VALUES (804, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"platform\":{\"name\":\"Honda\"},\"content\":\"132\",\"created_at\":{\"start\":null,\"end\":null}}', '2022-02-02 11:54:33', '2022-02-02 11:54:33');
INSERT INTO `admin_operation_log` VALUES (805, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"platform\":{\"name\":\"Honda\"},\"content\":null,\"created_at\":{\"start\":null,\"end\":null}}', '2022-02-02 11:54:36', '2022-02-02 11:54:36');
INSERT INTO `admin_operation_log` VALUES (806, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 11:55:00', '2022-02-02 11:55:00');
INSERT INTO `admin_operation_log` VALUES (807, 1, 'admin/pay', 'GET', '127.0.0.1', '[]', '2022-02-02 12:00:11', '2022-02-02 12:00:11');
INSERT INTO `admin_operation_log` VALUES (808, 1, 'admin/pay', 'GET', '127.0.0.1', '[]', '2022-02-02 12:00:33', '2022-02-02 12:00:33');
INSERT INTO `admin_operation_log` VALUES (809, 1, 'admin/pay/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:00:37', '2022-02-02 12:00:37');
INSERT INTO `admin_operation_log` VALUES (810, 1, 'admin/pay/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:00:58', '2022-02-02 12:00:58');
INSERT INTO `admin_operation_log` VALUES (811, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:02:51', '2022-02-02 12:02:51');
INSERT INTO `admin_operation_log` VALUES (812, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:02:53', '2022-02-02 12:02:53');
INSERT INTO `admin_operation_log` VALUES (813, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:02:56', '2022-02-02 12:02:56');
INSERT INTO `admin_operation_log` VALUES (814, 1, 'admin/rule/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:02:59', '2022-02-02 12:02:59');
INSERT INTO `admin_operation_log` VALUES (815, 1, 'admin/rule/1', 'PUT', '127.0.0.1', '{\"platform_id\":\"2\",\"enable\":\"1\",\"content\":\"<h2>\\u670d\\u52d9\\u6d41\\u7a0b\\u898f\\u7bc4<\\/h2>\\r\\n\\r\\n<p>\\u7533\\u544a\\u672c\\u670d\\u52d9\\u6642\\uff0c\\u8acb\\u5982\\u5be6\\u586b\\u5beb\\u7533\\u544a\\u5167\\u5bb9\\uff0c\\u5f8c\\u7e8c\\u7531\\u670d\\u52d9\\u4eba\\u54e1\\u8207\\u60a8\\u78ba\\u8a8d\\u8eab\\u5206\\u4e26\\u9810\\u5831\\u62b5\\u9054\\u6642\\u9593\\u3001\\u670d\\u52d9\\u8eca\\u8eca\\u865f\\u3001\\u670d\\u52d9\\u4eba\\u54e1\\u59d3\\u540d\\u3002<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<h2>\\u670d\\u52d9\\u7bc4\\u570d<\\/h2>\\r\\n\\r\\n<p>\\u4fc2\\u65bc\\u81fa\\u7063\\u672c\\u5cf6\\u3001\\u6f8e\\u6e56\\u3001\\u91d1\\u9580\\u53ca\\u99ac\\u7956\\u5730\\u5340\\u4e14\\u6551\\u63f4\\u8eca\\u8f1b\\u6240\\u80fd\\u884c\\u99db\\u53ca\\u4f5c\\u696d\\u4e4b\\u9053\\u8def\\u8005\\uff0c\\u7686\\u53ef\\u4eab\\u6709\\u672c\\u670d\\u52d9\\u3002<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<h2>\\u6536\\u8cbb\\u6a19\\u6e96<\\/h2>\\r\\n\\r\\n<ul>\\r\\n\\t<li>\\u5e73\\u9762\\u9053\\u8def 10\\u516c\\u91cc\\u51671,200\\u5143\\uff0c\\u8d85\\u904e\\u6bcf\\u516c\\u91cc\\u52a0\\u653650\\u5143<\\/li>\\r\\n\\t<li>\\u9ad8\\u901f\\u516c\\u8def(\\u4e0d\\u542b\\u570b\\u4e94) \\u4f9d\\u9ad8\\u5de5\\u5c40\\u898f\\u5b9a\\uff0c\\u8996\\u65b0\\u8eca\\u50f9\\u8207\\u8eca\\u8eab\\u9ad8\\u5ea6\\u4e0d\\u540c\\uff0c10\\u516c\\u91cc\\u51671,500~20,000\\u5143<\\/li>\\r\\n<\\/ul>\",\"_token\":\"mFvnRqC6ody7XNNmbk3QVUdfRqbc01rmcvUxYe13\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/rule\"}', '2022-02-02 12:03:03', '2022-02-02 12:03:03');
INSERT INTO `admin_operation_log` VALUES (816, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 12:03:03', '2022-02-02 12:03:03');
INSERT INTO `admin_operation_log` VALUES (817, 1, 'admin/rule/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:03:37', '2022-02-02 12:03:37');
INSERT INTO `admin_operation_log` VALUES (818, 1, 'admin/rule/1', 'PUT', '127.0.0.1', '{\"platform_id\":\"2\",\"enable\":\"1\",\"content\":\"<h2>\\u7533\\u544a\\u672c\\u670d\\u52d9\\u6642\\uff0c\\u8acb\\u5982\\u5be6\\u586b\\u5beb\\u7533\\u544a\\u5167\\u5bb9\\uff0c\\u5f8c\\u7e8c\\u7531\\u670d\\u52d9\\u4eba\\u54e1\\u8207\\u60a8\\u78ba\\u8a8d\\u8eab\\u5206\\u4e26\\u9810\\u5831\\u62b5\\u9054\\u6642\\u9593\\u3001\\u670d\\u52d9\\u8eca\\u8eca\\u865f\\u3001\\u670d\\u52d9\\u4eba\\u54e1\\u59d3\\u540d\\u3002<\\/h2>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<h2>\\u670d\\u52d9\\u7bc4\\u570d<\\/h2>\\r\\n\\r\\n<p>\\u4fc2\\u65bc\\u81fa\\u7063\\u672c\\u5cf6\\u3001\\u6f8e\\u6e56\\u3001\\u91d1\\u9580\\u53ca\\u99ac\\u7956\\u5730\\u5340\\u4e14\\u6551\\u63f4\\u8eca\\u8f1b\\u6240\\u80fd\\u884c\\u99db\\u53ca\\u4f5c\\u696d\\u4e4b\\u9053\\u8def\\u8005\\uff0c\\u7686\\u53ef\\u4eab\\u6709\\u672c\\u670d\\u52d9\\u3002<\\/p>\",\"_token\":\"mFvnRqC6ody7XNNmbk3QVUdfRqbc01rmcvUxYe13\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/rule\"}', '2022-02-02 12:03:46', '2022-02-02 12:03:46');
INSERT INTO `admin_operation_log` VALUES (819, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 12:03:47', '2022-02-02 12:03:47');
INSERT INTO `admin_operation_log` VALUES (820, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:04:13', '2022-02-02 12:04:13');
INSERT INTO `admin_operation_log` VALUES (821, 1, 'admin/pay/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:04:15', '2022-02-02 12:04:15');
INSERT INTO `admin_operation_log` VALUES (822, 1, 'admin/pay', 'POST', '127.0.0.1', '{\"platform_id\":\"2\",\"enable\":\"1\",\"content\":\"<ul>\\r\\n\\t<li>\\u5e73\\u9762\\u9053\\u8def 10\\u516c\\u91cc\\u51671,200\\u5143\\uff0c\\u8d85\\u904e\\u6bcf\\u516c\\u91cc\\u52a0\\u653650\\u5143<\\/li>\\r\\n\\t<li>\\u9ad8\\u901f\\u516c\\u8def(\\u4e0d\\u542b\\u570b\\u4e94) \\u4f9d\\u9ad8\\u5de5\\u5c40\\u898f\\u5b9a\\uff0c\\u8996\\u65b0\\u8eca\\u50f9\\u8207\\u8eca\\u8eab\\u9ad8\\u5ea6\\u4e0d\\u540c\\uff0c10\\u516c\\u91cc\\u51671,500~20,000\\u5143<\\/li>\\r\\n<\\/ul>\",\"_token\":\"mFvnRqC6ody7XNNmbk3QVUdfRqbc01rmcvUxYe13\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/pay\"}', '2022-02-02 12:04:21', '2022-02-02 12:04:21');
INSERT INTO `admin_operation_log` VALUES (823, 1, 'admin/pay/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:04:21', '2022-02-02 12:04:21');
INSERT INTO `admin_operation_log` VALUES (824, 1, 'admin/pay', 'POST', '127.0.0.1', '{\"platform_id\":\"2\",\"enable\":\"1\",\"content\":\"<ul>\\r\\n\\t<li>\\u5e73\\u9762\\u9053\\u8def 10\\u516c\\u91cc\\u51671,200\\u5143\\uff0c\\u8d85\\u904e\\u6bcf\\u516c\\u91cc\\u52a0\\u653650\\u5143<\\/li>\\r\\n\\t<li>\\u9ad8\\u901f\\u516c\\u8def(\\u4e0d\\u542b\\u570b\\u4e94) \\u4f9d\\u9ad8\\u5de5\\u5c40\\u898f\\u5b9a\\uff0c\\u8996\\u65b0\\u8eca\\u50f9\\u8207\\u8eca\\u8eab\\u9ad8\\u5ea6\\u4e0d\\u540c\\uff0c10\\u516c\\u91cc\\u51671,500~20,000\\u5143<\\/li>\\r\\n<\\/ul>\",\"_token\":\"mFvnRqC6ody7XNNmbk3QVUdfRqbc01rmcvUxYe13\"}', '2022-02-02 12:05:23', '2022-02-02 12:05:23');
INSERT INTO `admin_operation_log` VALUES (825, 1, 'admin/pay', 'GET', '127.0.0.1', '[]', '2022-02-02 12:05:23', '2022-02-02 12:05:23');
INSERT INTO `admin_operation_log` VALUES (826, 1, 'admin/pay', 'GET', '127.0.0.1', '[]', '2022-02-02 12:06:19', '2022-02-02 12:06:19');
INSERT INTO `admin_operation_log` VALUES (827, 1, 'admin/pay', 'GET', '127.0.0.1', '[]', '2022-02-02 12:06:22', '2022-02-02 12:06:22');
INSERT INTO `admin_operation_log` VALUES (828, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:06:23', '2022-02-02 12:06:23');
INSERT INTO `admin_operation_log` VALUES (829, 1, 'admin/disconut', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:06:24', '2022-02-02 12:06:24');
INSERT INTO `admin_operation_log` VALUES (830, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:07:01', '2022-02-02 12:07:01');
INSERT INTO `admin_operation_log` VALUES (831, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:07:13', '2022-02-02 12:07:13');
INSERT INTO `admin_operation_log` VALUES (832, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:07:14', '2022-02-02 12:07:14');
INSERT INTO `admin_operation_log` VALUES (833, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:07:15', '2022-02-02 12:07:15');
INSERT INTO `admin_operation_log` VALUES (834, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:07:16', '2022-02-02 12:07:16');
INSERT INTO `admin_operation_log` VALUES (835, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:07:17', '2022-02-02 12:07:17');
INSERT INTO `admin_operation_log` VALUES (836, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:07:18', '2022-02-02 12:07:18');
INSERT INTO `admin_operation_log` VALUES (837, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:07:18', '2022-02-02 12:07:18');
INSERT INTO `admin_operation_log` VALUES (838, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:07:20', '2022-02-02 12:07:20');
INSERT INTO `admin_operation_log` VALUES (839, 1, 'admin/ad', 'GET', '127.0.0.1', '[]', '2022-02-02 12:17:20', '2022-02-02 12:17:20');
INSERT INTO `admin_operation_log` VALUES (840, 1, 'admin/ad', 'GET', '127.0.0.1', '[]', '2022-02-02 12:18:06', '2022-02-02 12:18:06');
INSERT INTO `admin_operation_log` VALUES (841, 1, 'admin/ad', 'GET', '127.0.0.1', '[]', '2022-02-02 12:18:56', '2022-02-02 12:18:56');
INSERT INTO `admin_operation_log` VALUES (842, 1, 'admin/ad', 'GET', '127.0.0.1', '[]', '2022-02-02 12:20:03', '2022-02-02 12:20:03');
INSERT INTO `admin_operation_log` VALUES (843, 1, 'admin/ad', 'GET', '127.0.0.1', '[]', '2022-02-02 12:20:46', '2022-02-02 12:20:46');
INSERT INTO `admin_operation_log` VALUES (844, 1, 'admin/ad', 'GET', '127.0.0.1', '[]', '2022-02-02 12:20:54', '2022-02-02 12:20:54');
INSERT INTO `admin_operation_log` VALUES (845, 1, 'admin/ad', 'GET', '127.0.0.1', '[]', '2022-02-02 12:20:55', '2022-02-02 12:20:55');
INSERT INTO `admin_operation_log` VALUES (846, 1, 'admin/ad', 'GET', '127.0.0.1', '[]', '2022-02-02 12:21:11', '2022-02-02 12:21:11');
INSERT INTO `admin_operation_log` VALUES (847, 1, 'admin/ad/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:21:17', '2022-02-02 12:21:17');
INSERT INTO `admin_operation_log` VALUES (848, 1, 'admin/ad/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:22:41', '2022-02-02 12:22:41');
INSERT INTO `admin_operation_log` VALUES (849, 1, 'admin/ad/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:25:05', '2022-02-02 12:25:05');
INSERT INTO `admin_operation_log` VALUES (850, 1, 'admin/ad/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:25:33', '2022-02-02 12:25:33');
INSERT INTO `admin_operation_log` VALUES (851, 1, 'admin/ad/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:25:42', '2022-02-02 12:25:42');
INSERT INTO `admin_operation_log` VALUES (852, 1, 'admin/ad/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:25:44', '2022-02-02 12:25:44');
INSERT INTO `admin_operation_log` VALUES (853, 1, 'admin/ad/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:25:52', '2022-02-02 12:25:52');
INSERT INTO `admin_operation_log` VALUES (854, 1, 'admin/ad', 'POST', '127.0.0.1', '{\"platform_id\":\"2\",\"url\":\"http:\\/\\/yahoo.com.tw\",\"seq\":\"1\",\"enable\":\"1\",\"_token\":\"mFvnRqC6ody7XNNmbk3QVUdfRqbc01rmcvUxYe13\"}', '2022-02-02 12:26:44', '2022-02-02 12:26:44');
INSERT INTO `admin_operation_log` VALUES (855, 1, 'admin/ad', 'GET', '127.0.0.1', '[]', '2022-02-02 12:26:44', '2022-02-02 12:26:44');
INSERT INTO `admin_operation_log` VALUES (856, 1, 'admin/ad', 'GET', '127.0.0.1', '[]', '2022-02-02 12:27:31', '2022-02-02 12:27:31');
INSERT INTO `admin_operation_log` VALUES (857, 1, 'admin/ad', 'GET', '127.0.0.1', '[]', '2022-02-02 12:27:40', '2022-02-02 12:27:40');
INSERT INTO `admin_operation_log` VALUES (858, 1, 'admin/ad', 'GET', '127.0.0.1', '[]', '2022-02-02 12:27:49', '2022-02-02 12:27:49');
INSERT INTO `admin_operation_log` VALUES (859, 1, 'admin/ad', 'GET', '127.0.0.1', '[]', '2022-02-02 12:28:06', '2022-02-02 12:28:06');
INSERT INTO `admin_operation_log` VALUES (860, 1, 'admin/ad/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:28:16', '2022-02-02 12:28:16');
INSERT INTO `admin_operation_log` VALUES (861, 1, 'admin/ad', 'POST', '127.0.0.1', '{\"platform_id\":\"2\",\"url\":\"http:\\/\\/yahoo.com.tw\",\"seq\":\"2\",\"enable\":\"1\",\"_token\":\"mFvnRqC6ody7XNNmbk3QVUdfRqbc01rmcvUxYe13\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/ad\"}', '2022-02-02 12:28:31', '2022-02-02 12:28:31');
INSERT INTO `admin_operation_log` VALUES (862, 1, 'admin/ad', 'GET', '127.0.0.1', '[]', '2022-02-02 12:28:31', '2022-02-02 12:28:31');
INSERT INTO `admin_operation_log` VALUES (863, 1, 'admin/ad/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:28:40', '2022-02-02 12:28:40');
INSERT INTO `admin_operation_log` VALUES (864, 1, 'admin/ad', 'POST', '127.0.0.1', '{\"platform_id\":\"4\",\"url\":\"http:\\/\\/yahoo.com.tw\",\"seq\":\"0\",\"enable\":\"1\",\"_token\":\"mFvnRqC6ody7XNNmbk3QVUdfRqbc01rmcvUxYe13\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/ad\"}', '2022-02-02 12:28:52', '2022-02-02 12:28:52');
INSERT INTO `admin_operation_log` VALUES (865, 1, 'admin/ad', 'GET', '127.0.0.1', '[]', '2022-02-02 12:28:52', '2022-02-02 12:28:52');
INSERT INTO `admin_operation_log` VALUES (866, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"platform\":{\"name\":null},\"enable\":\"0\",\"created_at\":{\"start\":null,\"end\":null},\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:29:04', '2022-02-02 12:29:04');
INSERT INTO `admin_operation_log` VALUES (867, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"platform\":{\"name\":null},\"enable\":\"1\",\"created_at\":{\"start\":null,\"end\":null}}', '2022-02-02 12:29:06', '2022-02-02 12:29:06');
INSERT INTO `admin_operation_log` VALUES (868, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"platform\":{\"name\":\"Honda\"},\"enable\":\"1\",\"created_at\":{\"start\":null,\"end\":null}}', '2022-02-02 12:29:09', '2022-02-02 12:29:09');
INSERT INTO `admin_operation_log` VALUES (869, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"platform\":{\"name\":\"\\u718a\\u8c93\"},\"enable\":\"1\",\"created_at\":{\"start\":null,\"end\":null}}', '2022-02-02 12:29:11', '2022-02-02 12:29:11');
INSERT INTO `admin_operation_log` VALUES (870, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:29:12', '2022-02-02 12:29:12');
INSERT INTO `admin_operation_log` VALUES (871, 1, 'admin/disconut', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:29:16', '2022-02-02 12:29:16');
INSERT INTO `admin_operation_log` VALUES (872, 1, 'admin/disconut', 'GET', '127.0.0.1', '[]', '2022-02-02 12:33:08', '2022-02-02 12:33:08');
INSERT INTO `admin_operation_log` VALUES (873, 1, 'admin/disconut', 'GET', '127.0.0.1', '[]', '2022-02-02 12:33:22', '2022-02-02 12:33:22');
INSERT INTO `admin_operation_log` VALUES (874, 1, 'admin/disconut', 'GET', '127.0.0.1', '[]', '2022-02-02 12:35:26', '2022-02-02 12:35:26');
INSERT INTO `admin_operation_log` VALUES (875, 1, 'admin/disconut/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:35:28', '2022-02-02 12:35:28');
INSERT INTO `admin_operation_log` VALUES (876, 1, 'admin/disconut/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:35:58', '2022-02-02 12:35:58');
INSERT INTO `admin_operation_log` VALUES (877, 1, 'admin/disconut/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:36:57', '2022-02-02 12:36:57');
INSERT INTO `admin_operation_log` VALUES (878, 1, 'admin/disconut', 'POST', '127.0.0.1', '{\"platform_id\":\"2\",\"code\":\"CDR1001\",\"start_date\":\"2022-02-01\",\"end_date\":\"2022-02-03\",\"price\":\"100.00\",\"_token\":\"mFvnRqC6ody7XNNmbk3QVUdfRqbc01rmcvUxYe13\"}', '2022-02-02 12:37:16', '2022-02-02 12:37:16');
INSERT INTO `admin_operation_log` VALUES (879, 1, 'admin/disconut/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:37:17', '2022-02-02 12:37:17');
INSERT INTO `admin_operation_log` VALUES (880, 1, 'admin/disconut', 'POST', '127.0.0.1', '{\"platform_id\":\"2\",\"code\":\"CDR1001\",\"start_date\":\"2022-02-01\",\"end_date\":\"2022-02-03\",\"price\":\"100.00\",\"_token\":\"mFvnRqC6ody7XNNmbk3QVUdfRqbc01rmcvUxYe13\"}', '2022-02-02 12:37:52', '2022-02-02 12:37:52');
INSERT INTO `admin_operation_log` VALUES (881, 1, 'admin/disconut', 'GET', '127.0.0.1', '[]', '2022-02-02 12:37:53', '2022-02-02 12:37:53');
INSERT INTO `admin_operation_log` VALUES (882, 1, 'admin/disconut/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:37:59', '2022-02-02 12:37:59');
INSERT INTO `admin_operation_log` VALUES (883, 1, 'admin/disconut/1/edit', 'GET', '127.0.0.1', '[]', '2022-02-02 12:38:32', '2022-02-02 12:38:32');
INSERT INTO `admin_operation_log` VALUES (884, 1, 'admin/disconut/1/edit', 'GET', '127.0.0.1', '[]', '2022-02-02 12:38:34', '2022-02-02 12:38:34');
INSERT INTO `admin_operation_log` VALUES (885, 1, 'admin/disconut/1/edit', 'GET', '127.0.0.1', '[]', '2022-02-02 12:38:37', '2022-02-02 12:38:37');
INSERT INTO `admin_operation_log` VALUES (886, 1, 'admin/disconut/1/edit', 'GET', '127.0.0.1', '[]', '2022-02-02 12:38:38', '2022-02-02 12:38:38');
INSERT INTO `admin_operation_log` VALUES (887, 1, 'admin/disconut/1/edit', 'GET', '127.0.0.1', '[]', '2022-02-02 12:38:38', '2022-02-02 12:38:38');
INSERT INTO `admin_operation_log` VALUES (888, 1, 'admin/disconut/1/edit', 'GET', '127.0.0.1', '[]', '2022-02-02 12:38:38', '2022-02-02 12:38:38');
INSERT INTO `admin_operation_log` VALUES (889, 1, 'admin/disconut', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:38:50', '2022-02-02 12:38:50');
INSERT INTO `admin_operation_log` VALUES (890, 1, 'admin/disconut', 'GET', '127.0.0.1', '[]', '2022-02-02 12:38:53', '2022-02-02 12:38:53');
INSERT INTO `admin_operation_log` VALUES (891, 1, 'admin/disconut/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:38:55', '2022-02-02 12:38:55');
INSERT INTO `admin_operation_log` VALUES (892, 1, 'admin/disconut/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:39:37', '2022-02-02 12:39:37');
INSERT INTO `admin_operation_log` VALUES (893, 1, 'admin/disconut/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:39:38', '2022-02-02 12:39:38');
INSERT INTO `admin_operation_log` VALUES (894, 1, 'admin/disconut/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:39:40', '2022-02-02 12:39:40');
INSERT INTO `admin_operation_log` VALUES (895, 1, 'admin/disconut/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:39:53', '2022-02-02 12:39:53');
INSERT INTO `admin_operation_log` VALUES (896, 1, 'admin/disconut/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:40:53', '2022-02-02 12:40:53');
INSERT INTO `admin_operation_log` VALUES (897, 1, 'admin/disconut/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:40:55', '2022-02-02 12:40:55');
INSERT INTO `admin_operation_log` VALUES (898, 1, 'admin/disconut/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:40:55', '2022-02-02 12:40:55');
INSERT INTO `admin_operation_log` VALUES (899, 1, 'admin/disconut/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:40:56', '2022-02-02 12:40:56');
INSERT INTO `admin_operation_log` VALUES (900, 1, 'admin/disconut/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:40:57', '2022-02-02 12:40:57');
INSERT INTO `admin_operation_log` VALUES (901, 1, 'admin/disconut/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:40:57', '2022-02-02 12:40:57');
INSERT INTO `admin_operation_log` VALUES (902, 1, 'admin/disconut/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:40:57', '2022-02-02 12:40:57');
INSERT INTO `admin_operation_log` VALUES (903, 1, 'admin/disconut', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:40:58', '2022-02-02 12:40:58');
INSERT INTO `admin_operation_log` VALUES (904, 1, 'admin/disconut/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:41:00', '2022-02-02 12:41:00');
INSERT INTO `admin_operation_log` VALUES (905, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:41:03', '2022-02-02 12:41:03');
INSERT INTO `admin_operation_log` VALUES (906, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:41:04', '2022-02-02 12:41:04');
INSERT INTO `admin_operation_log` VALUES (907, 1, 'admin/disconut', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:41:05', '2022-02-02 12:41:05');
INSERT INTO `admin_operation_log` VALUES (908, 1, 'admin/disconut', 'GET', '127.0.0.1', '[]', '2022-02-02 12:42:26', '2022-02-02 12:42:26');
INSERT INTO `admin_operation_log` VALUES (909, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:42:28', '2022-02-02 12:42:28');
INSERT INTO `admin_operation_log` VALUES (910, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:42:28', '2022-02-02 12:42:28');
INSERT INTO `admin_operation_log` VALUES (911, 1, 'admin/ad/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:42:30', '2022-02-02 12:42:30');
INSERT INTO `admin_operation_log` VALUES (912, 1, 'admin/ad/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:42:41', '2022-02-02 12:42:41');
INSERT INTO `admin_operation_log` VALUES (913, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:42:43', '2022-02-02 12:42:43');
INSERT INTO `admin_operation_log` VALUES (914, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:42:45', '2022-02-02 12:42:45');
INSERT INTO `admin_operation_log` VALUES (915, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 12:42:46', '2022-02-02 12:42:46');
INSERT INTO `admin_operation_log` VALUES (916, 1, 'admin/rule/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:42:47', '2022-02-02 12:42:47');
INSERT INTO `admin_operation_log` VALUES (917, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:42:49', '2022-02-02 12:42:49');
INSERT INTO `admin_operation_log` VALUES (918, 1, 'admin/rule/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:42:51', '2022-02-02 12:42:51');
INSERT INTO `admin_operation_log` VALUES (919, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:42:52', '2022-02-02 12:42:52');
INSERT INTO `admin_operation_log` VALUES (920, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:42:53', '2022-02-02 12:42:53');
INSERT INTO `admin_operation_log` VALUES (921, 1, 'admin/ad/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:42:55', '2022-02-02 12:42:55');
INSERT INTO `admin_operation_log` VALUES (922, 1, 'admin/ad/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:43:26', '2022-02-02 12:43:26');
INSERT INTO `admin_operation_log` VALUES (923, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:43:30', '2022-02-02 12:43:30');
INSERT INTO `admin_operation_log` VALUES (924, 1, 'admin/pay/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:43:32', '2022-02-02 12:43:32');
INSERT INTO `admin_operation_log` VALUES (925, 1, 'admin/pay', 'POST', '127.0.0.1', '{\"platform_id\":\"2\",\"enable\":\"1\",\"content\":null,\"_token\":\"mFvnRqC6ody7XNNmbk3QVUdfRqbc01rmcvUxYe13\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/pay\"}', '2022-02-02 12:43:37', '2022-02-02 12:43:37');
INSERT INTO `admin_operation_log` VALUES (926, 1, 'admin/pay/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:43:37', '2022-02-02 12:43:37');
INSERT INTO `admin_operation_log` VALUES (927, 1, 'admin/pay/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:43:41', '2022-02-02 12:43:41');
INSERT INTO `admin_operation_log` VALUES (928, 1, 'admin/pay/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:44:04', '2022-02-02 12:44:04');
INSERT INTO `admin_operation_log` VALUES (929, 1, 'admin/pay', 'POST', '127.0.0.1', '{\"platform_id\":\"2\",\"enable\":\"1\",\"content\":null,\"_token\":\"mFvnRqC6ody7XNNmbk3QVUdfRqbc01rmcvUxYe13\"}', '2022-02-02 12:44:07', '2022-02-02 12:44:07');
INSERT INTO `admin_operation_log` VALUES (930, 1, 'admin/pay/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:44:07', '2022-02-02 12:44:07');
INSERT INTO `admin_operation_log` VALUES (931, 1, 'admin/pay/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:44:13', '2022-02-02 12:44:13');
INSERT INTO `admin_operation_log` VALUES (932, 1, 'admin/pay', 'POST', '127.0.0.1', '{\"platform_id\":\"2\",\"enable\":\"1\",\"content\":null,\"_token\":\"mFvnRqC6ody7XNNmbk3QVUdfRqbc01rmcvUxYe13\"}', '2022-02-02 12:44:54', '2022-02-02 12:44:54');
INSERT INTO `admin_operation_log` VALUES (933, 1, 'admin/pay', 'GET', '127.0.0.1', '[]', '2022-02-02 12:44:54', '2022-02-02 12:44:54');
INSERT INTO `admin_operation_log` VALUES (934, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:45:00', '2022-02-02 12:45:00');
INSERT INTO `admin_operation_log` VALUES (935, 1, 'admin/rule/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:45:05', '2022-02-02 12:45:05');
INSERT INTO `admin_operation_log` VALUES (936, 1, 'admin/rule/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:45:10', '2022-02-02 12:45:10');
INSERT INTO `admin_operation_log` VALUES (937, 1, 'admin/rule/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:45:11', '2022-02-02 12:45:11');
INSERT INTO `admin_operation_log` VALUES (938, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:45:12', '2022-02-02 12:45:12');
INSERT INTO `admin_operation_log` VALUES (939, 1, 'admin/rule/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:45:16', '2022-02-02 12:45:16');
INSERT INTO `admin_operation_log` VALUES (940, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:45:18', '2022-02-02 12:45:18');
INSERT INTO `admin_operation_log` VALUES (941, 1, 'admin/rule/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:45:20', '2022-02-02 12:45:20');
INSERT INTO `admin_operation_log` VALUES (942, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:45:23', '2022-02-02 12:45:23');
INSERT INTO `admin_operation_log` VALUES (943, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 12:45:25', '2022-02-02 12:45:25');
INSERT INTO `admin_operation_log` VALUES (944, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-02 12:45:26', '2022-02-02 12:45:26');
INSERT INTO `admin_operation_log` VALUES (945, 1, 'admin/rule/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:45:30', '2022-02-02 12:45:30');
INSERT INTO `admin_operation_log` VALUES (946, 1, 'admin/rule/1/edit', 'GET', '127.0.0.1', '[]', '2022-02-02 12:45:33', '2022-02-02 12:45:33');
INSERT INTO `admin_operation_log` VALUES (947, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:46:00', '2022-02-02 12:46:00');
INSERT INTO `admin_operation_log` VALUES (948, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:46:01', '2022-02-02 12:46:01');
INSERT INTO `admin_operation_log` VALUES (949, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:46:02', '2022-02-02 12:46:02');
INSERT INTO `admin_operation_log` VALUES (950, 1, 'admin/news/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:46:03', '2022-02-02 12:46:03');
INSERT INTO `admin_operation_log` VALUES (951, 1, 'admin/news/create', 'GET', '127.0.0.1', '[]', '2022-02-02 12:47:12', '2022-02-02 12:47:12');
INSERT INTO `admin_operation_log` VALUES (952, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:47:14', '2022-02-02 12:47:14');
INSERT INTO `admin_operation_log` VALUES (953, 1, 'admin/platform/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:47:16', '2022-02-02 12:47:16');
INSERT INTO `admin_operation_log` VALUES (954, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:47:19', '2022-02-02 12:47:19');
INSERT INTO `admin_operation_log` VALUES (955, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:47:21', '2022-02-02 12:47:21');
INSERT INTO `admin_operation_log` VALUES (956, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:47:22', '2022-02-02 12:47:22');
INSERT INTO `admin_operation_log` VALUES (957, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:47:37', '2022-02-02 12:47:37');
INSERT INTO `admin_operation_log` VALUES (958, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:47:38', '2022-02-02 12:47:38');
INSERT INTO `admin_operation_log` VALUES (959, 1, 'admin/disconut', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:47:39', '2022-02-02 12:47:39');
INSERT INTO `admin_operation_log` VALUES (960, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:47:39', '2022-02-02 12:47:39');
INSERT INTO `admin_operation_log` VALUES (961, 1, 'admin/disconut', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:47:40', '2022-02-02 12:47:40');
INSERT INTO `admin_operation_log` VALUES (962, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:47:49', '2022-02-02 12:47:49');
INSERT INTO `admin_operation_log` VALUES (963, 1, 'admin/platform/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:47:51', '2022-02-02 12:47:51');
INSERT INTO `admin_operation_log` VALUES (964, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 12:48:09', '2022-02-02 12:48:09');
INSERT INTO `admin_operation_log` VALUES (965, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-02 20:47:49', '2022-02-02 20:47:49');
INSERT INTO `admin_operation_log` VALUES (966, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 20:47:51', '2022-02-02 20:47:51');
INSERT INTO `admin_operation_log` VALUES (967, 1, 'admin/order/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 20:47:58', '2022-02-02 20:47:58');
INSERT INTO `admin_operation_log` VALUES (968, 1, 'admin/order', 'POST', '127.0.0.1', '{\"platform_id\":\"1\",\"name\":\"Sam\",\"phone\":\"0973011400\",\"car_no\":\"RZD298\",\"addr\":\"\\u53f0\\u5317\\u5e02\\u5fe0\\u5b5d\\u6771\\u8def\",\"status\":\"1\",\"_token\":\"wsyQGBLc9eMc4y6sL3UQSGtqH0rKvmjU6uQTi04t\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/order\"}', '2022-02-02 20:49:09', '2022-02-02 20:49:09');
INSERT INTO `admin_operation_log` VALUES (969, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 20:49:10', '2022-02-02 20:49:10');
INSERT INTO `admin_operation_log` VALUES (970, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 20:49:33', '2022-02-02 20:49:33');
INSERT INTO `admin_operation_log` VALUES (971, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 20:49:52', '2022-02-02 20:49:52');
INSERT INTO `admin_operation_log` VALUES (972, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 20:51:11', '2022-02-02 20:51:11');
INSERT INTO `admin_operation_log` VALUES (973, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 20:56:48', '2022-02-02 20:56:48');
INSERT INTO `admin_operation_log` VALUES (974, 1, 'admin/auth/menu/20/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 20:56:55', '2022-02-02 20:56:55');
INSERT INTO `admin_operation_log` VALUES (975, 1, 'admin/auth/menu/20', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u5ee3\\u544a\\u8f2a\\u64ad\\u7ba1\\u7406\",\"icon\":\"fa-file-photo-o\",\"uri\":\"\\/ad\",\"roles\":[\"1\",\"2\",\"3\",null],\"permission\":\"*\",\"_token\":\"wsyQGBLc9eMc4y6sL3UQSGtqH0rKvmjU6uQTi04t\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/auth\\/menu\"}', '2022-02-02 20:57:01', '2022-02-02 20:57:01');
INSERT INTO `admin_operation_log` VALUES (976, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-02 20:57:02', '2022-02-02 20:57:02');
INSERT INTO `admin_operation_log` VALUES (977, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 20:57:04', '2022-02-02 20:57:04');
INSERT INTO `admin_operation_log` VALUES (978, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-02 20:57:04', '2022-02-02 20:57:04');
INSERT INTO `admin_operation_log` VALUES (979, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 20:57:09', '2022-02-02 20:57:09');
INSERT INTO `admin_operation_log` VALUES (980, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-02 20:57:13', '2022-02-02 20:57:13');
INSERT INTO `admin_operation_log` VALUES (981, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-02 20:57:56', '2022-02-02 20:57:56');
INSERT INTO `admin_operation_log` VALUES (982, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 20:58:29', '2022-02-02 20:58:29');
INSERT INTO `admin_operation_log` VALUES (983, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 20:59:04', '2022-02-02 20:59:04');
INSERT INTO `admin_operation_log` VALUES (984, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 20:59:07', '2022-02-02 20:59:07');
INSERT INTO `admin_operation_log` VALUES (985, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 20:59:09', '2022-02-02 20:59:09');
INSERT INTO `admin_operation_log` VALUES (986, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 20:59:27', '2022-02-02 20:59:27');
INSERT INTO `admin_operation_log` VALUES (987, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:03:56', '2022-02-02 21:03:56');
INSERT INTO `admin_operation_log` VALUES (988, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:13:26', '2022-02-02 21:13:26');
INSERT INTO `admin_operation_log` VALUES (989, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:14:18', '2022-02-02 21:14:18');
INSERT INTO `admin_operation_log` VALUES (990, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:14:42', '2022-02-02 21:14:42');
INSERT INTO `admin_operation_log` VALUES (991, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:14:43', '2022-02-02 21:14:43');
INSERT INTO `admin_operation_log` VALUES (992, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:15:16', '2022-02-02 21:15:16');
INSERT INTO `admin_operation_log` VALUES (993, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:15:17', '2022-02-02 21:15:17');
INSERT INTO `admin_operation_log` VALUES (994, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:15:17', '2022-02-02 21:15:17');
INSERT INTO `admin_operation_log` VALUES (995, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:15:18', '2022-02-02 21:15:18');
INSERT INTO `admin_operation_log` VALUES (996, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:15:46', '2022-02-02 21:15:46');
INSERT INTO `admin_operation_log` VALUES (997, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:16:05', '2022-02-02 21:16:05');
INSERT INTO `admin_operation_log` VALUES (998, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:16:05', '2022-02-02 21:16:05');
INSERT INTO `admin_operation_log` VALUES (999, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:16:14', '2022-02-02 21:16:14');
INSERT INTO `admin_operation_log` VALUES (1000, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:16:19', '2022-02-02 21:16:19');
INSERT INTO `admin_operation_log` VALUES (1001, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:16:20', '2022-02-02 21:16:20');
INSERT INTO `admin_operation_log` VALUES (1002, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:16:34', '2022-02-02 21:16:34');
INSERT INTO `admin_operation_log` VALUES (1003, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:17:03', '2022-02-02 21:17:03');
INSERT INTO `admin_operation_log` VALUES (1004, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:17:09', '2022-02-02 21:17:09');
INSERT INTO `admin_operation_log` VALUES (1005, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:17:25', '2022-02-02 21:17:25');
INSERT INTO `admin_operation_log` VALUES (1006, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:18:24', '2022-02-02 21:18:24');
INSERT INTO `admin_operation_log` VALUES (1007, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:18:26', '2022-02-02 21:18:26');
INSERT INTO `admin_operation_log` VALUES (1008, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:18:32', '2022-02-02 21:18:32');
INSERT INTO `admin_operation_log` VALUES (1009, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:19:15', '2022-02-02 21:19:15');
INSERT INTO `admin_operation_log` VALUES (1010, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:20:26', '2022-02-02 21:20:26');
INSERT INTO `admin_operation_log` VALUES (1011, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:21:05', '2022-02-02 21:21:05');
INSERT INTO `admin_operation_log` VALUES (1012, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:24:42', '2022-02-02 21:24:42');
INSERT INTO `admin_operation_log` VALUES (1013, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:24:43', '2022-02-02 21:24:43');
INSERT INTO `admin_operation_log` VALUES (1014, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:24:51', '2022-02-02 21:24:51');
INSERT INTO `admin_operation_log` VALUES (1015, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:25:25', '2022-02-02 21:25:25');
INSERT INTO `admin_operation_log` VALUES (1016, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:25:26', '2022-02-02 21:25:26');
INSERT INTO `admin_operation_log` VALUES (1017, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:25:30', '2022-02-02 21:25:30');
INSERT INTO `admin_operation_log` VALUES (1018, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:25:46', '2022-02-02 21:25:46');
INSERT INTO `admin_operation_log` VALUES (1019, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:26:50', '2022-02-02 21:26:50');
INSERT INTO `admin_operation_log` VALUES (1020, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:26:51', '2022-02-02 21:26:51');
INSERT INTO `admin_operation_log` VALUES (1021, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:26:53', '2022-02-02 21:26:53');
INSERT INTO `admin_operation_log` VALUES (1022, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:26:54', '2022-02-02 21:26:54');
INSERT INTO `admin_operation_log` VALUES (1023, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:26:55', '2022-02-02 21:26:55');
INSERT INTO `admin_operation_log` VALUES (1024, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:26:56', '2022-02-02 21:26:56');
INSERT INTO `admin_operation_log` VALUES (1025, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:26:57', '2022-02-02 21:26:57');
INSERT INTO `admin_operation_log` VALUES (1026, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:26:59', '2022-02-02 21:26:59');
INSERT INTO `admin_operation_log` VALUES (1027, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:27:00', '2022-02-02 21:27:00');
INSERT INTO `admin_operation_log` VALUES (1028, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:27:01', '2022-02-02 21:27:01');
INSERT INTO `admin_operation_log` VALUES (1029, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:27:04', '2022-02-02 21:27:04');
INSERT INTO `admin_operation_log` VALUES (1030, 1, 'admin/disconut', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:27:10', '2022-02-02 21:27:10');
INSERT INTO `admin_operation_log` VALUES (1031, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:27:13', '2022-02-02 21:27:13');
INSERT INTO `admin_operation_log` VALUES (1032, 1, 'admin/disconut', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:27:17', '2022-02-02 21:27:17');
INSERT INTO `admin_operation_log` VALUES (1033, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:27:19', '2022-02-02 21:27:19');
INSERT INTO `admin_operation_log` VALUES (1034, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:28:13', '2022-02-02 21:28:13');
INSERT INTO `admin_operation_log` VALUES (1035, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:28:23', '2022-02-02 21:28:23');
INSERT INTO `admin_operation_log` VALUES (1036, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_export_\":\"all\"}', '2022-02-02 21:28:51', '2022-02-02 21:28:51');
INSERT INTO `admin_operation_log` VALUES (1037, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_export_\":\"all\"}', '2022-02-02 21:32:46', '2022-02-02 21:32:46');
INSERT INTO `admin_operation_log` VALUES (1038, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_export_\":\"all\"}', '2022-02-02 21:32:47', '2022-02-02 21:32:47');
INSERT INTO `admin_operation_log` VALUES (1039, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_export_\":\"all\"}', '2022-02-02 21:32:52', '2022-02-02 21:32:52');
INSERT INTO `admin_operation_log` VALUES (1040, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_export_\":\"all\"}', '2022-02-02 21:32:54', '2022-02-02 21:32:54');
INSERT INTO `admin_operation_log` VALUES (1041, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_export_\":\"all\"}', '2022-02-02 21:32:58', '2022-02-02 21:32:58');
INSERT INTO `admin_operation_log` VALUES (1042, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_export_\":\"all\"}', '2022-02-02 21:32:59', '2022-02-02 21:32:59');
INSERT INTO `admin_operation_log` VALUES (1043, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_export_\":\"all\"}', '2022-02-02 21:33:04', '2022-02-02 21:33:04');
INSERT INTO `admin_operation_log` VALUES (1044, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_export_\":\"all\"}', '2022-02-02 21:33:10', '2022-02-02 21:33:10');
INSERT INTO `admin_operation_log` VALUES (1045, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_export_\":\"all\"}', '2022-02-02 21:33:52', '2022-02-02 21:33:52');
INSERT INTO `admin_operation_log` VALUES (1046, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_export_\":\"all\"}', '2022-02-02 21:35:00', '2022-02-02 21:35:00');
INSERT INTO `admin_operation_log` VALUES (1047, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_export_\":\"all\"}', '2022-02-02 21:35:18', '2022-02-02 21:35:18');
INSERT INTO `admin_operation_log` VALUES (1048, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:36:16', '2022-02-02 21:36:16');
INSERT INTO `admin_operation_log` VALUES (1049, 1, 'admin/order/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:36:19', '2022-02-02 21:36:19');
INSERT INTO `admin_operation_log` VALUES (1050, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:36:22', '2022-02-02 21:36:22');
INSERT INTO `admin_operation_log` VALUES (1051, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:36:45', '2022-02-02 21:36:45');
INSERT INTO `admin_operation_log` VALUES (1052, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:36:48', '2022-02-02 21:36:48');
INSERT INTO `admin_operation_log` VALUES (1053, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:36:48', '2022-02-02 21:36:48');
INSERT INTO `admin_operation_log` VALUES (1054, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:36:49', '2022-02-02 21:36:49');
INSERT INTO `admin_operation_log` VALUES (1055, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:36:49', '2022-02-02 21:36:49');
INSERT INTO `admin_operation_log` VALUES (1056, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:36:50', '2022-02-02 21:36:50');
INSERT INTO `admin_operation_log` VALUES (1057, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:36:50', '2022-02-02 21:36:50');
INSERT INTO `admin_operation_log` VALUES (1058, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:37:24', '2022-02-02 21:37:24');
INSERT INTO `admin_operation_log` VALUES (1059, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:37:26', '2022-02-02 21:37:26');
INSERT INTO `admin_operation_log` VALUES (1060, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:37:27', '2022-02-02 21:37:27');
INSERT INTO `admin_operation_log` VALUES (1061, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:37:28', '2022-02-02 21:37:28');
INSERT INTO `admin_operation_log` VALUES (1062, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:37:28', '2022-02-02 21:37:28');
INSERT INTO `admin_operation_log` VALUES (1063, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:37:29', '2022-02-02 21:37:29');
INSERT INTO `admin_operation_log` VALUES (1064, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:37:30', '2022-02-02 21:37:30');
INSERT INTO `admin_operation_log` VALUES (1065, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:37:31', '2022-02-02 21:37:31');
INSERT INTO `admin_operation_log` VALUES (1066, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:37:32', '2022-02-02 21:37:32');
INSERT INTO `admin_operation_log` VALUES (1067, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:37:32', '2022-02-02 21:37:32');
INSERT INTO `admin_operation_log` VALUES (1068, 1, 'admin/disconut', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:37:33', '2022-02-02 21:37:33');
INSERT INTO `admin_operation_log` VALUES (1069, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:37:34', '2022-02-02 21:37:34');
INSERT INTO `admin_operation_log` VALUES (1070, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:37:35', '2022-02-02 21:37:35');
INSERT INTO `admin_operation_log` VALUES (1071, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:37:36', '2022-02-02 21:37:36');
INSERT INTO `admin_operation_log` VALUES (1072, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:37:42', '2022-02-02 21:37:42');
INSERT INTO `admin_operation_log` VALUES (1073, 2, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 21:37:56', '2022-02-02 21:37:56');
INSERT INTO `admin_operation_log` VALUES (1074, 2, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:38:07', '2022-02-02 21:38:07');
INSERT INTO `admin_operation_log` VALUES (1075, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:38:21', '2022-02-02 21:38:21');
INSERT INTO `admin_operation_log` VALUES (1076, 2, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-02 21:38:37', '2022-02-02 21:38:37');
INSERT INTO `admin_operation_log` VALUES (1077, 2, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-02 21:38:37', '2022-02-02 21:38:37');
INSERT INTO `admin_operation_log` VALUES (1078, 2, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:38:40', '2022-02-02 21:38:40');
INSERT INTO `admin_operation_log` VALUES (1079, 2, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:38:41', '2022-02-02 21:38:41');
INSERT INTO `admin_operation_log` VALUES (1080, 2, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:38:42', '2022-02-02 21:38:42');
INSERT INTO `admin_operation_log` VALUES (1081, 2, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:38:43', '2022-02-02 21:38:43');
INSERT INTO `admin_operation_log` VALUES (1082, 2, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:38:45', '2022-02-02 21:38:45');
INSERT INTO `admin_operation_log` VALUES (1083, 2, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:38:46', '2022-02-02 21:38:46');
INSERT INTO `admin_operation_log` VALUES (1084, 2, 'admin/disconut', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:38:47', '2022-02-02 21:38:47');
INSERT INTO `admin_operation_log` VALUES (1085, 2, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:38:47', '2022-02-02 21:38:47');
INSERT INTO `admin_operation_log` VALUES (1086, 2, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:38:48', '2022-02-02 21:38:48');
INSERT INTO `admin_operation_log` VALUES (1087, 2, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:38:49', '2022-02-02 21:38:49');
INSERT INTO `admin_operation_log` VALUES (1088, 2, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:38:50', '2022-02-02 21:38:50');
INSERT INTO `admin_operation_log` VALUES (1089, 2, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:38:51', '2022-02-02 21:38:51');
INSERT INTO `admin_operation_log` VALUES (1090, 2, 'admin/auth/logout', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:39:05', '2022-02-02 21:39:05');
INSERT INTO `admin_operation_log` VALUES (1091, 2, 'admin/auth/logout', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:39:16', '2022-02-02 21:39:16');
INSERT INTO `admin_operation_log` VALUES (1092, 2, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-02 21:39:30', '2022-02-02 21:39:30');
INSERT INTO `admin_operation_log` VALUES (1093, 2, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-02 21:39:31', '2022-02-02 21:39:31');
INSERT INTO `admin_operation_log` VALUES (1094, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-02 21:39:45', '2022-02-02 21:39:45');
INSERT INTO `admin_operation_log` VALUES (1095, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-02 21:39:45', '2022-02-02 21:39:45');
INSERT INTO `admin_operation_log` VALUES (1096, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:39:58', '2022-02-02 21:39:58');
INSERT INTO `admin_operation_log` VALUES (1097, 1, 'admin/auth/menu/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:40:02', '2022-02-02 21:40:02');
INSERT INTO `admin_operation_log` VALUES (1098, 1, 'admin/auth/menu/2', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"Admin\",\"icon\":\"fa-tasks\",\"uri\":null,\"roles\":[\"1\",null],\"permission\":null,\"_token\":\"g68UhrkIPsHsvRE7GdH0jWWKaqpvs91xOvSPWOs0\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/auth\\/menu\"}', '2022-02-02 21:40:14', '2022-02-02 21:40:14');
INSERT INTO `admin_operation_log` VALUES (1099, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-02 21:40:14', '2022-02-02 21:40:14');
INSERT INTO `admin_operation_log` VALUES (1100, 2, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:40:19', '2022-02-02 21:40:19');
INSERT INTO `admin_operation_log` VALUES (1101, 2, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:40:20', '2022-02-02 21:40:20');
INSERT INTO `admin_operation_log` VALUES (1102, 2, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-02 21:40:21', '2022-02-02 21:40:21');
INSERT INTO `admin_operation_log` VALUES (1103, 2, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-02 21:40:22', '2022-02-02 21:40:22');
INSERT INTO `admin_operation_log` VALUES (1104, 2, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:40:32', '2022-02-02 21:40:32');
INSERT INTO `admin_operation_log` VALUES (1105, 2, 'admin/disconut', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:40:33', '2022-02-02 21:40:33');
INSERT INTO `admin_operation_log` VALUES (1106, 2, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:40:33', '2022-02-02 21:40:33');
INSERT INTO `admin_operation_log` VALUES (1107, 2, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:40:34', '2022-02-02 21:40:34');
INSERT INTO `admin_operation_log` VALUES (1108, 2, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:40:35', '2022-02-02 21:40:35');
INSERT INTO `admin_operation_log` VALUES (1109, 2, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:40:36', '2022-02-02 21:40:36');
INSERT INTO `admin_operation_log` VALUES (1110, 2, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:40:37', '2022-02-02 21:40:37');
INSERT INTO `admin_operation_log` VALUES (1111, 2, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:40:37', '2022-02-02 21:40:37');
INSERT INTO `admin_operation_log` VALUES (1112, 2, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:40:38', '2022-02-02 21:40:38');
INSERT INTO `admin_operation_log` VALUES (1113, 2, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:40:39', '2022-02-02 21:40:39');
INSERT INTO `admin_operation_log` VALUES (1114, 2, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:40:39', '2022-02-02 21:40:39');
INSERT INTO `admin_operation_log` VALUES (1115, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"_token\":\"g68UhrkIPsHsvRE7GdH0jWWKaqpvs91xOvSPWOs0\",\"_order\":\"[{\\\"id\\\":15},{\\\"id\\\":14},{\\\"id\\\":17},{\\\"id\\\":18},{\\\"id\\\":19},{\\\"id\\\":20},{\\\"id\\\":16},{\\\"id\\\":3},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7}]},{\\\"id\\\":9,\\\"children\\\":[{\\\"id\\\":10},{\\\"id\\\":11},{\\\"id\\\":12},{\\\"id\\\":13}]}]\"}', '2022-02-02 21:41:00', '2022-02-02 21:41:00');
INSERT INTO `admin_operation_log` VALUES (1116, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:41:00', '2022-02-02 21:41:00');
INSERT INTO `admin_operation_log` VALUES (1117, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-02 21:41:15', '2022-02-02 21:41:15');
INSERT INTO `admin_operation_log` VALUES (1118, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:41:17', '2022-02-02 21:41:17');
INSERT INTO `admin_operation_log` VALUES (1119, 2, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:41:25', '2022-02-02 21:41:25');
INSERT INTO `admin_operation_log` VALUES (1120, 2, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:44:11', '2022-02-02 21:44:11');
INSERT INTO `admin_operation_log` VALUES (1121, 2, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:44:17', '2022-02-02 21:44:17');
INSERT INTO `admin_operation_log` VALUES (1122, 2, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:44:46', '2022-02-02 21:44:46');
INSERT INTO `admin_operation_log` VALUES (1123, 2, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:44:51', '2022-02-02 21:44:51');
INSERT INTO `admin_operation_log` VALUES (1124, 2, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:45:02', '2022-02-02 21:45:02');
INSERT INTO `admin_operation_log` VALUES (1125, 2, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 21:48:05', '2022-02-02 21:48:05');
INSERT INTO `admin_operation_log` VALUES (1126, 2, 'admin/auth/logout', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:48:18', '2022-02-02 21:48:18');
INSERT INTO `admin_operation_log` VALUES (1127, 2, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-02 21:52:03', '2022-02-02 21:52:03');
INSERT INTO `admin_operation_log` VALUES (1128, 2, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:52:03', '2022-02-02 21:52:03');
INSERT INTO `admin_operation_log` VALUES (1129, 2, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:52:32', '2022-02-02 21:52:32');
INSERT INTO `admin_operation_log` VALUES (1130, 2, 'admin/auth/logout', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:52:57', '2022-02-02 21:52:57');
INSERT INTO `admin_operation_log` VALUES (1131, 2, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-02 21:53:02', '2022-02-02 21:53:02');
INSERT INTO `admin_operation_log` VALUES (1132, 2, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 21:53:02', '2022-02-02 21:53:02');
INSERT INTO `admin_operation_log` VALUES (1133, 2, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:55:55', '2022-02-02 21:55:55');
INSERT INTO `admin_operation_log` VALUES (1134, 2, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 21:55:55', '2022-02-02 21:55:55');
INSERT INTO `admin_operation_log` VALUES (1135, 2, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 21:56:02', '2022-02-02 21:56:02');
INSERT INTO `admin_operation_log` VALUES (1136, 2, 'admin/auth/users/create', 'GET', '127.0.0.1', '[]', '2022-02-02 21:56:33', '2022-02-02 21:56:33');
INSERT INTO `admin_operation_log` VALUES (1137, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:57:24', '2022-02-02 21:57:24');
INSERT INTO `admin_operation_log` VALUES (1138, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-02 21:57:25', '2022-02-02 21:57:25');
INSERT INTO `admin_operation_log` VALUES (1139, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2022-02-02 21:57:26', '2022-02-02 21:57:26');
INSERT INTO `admin_operation_log` VALUES (1140, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:57:27', '2022-02-02 21:57:27');
INSERT INTO `admin_operation_log` VALUES (1141, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 21:57:28', '2022-02-02 21:57:28');
INSERT INTO `admin_operation_log` VALUES (1142, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 21:57:29', '2022-02-02 21:57:29');
INSERT INTO `admin_operation_log` VALUES (1143, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 21:57:30', '2022-02-02 21:57:30');
INSERT INTO `admin_operation_log` VALUES (1144, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 21:57:30', '2022-02-02 21:57:30');
INSERT INTO `admin_operation_log` VALUES (1145, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 21:57:30', '2022-02-02 21:57:30');
INSERT INTO `admin_operation_log` VALUES (1146, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 21:57:31', '2022-02-02 21:57:31');
INSERT INTO `admin_operation_log` VALUES (1147, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 21:58:02', '2022-02-02 21:58:02');
INSERT INTO `admin_operation_log` VALUES (1148, 1, 'admin/auth/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:58:06', '2022-02-02 21:58:06');
INSERT INTO `admin_operation_log` VALUES (1149, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:59:09', '2022-02-02 21:59:09');
INSERT INTO `admin_operation_log` VALUES (1150, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '[]', '2022-02-02 21:59:55', '2022-02-02 21:59:55');
INSERT INTO `admin_operation_log` VALUES (1151, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 21:59:59', '2022-02-02 21:59:59');
INSERT INTO `admin_operation_log` VALUES (1152, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 22:00:00', '2022-02-02 22:00:00');
INSERT INTO `admin_operation_log` VALUES (1153, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 22:00:01', '2022-02-02 22:00:01');
INSERT INTO `admin_operation_log` VALUES (1154, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 22:00:08', '2022-02-02 22:00:08');
INSERT INTO `admin_operation_log` VALUES (1155, 2, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 22:00:10', '2022-02-02 22:00:10');
INSERT INTO `admin_operation_log` VALUES (1156, 2, 'admin/auth/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 22:00:12', '2022-02-02 22:00:12');
INSERT INTO `admin_operation_log` VALUES (1157, 2, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 22:00:40', '2022-02-02 22:00:40');
INSERT INTO `admin_operation_log` VALUES (1158, 2, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 22:00:42', '2022-02-02 22:00:42');
INSERT INTO `admin_operation_log` VALUES (1159, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 22:01:41', '2022-02-02 22:01:41');
INSERT INTO `admin_operation_log` VALUES (1160, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 22:01:42', '2022-02-02 22:01:42');
INSERT INTO `admin_operation_log` VALUES (1161, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 22:01:42', '2022-02-02 22:01:42');
INSERT INTO `admin_operation_log` VALUES (1162, 1, 'admin/auth/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 22:01:43', '2022-02-02 22:01:43');
INSERT INTO `admin_operation_log` VALUES (1163, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 22:01:43', '2022-02-02 22:01:43');
INSERT INTO `admin_operation_log` VALUES (1164, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 22:01:52', '2022-02-02 22:01:52');
INSERT INTO `admin_operation_log` VALUES (1165, 1, 'admin/auth/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 22:01:54', '2022-02-02 22:01:54');
INSERT INTO `admin_operation_log` VALUES (1166, 2, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-02 22:02:01', '2022-02-02 22:02:01');
INSERT INTO `admin_operation_log` VALUES (1167, 2, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 22:02:02', '2022-02-02 22:02:02');
INSERT INTO `admin_operation_log` VALUES (1168, 2, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 22:02:04', '2022-02-02 22:02:04');
INSERT INTO `admin_operation_log` VALUES (1169, 2, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 22:02:05', '2022-02-02 22:02:05');
INSERT INTO `admin_operation_log` VALUES (1170, 2, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 22:02:05', '2022-02-02 22:02:05');
INSERT INTO `admin_operation_log` VALUES (1171, 2, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 22:02:29', '2022-02-02 22:02:29');
INSERT INTO `admin_operation_log` VALUES (1172, 2, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 22:02:30', '2022-02-02 22:02:30');
INSERT INTO `admin_operation_log` VALUES (1173, 2, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 22:02:48', '2022-02-02 22:02:48');
INSERT INTO `admin_operation_log` VALUES (1174, 2, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 22:02:49', '2022-02-02 22:02:49');
INSERT INTO `admin_operation_log` VALUES (1175, 2, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 22:02:49', '2022-02-02 22:02:49');
INSERT INTO `admin_operation_log` VALUES (1176, 2, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_export_\":\"all\"}', '2022-02-02 22:03:34', '2022-02-02 22:03:34');
INSERT INTO `admin_operation_log` VALUES (1177, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 22:05:17', '2022-02-02 22:05:17');
INSERT INTO `admin_operation_log` VALUES (1178, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 22:05:50', '2022-02-02 22:05:50');
INSERT INTO `admin_operation_log` VALUES (1179, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2022-02-02 22:06:24', '2022-02-02 22:06:24');
INSERT INTO `admin_operation_log` VALUES (1180, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-02 22:06:52', '2022-02-02 22:06:52');
INSERT INTO `admin_operation_log` VALUES (1181, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-20 15:31:48', '2022-02-20 15:31:48');
INSERT INTO `admin_operation_log` VALUES (1182, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-20 15:31:49', '2022-02-20 15:31:49');
INSERT INTO `admin_operation_log` VALUES (1183, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 15:31:51', '2022-02-20 15:31:51');
INSERT INTO `admin_operation_log` VALUES (1184, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 15:31:52', '2022-02-20 15:31:52');
INSERT INTO `admin_operation_log` VALUES (1185, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 15:31:53', '2022-02-20 15:31:53');
INSERT INTO `admin_operation_log` VALUES (1186, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 15:31:54', '2022-02-20 15:31:54');
INSERT INTO `admin_operation_log` VALUES (1187, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 15:31:55', '2022-02-20 15:31:55');
INSERT INTO `admin_operation_log` VALUES (1188, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 15:31:56', '2022-02-20 15:31:56');
INSERT INTO `admin_operation_log` VALUES (1189, 1, 'admin/disconut', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 15:31:56', '2022-02-20 15:31:56');
INSERT INTO `admin_operation_log` VALUES (1190, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 15:31:57', '2022-02-20 15:31:57');
INSERT INTO `admin_operation_log` VALUES (1191, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 15:32:05', '2022-02-20 15:32:05');
INSERT INTO `admin_operation_log` VALUES (1192, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 15:32:12', '2022-02-20 15:32:12');
INSERT INTO `admin_operation_log` VALUES (1193, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 15:32:13', '2022-02-20 15:32:13');
INSERT INTO `admin_operation_log` VALUES (1194, 1, 'admin/auth/menu/3/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 15:32:26', '2022-02-20 15:32:26');
INSERT INTO `admin_operation_log` VALUES (1195, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 15:32:28', '2022-02-20 15:32:28');
INSERT INTO `admin_operation_log` VALUES (1196, 1, 'admin/auth/menu/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 15:32:31', '2022-02-20 15:32:31');
INSERT INTO `admin_operation_log` VALUES (1197, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 15:32:33', '2022-02-20 15:32:33');
INSERT INTO `admin_operation_log` VALUES (1198, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 15:32:37', '2022-02-20 15:32:37');
INSERT INTO `admin_operation_log` VALUES (1199, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 15:32:39', '2022-02-20 15:32:39');
INSERT INTO `admin_operation_log` VALUES (1200, 1, 'admin/platform/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 15:32:43', '2022-02-20 15:32:43');
INSERT INTO `admin_operation_log` VALUES (1201, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 15:32:47', '2022-02-20 15:32:47');
INSERT INTO `admin_operation_log` VALUES (1202, 1, 'admin/platform/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 15:32:50', '2022-02-20 15:32:50');
INSERT INTO `admin_operation_log` VALUES (1203, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 15:33:01', '2022-02-20 15:33:01');
INSERT INTO `admin_operation_log` VALUES (1204, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-20 15:37:09', '2022-02-20 15:37:09');
INSERT INTO `admin_operation_log` VALUES (1205, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-20 15:37:32', '2022-02-20 15:37:32');
INSERT INTO `admin_operation_log` VALUES (1206, 1, 'admin/platform', 'GET', '127.0.0.1', '[]', '2022-02-20 15:37:34', '2022-02-20 15:37:34');
INSERT INTO `admin_operation_log` VALUES (1207, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-20 16:49:30', '2022-02-20 16:49:30');
INSERT INTO `admin_operation_log` VALUES (1208, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-20 16:49:30', '2022-02-20 16:49:30');
INSERT INTO `admin_operation_log` VALUES (1209, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 16:49:32', '2022-02-20 16:49:32');
INSERT INTO `admin_operation_log` VALUES (1210, 1, 'admin/platform/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 16:49:35', '2022-02-20 16:49:35');
INSERT INTO `admin_operation_log` VALUES (1211, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 16:49:37', '2022-02-20 16:49:37');
INSERT INTO `admin_operation_log` VALUES (1212, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 16:49:44', '2022-02-20 16:49:44');
INSERT INTO `admin_operation_log` VALUES (1213, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 16:49:46', '2022-02-20 16:49:46');
INSERT INTO `admin_operation_log` VALUES (1214, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 16:49:53', '2022-02-20 16:49:53');
INSERT INTO `admin_operation_log` VALUES (1215, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 16:49:55', '2022-02-20 16:49:55');
INSERT INTO `admin_operation_log` VALUES (1216, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-20 16:50:38', '2022-02-20 16:50:38');
INSERT INTO `admin_operation_log` VALUES (1217, 1, 'admin/news/3/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:00:27', '2022-02-20 17:00:27');
INSERT INTO `admin_operation_log` VALUES (1218, 1, 'admin/news/3', 'PUT', '127.0.0.1', '{\"platform_id\":\"2\",\"title\":\"\\u9053\\u8def\\u6551\\u63f4\\u670d\\u52d9\",\"content\":\"\\u6700\\u65b0\\u512a\\u60e0\",\"_token\":\"pYZNYuzsbrVxizNm1365SPg7oEmV7gtZOVUsdnrp\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/news\"}', '2022-02-20 17:00:31', '2022-02-20 17:00:31');
INSERT INTO `admin_operation_log` VALUES (1219, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-20 17:00:31', '2022-02-20 17:00:31');
INSERT INTO `admin_operation_log` VALUES (1220, 1, 'admin/news/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:00:34', '2022-02-20 17:00:34');
INSERT INTO `admin_operation_log` VALUES (1221, 1, 'admin/news/4', 'PUT', '127.0.0.1', '{\"platform_id\":\"2\",\"title\":\"\\u9053\\u8def\\u670d\\u52d9\\u6700\\u65b0\\u512a\\u60e0\",\"content\":\"\\u6700\\u65b0\\u512a\\u60e02\",\"_token\":\"pYZNYuzsbrVxizNm1365SPg7oEmV7gtZOVUsdnrp\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/news\"}', '2022-02-20 17:00:52', '2022-02-20 17:00:52');
INSERT INTO `admin_operation_log` VALUES (1222, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-20 17:00:53', '2022-02-20 17:00:53');
INSERT INTO `admin_operation_log` VALUES (1223, 1, 'admin/news/3/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:00:58', '2022-02-20 17:00:58');
INSERT INTO `admin_operation_log` VALUES (1224, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:01:01', '2022-02-20 17:01:01');
INSERT INTO `admin_operation_log` VALUES (1225, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:01:37', '2022-02-20 17:01:37');
INSERT INTO `admin_operation_log` VALUES (1226, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:01:45', '2022-02-20 17:01:45');
INSERT INTO `admin_operation_log` VALUES (1227, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:01:46', '2022-02-20 17:01:46');
INSERT INTO `admin_operation_log` VALUES (1228, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:01:48', '2022-02-20 17:01:48');
INSERT INTO `admin_operation_log` VALUES (1229, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:01:49', '2022-02-20 17:01:49');
INSERT INTO `admin_operation_log` VALUES (1230, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:01:50', '2022-02-20 17:01:50');
INSERT INTO `admin_operation_log` VALUES (1231, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:01:51', '2022-02-20 17:01:51');
INSERT INTO `admin_operation_log` VALUES (1232, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:01:51', '2022-02-20 17:01:51');
INSERT INTO `admin_operation_log` VALUES (1233, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:02:04', '2022-02-20 17:02:04');
INSERT INTO `admin_operation_log` VALUES (1234, 1, 'admin/rule/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:06:44', '2022-02-20 17:06:44');
INSERT INTO `admin_operation_log` VALUES (1235, 1, 'admin/rule/1', 'PUT', '127.0.0.1', '{\"platform_id\":\"2\",\"enable\":\"1\",\"content\":\"<h2>\\u670d\\u52d9\\u6d41\\u7a0b\\u898f\\u7bc4<\\/h2>\\r\\n\\r\\n<p>\\u7533\\u544a\\u672c\\u670d\\u52d9\\u6642\\uff0c\\u8acb\\u5982\\u5be6\\u586b\\u5beb\\u7533\\u544a\\u5167\\u5bb9\\uff0c\\u5f8c\\u7e8c\\u7531\\u670d\\u52d9\\u4eba\\u54e1\\u8207\\u60a8\\u78ba\\u8a8d\\u8eab\\u5206\\u4e26\\u9810\\u5831\\u62b5\\u9054\\u6642\\u9593\\u3001\\u670d\\u52d9\\u8eca\\u8eca\\u865f\\u3001\\u670d\\u52d9\\u4eba\\u54e1\\u59d3\\u540d\\u3002<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<h2>\\u670d\\u52d9\\u7bc4\\u570d<\\/h2>\\r\\n\\r\\n<p>\\u4fc2\\u65bc\\u81fa\\u7063\\u672c\\u5cf6\\u3001\\u6f8e\\u6e56\\u3001\\u91d1\\u9580\\u53ca\\u99ac\\u7956\\u5730\\u5340\\u4e14\\u6551\\u63f4\\u8eca\\u8f1b\\u6240\\u80fd\\u884c\\u99db\\u53ca\\u4f5c\\u696d\\u4e4b\\u9053\\u8def\\u8005\\uff0c\\u7686\\u53ef\\u4eab\\u6709\\u672c\\u670d\\u52d9\\u3002<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<h2>\\u6536\\u8cbb\\u6a19\\u6e96<\\/h2>\\r\\n\\r\\n<ul>\\r\\n\\t<li>\\u5e73\\u9762\\u9053\\u8def 10\\u516c\\u91cc\\u51671,200\\u5143\\uff0c\\u8d85\\u904e\\u6bcf\\u516c\\u91cc\\u52a0\\u653650\\u5143<\\/li>\\r\\n\\t<li>\\u9ad8\\u901f\\u516c\\u8def(\\u4e0d\\u542b\\u570b\\u4e94) \\u4f9d\\u9ad8\\u5de5\\u5c40\\u898f\\u5b9a\\uff0c\\u8996\\u65b0\\u8eca\\u50f9\\u8207\\u8eca\\u8eab\\u9ad8\\u5ea6\\u4e0d\\u540c\\uff0c10\\u516c\\u91cc\\u51671,500~20,000\\u5143<\\/li>\\r\\n<\\/ul>\",\"_token\":\"pYZNYuzsbrVxizNm1365SPg7oEmV7gtZOVUsdnrp\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/rule\"}', '2022-02-20 17:06:52', '2022-02-20 17:06:52');
INSERT INTO `admin_operation_log` VALUES (1236, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-20 17:06:53', '2022-02-20 17:06:53');
INSERT INTO `admin_operation_log` VALUES (1237, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-20 17:08:19', '2022-02-20 17:08:19');
INSERT INTO `admin_operation_log` VALUES (1238, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:15:38', '2022-02-20 17:15:38');
INSERT INTO `admin_operation_log` VALUES (1239, 1, 'admin/rule/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:15:41', '2022-02-20 17:15:41');
INSERT INTO `admin_operation_log` VALUES (1240, 1, 'admin/rule/1', 'PUT', '127.0.0.1', '{\"platform_id\":\"2\",\"enable\":\"1\",\"content\":\"<h2><strong>\\u670d\\u52d9\\u6d41\\u7a0b\\u898f\\u7bc4<\\/strong><\\/h2>\\r\\n\\r\\n<p>\\u7533\\u544a\\u672c\\u670d\\u52d9\\u6642\\uff0c\\u8acb\\u5982\\u5be6\\u586b\\u5beb\\u7533\\u544a\\u5167\\u5bb9\\uff0c\\u5f8c\\u7e8c\\u7531\\u670d\\u52d9\\u4eba\\u54e1\\u8207\\u60a8\\u78ba\\u8a8d\\u8eab\\u5206\\u4e26\\u9810\\u5831\\u62b5\\u9054\\u6642\\u9593\\u3001\\u670d\\u52d9\\u8eca\\u8eca\\u865f\\u3001\\u670d\\u52d9\\u4eba\\u54e1\\u59d3\\u540d\\u3002<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<h2><strong>\\u670d\\u52d9\\u7bc4\\u570d<\\/strong><\\/h2>\\r\\n\\r\\n<p>\\u4fc2\\u65bc\\u81fa\\u7063\\u672c\\u5cf6\\u3001\\u6f8e\\u6e56\\u3001\\u91d1\\u9580\\u53ca\\u99ac\\u7956\\u5730\\u5340\\u4e14\\u6551\\u63f4\\u8eca\\u8f1b\\u6240\\u80fd\\u884c\\u99db\\u53ca\\u4f5c\\u696d\\u4e4b\\u9053\\u8def\\u8005\\uff0c\\u7686\\u53ef\\u4eab\\u6709\\u672c\\u670d\\u52d9\\u3002<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<h2><strong>\\u6536\\u8cbb\\u6a19\\u6e96<\\/strong><\\/h2>\\r\\n\\r\\n<ul>\\r\\n\\t<li>\\u5e73\\u9762\\u9053\\u8def 10\\u516c\\u91cc\\u51671,200\\u5143\\uff0c\\u8d85\\u904e\\u6bcf\\u516c\\u91cc\\u52a0\\u653650\\u5143<\\/li>\\r\\n\\t<li>\\u9ad8\\u901f\\u516c\\u8def(\\u4e0d\\u542b\\u570b\\u4e94) \\u4f9d\\u9ad8\\u5de5\\u5c40\\u898f\\u5b9a\\uff0c\\u8996\\u65b0\\u8eca\\u50f9\\u8207\\u8eca\\u8eab\\u9ad8\\u5ea6\\u4e0d\\u540c\\uff0c10\\u516c\\u91cc\\u51671,500~20,000\\u5143<\\/li>\\r\\n<\\/ul>\",\"_token\":\"pYZNYuzsbrVxizNm1365SPg7oEmV7gtZOVUsdnrp\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/rule\"}', '2022-02-20 17:15:54', '2022-02-20 17:15:54');
INSERT INTO `admin_operation_log` VALUES (1241, 1, 'admin/rule', 'GET', '127.0.0.1', '[]', '2022-02-20 17:15:55', '2022-02-20 17:15:55');
INSERT INTO `admin_operation_log` VALUES (1242, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:30:37', '2022-02-20 17:30:37');
INSERT INTO `admin_operation_log` VALUES (1243, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:30:38', '2022-02-20 17:30:38');
INSERT INTO `admin_operation_log` VALUES (1244, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:30:39', '2022-02-20 17:30:39');
INSERT INTO `admin_operation_log` VALUES (1245, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:30:40', '2022-02-20 17:30:40');
INSERT INTO `admin_operation_log` VALUES (1246, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:30:41', '2022-02-20 17:30:41');
INSERT INTO `admin_operation_log` VALUES (1247, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:30:42', '2022-02-20 17:30:42');
INSERT INTO `admin_operation_log` VALUES (1248, 1, 'admin/ad/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:30:44', '2022-02-20 17:30:44');
INSERT INTO `admin_operation_log` VALUES (1249, 1, 'admin/ad/1', 'PUT', '127.0.0.1', '{\"platform_id\":\"2\",\"url\":\"http:\\/\\/yahoo.com.tw\",\"seq\":\"1\",\"enable\":\"1\",\"_token\":\"pYZNYuzsbrVxizNm1365SPg7oEmV7gtZOVUsdnrp\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/ad\"}', '2022-02-20 17:31:05', '2022-02-20 17:31:05');
INSERT INTO `admin_operation_log` VALUES (1250, 1, 'admin/ad', 'GET', '127.0.0.1', '[]', '2022-02-20 17:31:05', '2022-02-20 17:31:05');
INSERT INTO `admin_operation_log` VALUES (1251, 1, 'admin/ad/3/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:31:22', '2022-02-20 17:31:22');
INSERT INTO `admin_operation_log` VALUES (1252, 1, 'admin/ad/3', 'PUT', '127.0.0.1', '{\"platform_id\":\"4\",\"url\":\"http:\\/\\/yahoo.com.tw\",\"seq\":\"0\",\"enable\":\"1\",\"_token\":\"pYZNYuzsbrVxizNm1365SPg7oEmV7gtZOVUsdnrp\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/ad\"}', '2022-02-20 17:31:38', '2022-02-20 17:31:38');
INSERT INTO `admin_operation_log` VALUES (1253, 1, 'admin/ad', 'GET', '127.0.0.1', '[]', '2022-02-20 17:31:39', '2022-02-20 17:31:39');
INSERT INTO `admin_operation_log` VALUES (1254, 1, 'admin/ad/1,2', 'DELETE', '127.0.0.1', '{\"_method\":\"delete\",\"_token\":\"pYZNYuzsbrVxizNm1365SPg7oEmV7gtZOVUsdnrp\"}', '2022-02-20 17:32:30', '2022-02-20 17:32:30');
INSERT INTO `admin_operation_log` VALUES (1255, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:32:30', '2022-02-20 17:32:30');
INSERT INTO `admin_operation_log` VALUES (1256, 1, 'admin/ad', 'GET', '127.0.0.1', '[]', '2022-02-20 17:32:31', '2022-02-20 17:32:31');
INSERT INTO `admin_operation_log` VALUES (1257, 1, 'admin/ad/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:32:50', '2022-02-20 17:32:50');
INSERT INTO `admin_operation_log` VALUES (1258, 1, 'admin/ad', 'POST', '127.0.0.1', '{\"platform_id\":\"2\",\"url\":\"https:\\/\\/tw.yahoo.com\\/\",\"seq\":\"0\",\"enable\":\"1\",\"_token\":\"pYZNYuzsbrVxizNm1365SPg7oEmV7gtZOVUsdnrp\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/ad\"}', '2022-02-20 17:33:23', '2022-02-20 17:33:23');
INSERT INTO `admin_operation_log` VALUES (1259, 1, 'admin/ad', 'GET', '127.0.0.1', '[]', '2022-02-20 17:33:23', '2022-02-20 17:33:23');
INSERT INTO `admin_operation_log` VALUES (1260, 1, 'admin/ad/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:34:07', '2022-02-20 17:34:07');
INSERT INTO `admin_operation_log` VALUES (1261, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:34:11', '2022-02-20 17:34:11');
INSERT INTO `admin_operation_log` VALUES (1262, 1, 'admin/ad', 'GET', '127.0.0.1', '[]', '2022-02-20 17:34:36', '2022-02-20 17:34:36');
INSERT INTO `admin_operation_log` VALUES (1263, 1, 'admin/ad/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:34:56', '2022-02-20 17:34:56');
INSERT INTO `admin_operation_log` VALUES (1264, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:34:57', '2022-02-20 17:34:57');
INSERT INTO `admin_operation_log` VALUES (1265, 1, 'admin/ad/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:34:58', '2022-02-20 17:34:58');
INSERT INTO `admin_operation_log` VALUES (1266, 1, 'admin/ad', 'POST', '127.0.0.1', '{\"platform_id\":\"2\",\"url\":\"https:\\/\\/tw.yahoo.com\\/\",\"seq\":\"2\",\"enable\":\"1\",\"_token\":\"pYZNYuzsbrVxizNm1365SPg7oEmV7gtZOVUsdnrp\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/ad\"}', '2022-02-20 17:35:28', '2022-02-20 17:35:28');
INSERT INTO `admin_operation_log` VALUES (1267, 1, 'admin/ad', 'GET', '127.0.0.1', '[]', '2022-02-20 17:35:28', '2022-02-20 17:35:28');
INSERT INTO `admin_operation_log` VALUES (1268, 1, 'admin/ad/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:35:43', '2022-02-20 17:35:43');
INSERT INTO `admin_operation_log` VALUES (1269, 1, 'admin/ad/4/edit', 'GET', '127.0.0.1', '[]', '2022-02-20 17:36:24', '2022-02-20 17:36:24');
INSERT INTO `admin_operation_log` VALUES (1270, 1, 'admin/ad/4/edit', 'GET', '127.0.0.1', '[]', '2022-02-20 17:36:34', '2022-02-20 17:36:34');
INSERT INTO `admin_operation_log` VALUES (1271, 1, 'admin/ad/4', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:37:05', '2022-02-20 17:37:05');
INSERT INTO `admin_operation_log` VALUES (1272, 1, 'admin/ad/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:37:08', '2022-02-20 17:37:08');
INSERT INTO `admin_operation_log` VALUES (1273, 1, 'admin/ad/4', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:37:11', '2022-02-20 17:37:11');
INSERT INTO `admin_operation_log` VALUES (1274, 1, 'admin/ad/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:37:13', '2022-02-20 17:37:13');
INSERT INTO `admin_operation_log` VALUES (1275, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:37:15', '2022-02-20 17:37:15');
INSERT INTO `admin_operation_log` VALUES (1276, 1, 'admin/ad/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:37:28', '2022-02-20 17:37:28');
INSERT INTO `admin_operation_log` VALUES (1277, 1, 'admin/ad/4', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:37:30', '2022-02-20 17:37:30');
INSERT INTO `admin_operation_log` VALUES (1278, 1, 'admin/ad/4', 'GET', '127.0.0.1', '[]', '2022-02-20 17:37:51', '2022-02-20 17:37:51');
INSERT INTO `admin_operation_log` VALUES (1279, 1, 'admin/ad/4', 'GET', '127.0.0.1', '[]', '2022-02-20 17:38:16', '2022-02-20 17:38:16');
INSERT INTO `admin_operation_log` VALUES (1280, 1, 'admin/ad/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:38:18', '2022-02-20 17:38:18');
INSERT INTO `admin_operation_log` VALUES (1281, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:38:19', '2022-02-20 17:38:19');
INSERT INTO `admin_operation_log` VALUES (1282, 1, 'admin/platform/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:38:21', '2022-02-20 17:38:21');
INSERT INTO `admin_operation_log` VALUES (1283, 1, 'admin/platform/2', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:38:23', '2022-02-20 17:38:23');
INSERT INTO `admin_operation_log` VALUES (1284, 1, 'admin/platform/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:38:25', '2022-02-20 17:38:25');
INSERT INTO `admin_operation_log` VALUES (1285, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:38:26', '2022-02-20 17:38:26');
INSERT INTO `admin_operation_log` VALUES (1286, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:38:29', '2022-02-20 17:38:29');
INSERT INTO `admin_operation_log` VALUES (1287, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:38:30', '2022-02-20 17:38:30');
INSERT INTO `admin_operation_log` VALUES (1288, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:38:34', '2022-02-20 17:38:34');
INSERT INTO `admin_operation_log` VALUES (1289, 1, 'admin/platform/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:38:36', '2022-02-20 17:38:36');
INSERT INTO `admin_operation_log` VALUES (1290, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:38:39', '2022-02-20 17:38:39');
INSERT INTO `admin_operation_log` VALUES (1291, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:38:40', '2022-02-20 17:38:40');
INSERT INTO `admin_operation_log` VALUES (1292, 1, 'admin/news/3/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:38:42', '2022-02-20 17:38:42');
INSERT INTO `admin_operation_log` VALUES (1293, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:38:45', '2022-02-20 17:38:45');
INSERT INTO `admin_operation_log` VALUES (1294, 1, 'admin/rule/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:38:47', '2022-02-20 17:38:47');
INSERT INTO `admin_operation_log` VALUES (1295, 1, 'admin/rule/1', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:38:49', '2022-02-20 17:38:49');
INSERT INTO `admin_operation_log` VALUES (1296, 1, 'admin/rule/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:38:51', '2022-02-20 17:38:51');
INSERT INTO `admin_operation_log` VALUES (1297, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:38:53', '2022-02-20 17:38:53');
INSERT INTO `admin_operation_log` VALUES (1298, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:39:08', '2022-02-20 17:39:08');
INSERT INTO `admin_operation_log` VALUES (1299, 1, 'admin/platform/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:39:10', '2022-02-20 17:39:10');
INSERT INTO `admin_operation_log` VALUES (1300, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:39:12', '2022-02-20 17:39:12');
INSERT INTO `admin_operation_log` VALUES (1301, 1, 'admin/platform/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:39:14', '2022-02-20 17:39:14');
INSERT INTO `admin_operation_log` VALUES (1302, 1, 'admin/platform/2', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:39:17', '2022-02-20 17:39:17');
INSERT INTO `admin_operation_log` VALUES (1303, 1, 'admin/platform/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:39:19', '2022-02-20 17:39:19');
INSERT INTO `admin_operation_log` VALUES (1304, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:39:46', '2022-02-20 17:39:46');
INSERT INTO `admin_operation_log` VALUES (1305, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:39:47', '2022-02-20 17:39:47');
INSERT INTO `admin_operation_log` VALUES (1306, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:39:49', '2022-02-20 17:39:49');
INSERT INTO `admin_operation_log` VALUES (1307, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:39:50', '2022-02-20 17:39:50');
INSERT INTO `admin_operation_log` VALUES (1308, 1, 'admin/ad/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:39:53', '2022-02-20 17:39:53');
INSERT INTO `admin_operation_log` VALUES (1309, 1, 'admin/ad/4', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:39:55', '2022-02-20 17:39:55');
INSERT INTO `admin_operation_log` VALUES (1310, 1, 'admin/ad/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:39:56', '2022-02-20 17:39:56');
INSERT INTO `admin_operation_log` VALUES (1311, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:39:57', '2022-02-20 17:39:57');
INSERT INTO `admin_operation_log` VALUES (1312, 1, 'admin/ad', 'GET', '127.0.0.1', '[]', '2022-02-20 17:39:58', '2022-02-20 17:39:58');
INSERT INTO `admin_operation_log` VALUES (1313, 1, 'admin/ad/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:40:01', '2022-02-20 17:40:01');
INSERT INTO `admin_operation_log` VALUES (1314, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:40:03', '2022-02-20 17:40:03');
INSERT INTO `admin_operation_log` VALUES (1315, 1, 'admin/ad/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:40:05', '2022-02-20 17:40:05');
INSERT INTO `admin_operation_log` VALUES (1316, 1, 'admin/ad/4', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:40:06', '2022-02-20 17:40:06');
INSERT INTO `admin_operation_log` VALUES (1317, 1, 'admin/ad/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:40:07', '2022-02-20 17:40:07');
INSERT INTO `admin_operation_log` VALUES (1318, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:40:10', '2022-02-20 17:40:10');
INSERT INTO `admin_operation_log` VALUES (1319, 1, 'admin/ad', 'GET', '127.0.0.1', '[]', '2022-02-20 17:40:11', '2022-02-20 17:40:11');
INSERT INTO `admin_operation_log` VALUES (1320, 1, 'admin/ad/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:40:20', '2022-02-20 17:40:20');
INSERT INTO `admin_operation_log` VALUES (1321, 1, 'admin/ad/4/edit', 'GET', '127.0.0.1', '[]', '2022-02-20 17:40:22', '2022-02-20 17:40:22');
INSERT INTO `admin_operation_log` VALUES (1322, 1, 'admin/ad/4/edit', 'GET', '127.0.0.1', '[]', '2022-02-20 17:41:22', '2022-02-20 17:41:22');
INSERT INTO `admin_operation_log` VALUES (1323, 1, 'admin/ad/4/edit', 'GET', '127.0.0.1', '[]', '2022-02-20 17:41:24', '2022-02-20 17:41:24');
INSERT INTO `admin_operation_log` VALUES (1324, 1, 'admin/ad', 'GET', '127.0.0.1', '[]', '2022-02-20 17:41:33', '2022-02-20 17:41:33');
INSERT INTO `admin_operation_log` VALUES (1325, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:41:37', '2022-02-20 17:41:37');
INSERT INTO `admin_operation_log` VALUES (1326, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:41:38', '2022-02-20 17:41:38');
INSERT INTO `admin_operation_log` VALUES (1327, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:41:40', '2022-02-20 17:41:40');
INSERT INTO `admin_operation_log` VALUES (1328, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:41:40', '2022-02-20 17:41:40');
INSERT INTO `admin_operation_log` VALUES (1329, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:41:41', '2022-02-20 17:41:41');
INSERT INTO `admin_operation_log` VALUES (1330, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:42:16', '2022-02-20 17:42:16');
INSERT INTO `admin_operation_log` VALUES (1331, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:42:18', '2022-02-20 17:42:18');
INSERT INTO `admin_operation_log` VALUES (1332, 1, 'admin/order', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:42:19', '2022-02-20 17:42:19');
INSERT INTO `admin_operation_log` VALUES (1333, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-20 17:42:49', '2022-02-20 17:42:49');
INSERT INTO `admin_operation_log` VALUES (1334, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:56:36', '2022-02-20 17:56:36');
INSERT INTO `admin_operation_log` VALUES (1335, 1, 'admin/news/3/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:56:39', '2022-02-20 17:56:39');
INSERT INTO `admin_operation_log` VALUES (1336, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:57:14', '2022-02-20 17:57:14');
INSERT INTO `admin_operation_log` VALUES (1337, 1, 'admin/rule', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:59:17', '2022-02-20 17:59:17');
INSERT INTO `admin_operation_log` VALUES (1338, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:59:19', '2022-02-20 17:59:19');
INSERT INTO `admin_operation_log` VALUES (1339, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-20 17:59:20', '2022-02-20 17:59:20');
INSERT INTO `admin_operation_log` VALUES (1340, 1, 'admin/news/3/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 17:59:27', '2022-02-20 17:59:27');
INSERT INTO `admin_operation_log` VALUES (1341, 1, 'admin/news/3', 'PUT', '127.0.0.1', '{\"platform_id\":\"2\",\"title\":\"\\u9053\\u8def\\u6551\\u63f4\\u670d\\u52d9\",\"content\":\"<h2>\\u670d\\u52d9\\u6d41\\u7a0b\\u898f\\u7bc4<\\/h2>\\r\\n\\r\\n<p>\\u7533\\u544a\\u672c\\u670d\\u52d9\\u6642\\uff0c\\u8acb\\u5982\\u5be6\\u586b\\u5beb\\u7533\\u544a\\u5167\\u5bb9\\uff0c\\u5f8c\\u7e8c\\u7531\\u670d\\u52d9\\u4eba\\u54e1\\u8207\\u60a8\\u78ba\\u8a8d\\u8eab\\u5206\\u4e26\\u9810\\u5831\\u62b5\\u9054\\u6642\\u9593\\u3001\\u670d\\u52d9\\u8eca\\u8eca\\u865f\\u3001\\u670d\\u52d9\\u4eba\\u54e1\\u59d3\\u540d\\u3002<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<h2>\\u670d\\u52d9\\u7bc4\\u570d<\\/h2>\\r\\n\\r\\n<p>\\u4fc2\\u65bc\\u81fa\\u7063\\u672c\\u5cf6\\u3001\\u6f8e\\u6e56\\u3001\\u91d1\\u9580\\u53ca\\u99ac\\u7956\\u5730\\u5340\\u4e14\\u6551\\u63f4\\u8eca\\u8f1b\\u6240\\u80fd\\u884c\\u99db\\u53ca\\u4f5c\\u696d\\u4e4b\\u9053\\u8def\\u8005\\uff0c\\u7686\\u53ef\\u4eab\\u6709\\u672c\\u670d\\u52d9\\u3002<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<h2>\\u6536\\u8cbb\\u6a19\\u6e96<\\/h2>\\r\\n\\r\\n<ul>\\r\\n\\t<li>\\u5e73\\u9762\\u9053\\u8def 10\\u516c\\u91cc\\u51671,200\\u5143\\uff0c\\u8d85\\u904e\\u6bcf\\u516c\\u91cc\\u52a0\\u653650\\u5143<\\/li>\\r\\n\\t<li>\\u9ad8\\u901f\\u516c\\u8def(\\u4e0d\\u542b\\u570b\\u4e94) \\u4f9d\\u9ad8\\u5de5\\u5c40\\u898f\\u5b9a\\uff0c\\u8996\\u65b0\\u8eca\\u50f9\\u8207\\u8eca\\u8eab\\u9ad8\\u5ea6\\u4e0d\\u540c\\uff0c10\\u516c\\u91cc\\u51671,500~20,000\\u5143<\\/li>\\r\\n<\\/ul>\\r\\n\\r\\n<h2>\\u670d\\u52d9\\u6d41\\u7a0b\\u898f\\u7bc4<\\/h2>\\r\\n\\r\\n<p>\\u7533\\u544a\\u672c\\u670d\\u52d9\\u6642\\uff0c\\u8acb\\u5982\\u5be6\\u586b\\u5beb\\u7533\\u544a\\u5167\\u5bb9\\uff0c\\u5f8c\\u7e8c\\u7531\\u670d\\u52d9\\u4eba\\u54e1\\u8207\\u60a8\\u78ba\\u8a8d\\u8eab\\u5206\\u4e26\\u9810\\u5831\\u62b5\\u9054\\u6642\\u9593\\u3001\\u670d\\u52d9\\u8eca\\u8eca\\u865f\\u3001\\u670d\\u52d9\\u4eba\\u54e1\\u59d3\\u540d\\u3002<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<h2>\\u670d\\u52d9\\u7bc4\\u570d<\\/h2>\\r\\n\\r\\n<p>\\u4fc2\\u65bc\\u81fa\\u7063\\u672c\\u5cf6\\u3001\\u6f8e\\u6e56\\u3001\\u91d1\\u9580\\u53ca\\u99ac\\u7956\\u5730\\u5340\\u4e14\\u6551\\u63f4\\u8eca\\u8f1b\\u6240\\u80fd\\u884c\\u99db\\u53ca\\u4f5c\\u696d\\u4e4b\\u9053\\u8def\\u8005\\uff0c\\u7686\\u53ef\\u4eab\\u6709\\u672c\\u670d\\u52d9\\u3002<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<h2>\\u6536\\u8cbb\\u6a19\\u6e96<\\/h2>\\r\\n\\r\\n<ul>\\r\\n\\t<li>\\u5e73\\u9762\\u9053\\u8def 10\\u516c\\u91cc\\u51671,200\\u5143\\uff0c\\u8d85\\u904e\\u6bcf\\u516c\\u91cc\\u52a0\\u653650\\u5143<\\/li>\\r\\n\\t<li>\\u9ad8\\u901f\\u516c\\u8def(\\u4e0d\\u542b\\u570b\\u4e94) \\u4f9d\\u9ad8\\u5de5\\u5c40\\u898f\\u5b9a\\uff0c\\u8996\\u65b0\\u8eca\\u50f9\\u8207\\u8eca\\u8eab\\u9ad8\\u5ea6\\u4e0d\\u540c\\uff0c10\\u516c\\u91cc\\u51671,500~20,000\\u5143<\\/li>\\r\\n<\\/ul>\\r\\n\\r\\n<h2>\\u670d\\u52d9\\u6d41\\u7a0b\\u898f\\u7bc4<\\/h2>\\r\\n\\r\\n<p>\\u7533\\u544a\\u672c\\u670d\\u52d9\\u6642\\uff0c\\u8acb\\u5982\\u5be6\\u586b\\u5beb\\u7533\\u544a\\u5167\\u5bb9\\uff0c\\u5f8c\\u7e8c\\u7531\\u670d\\u52d9\\u4eba\\u54e1\\u8207\\u60a8\\u78ba\\u8a8d\\u8eab\\u5206\\u4e26\\u9810\\u5831\\u62b5\\u9054\\u6642\\u9593\\u3001\\u670d\\u52d9\\u8eca\\u8eca\\u865f\\u3001\\u670d\\u52d9\\u4eba\\u54e1\\u59d3\\u540d\\u3002<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<h2>\\u670d\\u52d9\\u7bc4\\u570d<\\/h2>\\r\\n\\r\\n<p>\\u4fc2\\u65bc\\u81fa\\u7063\\u672c\\u5cf6\\u3001\\u6f8e\\u6e56\\u3001\\u91d1\\u9580\\u53ca\\u99ac\\u7956\\u5730\\u5340\\u4e14\\u6551\\u63f4\\u8eca\\u8f1b\\u6240\\u80fd\\u884c\\u99db\\u53ca\\u4f5c\\u696d\\u4e4b\\u9053\\u8def\\u8005\\uff0c\\u7686\\u53ef\\u4eab\\u6709\\u672c\\u670d\\u52d9\\u3002<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<h2>\\u6536\\u8cbb\\u6a19\\u6e96<\\/h2>\\r\\n\\r\\n<ul>\\r\\n\\t<li>\\u5e73\\u9762\\u9053\\u8def 10\\u516c\\u91cc\\u51671,200\\u5143\\uff0c\\u8d85\\u904e\\u6bcf\\u516c\\u91cc\\u52a0\\u653650\\u5143<\\/li>\\r\\n\\t<li>\\u9ad8\\u901f\\u516c\\u8def(\\u4e0d\\u542b\\u570b\\u4e94) \\u4f9d\\u9ad8\\u5de5\\u5c40\\u898f\\u5b9a\\uff0c\\u8996\\u65b0\\u8eca\\u50f9\\u8207\\u8eca\\u8eab\\u9ad8\\u5ea6\\u4e0d\\u540c\\uff0c10\\u516c\\u91cc\\u51671,500~20,000\\u5143<\\/li>\\r\\n<\\/ul>\\r\\n\\r\\n<h2>\\u670d\\u52d9\\u6d41\\u7a0b\\u898f\\u7bc4<\\/h2>\\r\\n\\r\\n<p>\\u7533\\u544a\\u672c\\u670d\\u52d9\\u6642\\uff0c\\u8acb\\u5982\\u5be6\\u586b\\u5beb\\u7533\\u544a\\u5167\\u5bb9\\uff0c\\u5f8c\\u7e8c\\u7531\\u670d\\u52d9\\u4eba\\u54e1\\u8207\\u60a8\\u78ba\\u8a8d\\u8eab\\u5206\\u4e26\\u9810\\u5831\\u62b5\\u9054\\u6642\\u9593\\u3001\\u670d\\u52d9\\u8eca\\u8eca\\u865f\\u3001\\u670d\\u52d9\\u4eba\\u54e1\\u59d3\\u540d\\u3002<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<h2>\\u670d\\u52d9\\u7bc4\\u570d<\\/h2>\\r\\n\\r\\n<p>\\u4fc2\\u65bc\\u81fa\\u7063\\u672c\\u5cf6\\u3001\\u6f8e\\u6e56\\u3001\\u91d1\\u9580\\u53ca\\u99ac\\u7956\\u5730\\u5340\\u4e14\\u6551\\u63f4\\u8eca\\u8f1b\\u6240\\u80fd\\u884c\\u99db\\u53ca\\u4f5c\\u696d\\u4e4b\\u9053\\u8def\\u8005\\uff0c\\u7686\\u53ef\\u4eab\\u6709\\u672c\\u670d\\u52d9\\u3002<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<h2>\\u6536\\u8cbb\\u6a19\\u6e96<\\/h2>\\r\\n\\r\\n<ul>\\r\\n\\t<li>\\u5e73\\u9762\\u9053\\u8def 10\\u516c\\u91cc\\u51671,200\\u5143\\uff0c\\u8d85\\u904e\\u6bcf\\u516c\\u91cc\\u52a0\\u653650\\u5143<\\/li>\\r\\n\\t<li>\\u9ad8\\u901f\\u516c\\u8def(\\u4e0d\\u542b\\u570b\\u4e94) \\u4f9d\\u9ad8\\u5de5\\u5c40\\u898f\\u5b9a\\uff0c\\u8996\\u65b0\\u8eca\\u50f9\\u8207\\u8eca\\u8eab\\u9ad8\\u5ea6\\u4e0d\\u540c\\uff0c10\\u516c\\u91cc\\u51671,500~20,000\\u5143<\\/li>\\r\\n<\\/ul>\",\"_token\":\"pYZNYuzsbrVxizNm1365SPg7oEmV7gtZOVUsdnrp\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/news\"}', '2022-02-20 17:59:42', '2022-02-20 17:59:42');
INSERT INTO `admin_operation_log` VALUES (1342, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-20 17:59:42', '2022-02-20 17:59:42');
INSERT INTO `admin_operation_log` VALUES (1343, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-20 18:07:14', '2022-02-20 18:07:14');
INSERT INTO `admin_operation_log` VALUES (1344, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-20 18:08:02', '2022-02-20 18:08:02');
INSERT INTO `admin_operation_log` VALUES (1345, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-20 18:08:23', '2022-02-20 18:08:23');
INSERT INTO `admin_operation_log` VALUES (1346, 1, 'admin/news/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-20 18:14:50', '2022-02-20 18:14:50');
INSERT INTO `admin_operation_log` VALUES (1347, 1, 'admin/news/4', 'PUT', '127.0.0.1', '{\"platform_id\":\"2\",\"title\":\"\\u670d\\u52d9\\u6700\\u65b0\\u512a\\u60e0\",\"content\":\"<p>\\u6700\\u65b0\\u512a\\u60e02<\\/p>\",\"_token\":\"pYZNYuzsbrVxizNm1365SPg7oEmV7gtZOVUsdnrp\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/news\"}', '2022-02-20 18:14:56', '2022-02-20 18:14:56');
INSERT INTO `admin_operation_log` VALUES (1348, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2022-02-20 18:14:56', '2022-02-20 18:14:56');
INSERT INTO `admin_operation_log` VALUES (1349, 1, 'admin', 'GET', '127.0.0.1', '[]', '2022-02-28 16:06:06', '2022-02-28 16:06:06');
INSERT INTO `admin_operation_log` VALUES (1350, 1, 'admin/order', 'GET', '127.0.0.1', '[]', '2022-02-28 16:06:07', '2022-02-28 16:06:07');
INSERT INTO `admin_operation_log` VALUES (1351, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-28 16:06:09', '2022-02-28 16:06:09');
INSERT INTO `admin_operation_log` VALUES (1352, 1, 'admin/pay/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-28 16:06:26', '2022-02-28 16:06:26');
INSERT INTO `admin_operation_log` VALUES (1353, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-28 16:09:46', '2022-02-28 16:09:46');
INSERT INTO `admin_operation_log` VALUES (1354, 1, 'admin/pay/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-28 16:09:56', '2022-02-28 16:09:56');
INSERT INTO `admin_operation_log` VALUES (1355, 1, 'admin/pay', 'GET', '127.0.0.1', '[]', '2022-02-28 16:09:56', '2022-02-28 16:09:56');
INSERT INTO `admin_operation_log` VALUES (1356, 1, 'admin/pay/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-28 16:10:00', '2022-02-28 16:10:00');
INSERT INTO `admin_operation_log` VALUES (1357, 1, 'admin/pay', 'GET', '127.0.0.1', '[]', '2022-02-28 16:10:00', '2022-02-28 16:10:00');
INSERT INTO `admin_operation_log` VALUES (1358, 1, 'admin/pay', 'GET', '127.0.0.1', '[]', '2022-02-28 16:10:07', '2022-02-28 16:10:07');
INSERT INTO `admin_operation_log` VALUES (1359, 1, 'admin/pay/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-28 16:10:09', '2022-02-28 16:10:09');
INSERT INTO `admin_operation_log` VALUES (1360, 1, 'admin/pay', 'GET', '127.0.0.1', '[]', '2022-02-28 16:10:10', '2022-02-28 16:10:10');
INSERT INTO `admin_operation_log` VALUES (1361, 1, 'admin/pay', 'GET', '127.0.0.1', '[]', '2022-02-28 16:10:33', '2022-02-28 16:10:33');
INSERT INTO `admin_operation_log` VALUES (1362, 1, 'admin/pay', 'GET', '127.0.0.1', '[]', '2022-02-28 16:10:34', '2022-02-28 16:10:34');
INSERT INTO `admin_operation_log` VALUES (1363, 1, 'admin/pay/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-28 16:10:39', '2022-02-28 16:10:39');
INSERT INTO `admin_operation_log` VALUES (1364, 1, 'admin/pay', 'GET', '127.0.0.1', '[]', '2022-02-28 16:10:39', '2022-02-28 16:10:39');
INSERT INTO `admin_operation_log` VALUES (1365, 1, 'admin/pay/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-28 16:10:42', '2022-02-28 16:10:42');
INSERT INTO `admin_operation_log` VALUES (1366, 1, 'admin/pay', 'GET', '127.0.0.1', '[]', '2022-02-28 16:10:42', '2022-02-28 16:10:42');
INSERT INTO `admin_operation_log` VALUES (1367, 1, 'admin/platform', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-28 16:10:43', '2022-02-28 16:10:43');
INSERT INTO `admin_operation_log` VALUES (1368, 1, 'admin/platform/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-28 16:10:45', '2022-02-28 16:10:45');
INSERT INTO `admin_operation_log` VALUES (1369, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-28 16:10:47', '2022-02-28 16:10:47');
INSERT INTO `admin_operation_log` VALUES (1370, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-28 16:10:48', '2022-02-28 16:10:48');
INSERT INTO `admin_operation_log` VALUES (1371, 1, 'admin/ad', 'GET', '127.0.0.1', '[]', '2022-02-28 16:12:28', '2022-02-28 16:12:28');
INSERT INTO `admin_operation_log` VALUES (1372, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-28 16:16:44', '2022-02-28 16:16:44');
INSERT INTO `admin_operation_log` VALUES (1373, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-28 16:16:45', '2022-02-28 16:16:45');
INSERT INTO `admin_operation_log` VALUES (1374, 1, 'admin/disconut', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-28 16:16:46', '2022-02-28 16:16:46');
INSERT INTO `admin_operation_log` VALUES (1375, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-28 16:16:47', '2022-02-28 16:16:47');
INSERT INTO `admin_operation_log` VALUES (1376, 1, 'admin/ad', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-28 16:16:48', '2022-02-28 16:16:48');
INSERT INTO `admin_operation_log` VALUES (1377, 1, 'admin/pay', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-28 16:16:48', '2022-02-28 16:16:48');
INSERT INTO `admin_operation_log` VALUES (1378, 1, 'admin/pay/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-28 16:16:50', '2022-02-28 16:16:50');
INSERT INTO `admin_operation_log` VALUES (1379, 1, 'admin/pay', 'GET', '127.0.0.1', '[]', '2022-02-28 16:16:51', '2022-02-28 16:16:51');
INSERT INTO `admin_operation_log` VALUES (1380, 1, 'admin/pay', 'GET', '127.0.0.1', '[]', '2022-02-28 16:17:16', '2022-02-28 16:17:16');
INSERT INTO `admin_operation_log` VALUES (1381, 1, 'admin/pay/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2022-02-28 16:17:20', '2022-02-28 16:17:20');
INSERT INTO `admin_operation_log` VALUES (1382, 1, 'admin/pay/1', 'PUT', '127.0.0.1', '{\"platform_id\":\"2\",\"enable\":\"1\",\"content\":\"<ul>\\r\\n\\t<li>\\r\\n\\t<h3>\\u65b0\\u8eca\\u6703\\u54e1\\u514d\\u8cbb\\u6b0a\\u76ca<\\/h3>\\r\\n\\r\\n\\t<p>\\uff0d\\u8eca\\u8f1b\\u56e0\\u54c1\\u8cea\\u554f\\u984c\\u4ee5\\u81f4\\u7121\\u6cd5\\u884c\\u99db\\uff0c\\u81ea\\u8eca\\u8f1b\\u6545\\u969c\\u5730\\u9ede\\u8d77\\u7b97\\uff0c\\u62d6\\u81f3HONDA\\u6700\\u8fd1\\u6388\\u6b0a\\u7d93\\u92b7\\u5546\\u7dad\\u4fee\\u5ee0 30 \\u516c\\u91cc\\u5167\\u514d\\u8cbb\\uff0c\\u8d85\\u904e\\u90e8\\u4efd\\u6bcf\\u516c\\u91cc\\u52a0\\u6536 50 \\u5143\\uff0c\\u7531\\u60a8\\u81ea\\u4ed8\\u3002<\\/p>\\r\\n\\r\\n\\t<p>\\uff0d\\u5982\\u70ba\\u975e\\u54c1\\u8cea\\u56e0\\u7d20\\u6240\\u81f4\\u4e4b\\u62d6\\u540a\\u8cbb\\u7528\\u5247\\u7531\\u60a8\\u81ea\\u4ed8\\u3002<\\/p>\\r\\n\\t<\\/li>\\r\\n\\t<li>\\r\\n\\t<h3>\\u975e\\u65b0\\u8eca\\u6703\\u54e1\\u514d\\u8cbb\\u6b0a\\u76ca<\\/h3>\\r\\n\\r\\n\\t<p>10\\u516c\\u91cc\\u5167\\u62d6\\u540a\\u8cbb\\u7528-- \\u4e00\\u822c\\u9053\\u8def1500\\u5143\\u3001\\u570b\\u90531500\\u5143( \\u591c\\u9593\\u53ca\\u5047\\u65e5\\u52a0\\u6536500\\u5143 )\\u3001\\u6bcf\\u8d85\\u904e\\u4e00\\u516c\\u91cc\\u52a0\\u653650\\u5143\\uff0c\\u7279\\u6b8a\\u8655\\u7406\\u4f5c\\u696d\\u4e4b\\u6536\\u8cbb\\u5247\\u53e6\\u8a08\\u4e4b\\u3002 ( \\u82e5\\u6709\\u8cbb\\u7528\\u6a19\\u6e96\\u8b8a\\u66f4\\u6642, \\u4fc2\\u4f9d\\u9ad8\\u516c\\u5c40\\u516c\\u4f48\\u70ba\\u6e96 )<\\/p>\\r\\n\\t<\\/li>\\r\\n<\\/ul>\",\"_token\":\"rOsfrJKjXu8tKw39itzN75Tts4eGpvuS0FdU6nGn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1\\/tmsras\\/admin\\/pay\"}', '2022-02-28 16:17:36', '2022-02-28 16:17:36');
INSERT INTO `admin_operation_log` VALUES (1383, 1, 'admin/pay', 'GET', '127.0.0.1', '[]', '2022-02-28 16:17:36', '2022-02-28 16:17:36');

-- ----------------------------
-- Table structure for admin_permissions
-- ----------------------------
DROP TABLE IF EXISTS `admin_permissions`;
CREATE TABLE `admin_permissions`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `http_method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `http_path` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `admin_permissions_name_unique`(`name`) USING BTREE,
  UNIQUE INDEX `admin_permissions_slug_unique`(`slug`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_permissions
-- ----------------------------
INSERT INTO `admin_permissions` VALUES (1, 'All permission', '*', '', '*', NULL, NULL);
INSERT INTO `admin_permissions` VALUES (2, 'Dashboard', 'dashboard', 'GET', '/', NULL, NULL);
INSERT INTO `admin_permissions` VALUES (3, 'Login', 'auth.login', '', '/auth/login\r\n/auth/logout', NULL, NULL);
INSERT INTO `admin_permissions` VALUES (4, 'User setting', 'auth.setting', 'GET,PUT', '/auth/setting', NULL, NULL);
INSERT INTO `admin_permissions` VALUES (5, 'Auth management', 'auth.management', '', '/auth/roles\r\n/auth/permissions\r\n/auth/menu\r\n/auth/logs', NULL, NULL);
INSERT INTO `admin_permissions` VALUES (6, 'Exceptions reporter', 'ext.reporter', '', '/exceptions*', '2022-02-01 09:07:33', '2022-02-01 09:07:33');
INSERT INTO `admin_permissions` VALUES (7, 'Admin helpers', 'ext.helpers', '', '/helpers/*', '2022-02-01 09:34:08', '2022-02-01 09:34:08');

-- ----------------------------
-- Table structure for admin_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `admin_role_menu`;
CREATE TABLE `admin_role_menu`  (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `admin_role_menu_role_id_menu_id_index`(`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_role_menu
-- ----------------------------
INSERT INTO `admin_role_menu` VALUES (1, 9, NULL, NULL);
INSERT INTO `admin_role_menu` VALUES (1, 3, NULL, NULL);
INSERT INTO `admin_role_menu` VALUES (2, 3, NULL, NULL);
INSERT INTO `admin_role_menu` VALUES (1, 14, NULL, NULL);
INSERT INTO `admin_role_menu` VALUES (2, 14, NULL, NULL);
INSERT INTO `admin_role_menu` VALUES (3, 14, NULL, NULL);
INSERT INTO `admin_role_menu` VALUES (1, 15, NULL, NULL);
INSERT INTO `admin_role_menu` VALUES (2, 15, NULL, NULL);
INSERT INTO `admin_role_menu` VALUES (3, 15, NULL, NULL);
INSERT INTO `admin_role_menu` VALUES (1, 16, NULL, NULL);
INSERT INTO `admin_role_menu` VALUES (2, 16, NULL, NULL);
INSERT INTO `admin_role_menu` VALUES (3, 16, NULL, NULL);
INSERT INTO `admin_role_menu` VALUES (1, 17, NULL, NULL);
INSERT INTO `admin_role_menu` VALUES (2, 17, NULL, NULL);
INSERT INTO `admin_role_menu` VALUES (3, 17, NULL, NULL);
INSERT INTO `admin_role_menu` VALUES (1, 18, NULL, NULL);
INSERT INTO `admin_role_menu` VALUES (2, 18, NULL, NULL);
INSERT INTO `admin_role_menu` VALUES (3, 18, NULL, NULL);
INSERT INTO `admin_role_menu` VALUES (1, 19, NULL, NULL);
INSERT INTO `admin_role_menu` VALUES (2, 19, NULL, NULL);
INSERT INTO `admin_role_menu` VALUES (3, 19, NULL, NULL);
INSERT INTO `admin_role_menu` VALUES (1, 20, NULL, NULL);
INSERT INTO `admin_role_menu` VALUES (2, 20, NULL, NULL);
INSERT INTO `admin_role_menu` VALUES (3, 20, NULL, NULL);
INSERT INTO `admin_role_menu` VALUES (1, 2, NULL, NULL);

-- ----------------------------
-- Table structure for admin_role_permissions
-- ----------------------------
DROP TABLE IF EXISTS `admin_role_permissions`;
CREATE TABLE `admin_role_permissions`  (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `admin_role_permissions_role_id_permission_id_index`(`role_id`, `permission_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_role_permissions
-- ----------------------------
INSERT INTO `admin_role_permissions` VALUES (1, 1, NULL, NULL);
INSERT INTO `admin_role_permissions` VALUES (2, 1, NULL, NULL);
INSERT INTO `admin_role_permissions` VALUES (2, 2, NULL, NULL);
INSERT INTO `admin_role_permissions` VALUES (2, 3, NULL, NULL);
INSERT INTO `admin_role_permissions` VALUES (2, 4, NULL, NULL);
INSERT INTO `admin_role_permissions` VALUES (2, 5, NULL, NULL);
INSERT INTO `admin_role_permissions` VALUES (3, 1, NULL, NULL);

-- ----------------------------
-- Table structure for admin_role_users
-- ----------------------------
DROP TABLE IF EXISTS `admin_role_users`;
CREATE TABLE `admin_role_users`  (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `admin_role_users_role_id_user_id_index`(`role_id`, `user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_role_users
-- ----------------------------
INSERT INTO `admin_role_users` VALUES (1, 1, NULL, NULL);
INSERT INTO `admin_role_users` VALUES (2, 2, NULL, NULL);

-- ----------------------------
-- Table structure for admin_roles
-- ----------------------------
DROP TABLE IF EXISTS `admin_roles`;
CREATE TABLE `admin_roles`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `admin_roles_name_unique`(`name`) USING BTREE,
  UNIQUE INDEX `admin_roles_slug_unique`(`slug`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_roles
-- ----------------------------
INSERT INTO `admin_roles` VALUES (1, 'Administrator', 'administrator', '2022-02-01 08:49:03', '2022-02-01 08:49:03');
INSERT INTO `admin_roles` VALUES (2, '網站管理員', 'system', '2022-02-01 17:46:45', '2022-02-01 17:46:45');
INSERT INTO `admin_roles` VALUES (3, '使用者', 'user', '2022-02-01 18:38:08', '2022-02-01 18:38:08');

-- ----------------------------
-- Table structure for admin_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS `admin_user_permissions`;
CREATE TABLE `admin_user_permissions`  (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `admin_user_permissions_user_id_permission_id_index`(`user_id`, `permission_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_user_permissions
-- ----------------------------
INSERT INTO `admin_user_permissions` VALUES (2, 1, NULL, NULL);

-- ----------------------------
-- Table structure for admin_users
-- ----------------------------
DROP TABLE IF EXISTS `admin_users`;
CREATE TABLE `admin_users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `admin_users_username_unique`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_users
-- ----------------------------
INSERT INTO `admin_users` VALUES (1, 'admin', '$2y$10$bGtvLvqDI8dBwUOD2LWEhOnxkTxELMxTUk0xf.8KkjAxiyaBp3y4C', 'Administrator', NULL, 'c0gLw7TyfV4cTymRW6m9mRngXq18I7NbEH80Pn5JXzkAK2AXaHzfMpmoghOK', '2022-02-01 08:49:03', '2022-02-01 08:49:03');
INSERT INTO `admin_users` VALUES (2, 'system', '$2y$10$vwi/1X33SL4yJYBLuxI/m.rW1P72mY6FToynvg2k7jOQLYQf74Vtm', '系統管理者', NULL, 'qthOwJUfXNS6jKhZtCSw3VpAtcQU85OZhopjFt3MXsNmcE4tQ8VISUeDf6mN', '2022-02-01 17:45:49', '2022-02-01 17:50:34');

-- ----------------------------
-- Table structure for ads
-- ----------------------------
DROP TABLE IF EXISTS `ads`;
CREATE TABLE `ads`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `platform_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `enable` int(11) NOT NULL DEFAULT 1,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `seq` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ads
-- ----------------------------
INSERT INTO `ads` VALUES (3, '4', 1, 'http://yahoo.com.tw', 'images/e3a1c51e3eacadf102b8fc152a976a18.jpg', '2022-02-02 12:28:52', '2022-02-20 17:31:38', 0);
INSERT INTO `ads` VALUES (4, '2', 1, 'https://tw.yahoo.com/', 'images/98697840f5da2be018f49cb14a11919a.jpg', '2022-02-20 17:33:23', '2022-02-20 17:33:23', 0);
INSERT INTO `ads` VALUES (5, '2', 1, 'https://tw.yahoo.com/', 'images/777acb15ac6459b9a4e273c55d2588df.jpg', '2022-02-20 17:35:28', '2022-02-20 17:35:28', 2);

-- ----------------------------
-- Table structure for disconuts
-- ----------------------------
DROP TABLE IF EXISTS `disconuts`;
CREATE TABLE `disconuts`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `platform_id` int(11) NOT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '折扣代碼',
  `start_date` date NOT NULL COMMENT '有效開始日期',
  `end_date` date NOT NULL COMMENT '有效結束日期',
  `price` int(11) NOT NULL COMMENT '折扣金額',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of disconuts
-- ----------------------------
INSERT INTO `disconuts` VALUES (1, 2, 'CDR1001', '2022-02-01', '2022-02-03', 100, '2022-02-02 12:37:52', '2022-02-02 12:37:52');

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `failed_jobs_uuid_unique`(`uuid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for laravel_exceptions
-- ----------------------------
DROP TABLE IF EXISTS `laravel_exceptions`;
CREATE TABLE `laravel_exceptions`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `line` int(11) NOT NULL,
  `trace` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `query` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `cookies` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `headers` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of laravel_exceptions
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2016_01_04_173148_create_admin_tables', 1);
INSERT INTO `migrations` VALUES (4, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (5, '2019_12_14_000001_create_personal_access_tokens_table', 1);
INSERT INTO `migrations` VALUES (6, '2017_07_17_040159_create_exceptions_table', 2);
INSERT INTO `migrations` VALUES (7, '2022_02_01_181446_create_supports_table', 3);
INSERT INTO `migrations` VALUES (8, '2022_02_01_181916_create_ads_table', 4);
INSERT INTO `migrations` VALUES (9, '2022_02_01_182234_create_disconuts_table', 5);
INSERT INTO `migrations` VALUES (10, '2022_02_01_213038_create_platforms_table', 6);
INSERT INTO `migrations` VALUES (11, '2022_02_01_213328_create_news_table', 7);
INSERT INTO `migrations` VALUES (12, '2022_02_01_213733_create_rules_table', 8);
INSERT INTO `migrations` VALUES (13, '2022_02_01_214025_create_pays_table', 9);
INSERT INTO `migrations` VALUES (14, '2022_02_01_214207_create_pays_table', 10);
INSERT INTO `migrations` VALUES (15, '2022_02_01_214435_create_Ads_table', 11);
INSERT INTO `migrations` VALUES (16, '2022_02_01_214606_create_orders_table', 12);

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `platform_id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '標題',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '文字內容',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES (3, 2, '道路救援服務', '<h2>服務流程規範</h2>\r\n\r\n<p>申告本服務時，請如實填寫申告內容，後續由服務人員與您確認身分並預報抵達時間、服務車車號、服務人員姓名。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>服務範圍</h2>\r\n\r\n<p>係於臺灣本島、澎湖、金門及馬祖地區且救援車輛所能行駛及作業之道路者，皆可享有本服務。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>收費標準</h2>\r\n\r\n<ul>\r\n	<li>平面道路 10公里內1,200元，超過每公里加收50元</li>\r\n	<li>高速公路(不含國五) 依高工局規定，視新車價與車身高度不同，10公里內1,500~20,000元</li>\r\n</ul>\r\n\r\n<h2>服務流程規範</h2>\r\n\r\n<p>申告本服務時，請如實填寫申告內容，後續由服務人員與您確認身分並預報抵達時間、服務車車號、服務人員姓名。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>服務範圍</h2>\r\n\r\n<p>係於臺灣本島、澎湖、金門及馬祖地區且救援車輛所能行駛及作業之道路者，皆可享有本服務。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>收費標準</h2>\r\n\r\n<ul>\r\n	<li>平面道路 10公里內1,200元，超過每公里加收50元</li>\r\n	<li>高速公路(不含國五) 依高工局規定，視新車價與車身高度不同，10公里內1,500~20,000元</li>\r\n</ul>\r\n\r\n<h2>服務流程規範</h2>\r\n\r\n<p>申告本服務時，請如實填寫申告內容，後續由服務人員與您確認身分並預報抵達時間、服務車車號、服務人員姓名。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>服務範圍</h2>\r\n\r\n<p>係於臺灣本島、澎湖、金門及馬祖地區且救援車輛所能行駛及作業之道路者，皆可享有本服務。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>收費標準</h2>\r\n\r\n<ul>\r\n	<li>平面道路 10公里內1,200元，超過每公里加收50元</li>\r\n	<li>高速公路(不含國五) 依高工局規定，視新車價與車身高度不同，10公里內1,500~20,000元</li>\r\n</ul>\r\n\r\n<h2>服務流程規範</h2>\r\n\r\n<p>申告本服務時，請如實填寫申告內容，後續由服務人員與您確認身分並預報抵達時間、服務車車號、服務人員姓名。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>服務範圍</h2>\r\n\r\n<p>係於臺灣本島、澎湖、金門及馬祖地區且救援車輛所能行駛及作業之道路者，皆可享有本服務。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>收費標準</h2>\r\n\r\n<ul>\r\n	<li>平面道路 10公里內1,200元，超過每公里加收50元</li>\r\n	<li>高速公路(不含國五) 依高工局規定，視新車價與車身高度不同，10公里內1,500~20,000元</li>\r\n</ul>', '2022-02-01 23:24:15', '2022-02-20 17:59:42');
INSERT INTO `news` VALUES (4, 2, '服務最新優惠', '<p>最新優惠2</p>', '2022-02-01 23:24:32', '2022-02-20 18:14:56');
INSERT INTO `news` VALUES (5, 4, '熊貓優惠', '年假免運費', '2022-02-01 23:25:34', '2022-02-01 23:25:34');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `platform_id` int(11) NULL DEFAULT NULL,
  `no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '訂單編號',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '連絡電話',
  `car_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '車牌',
  `addr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '故障地址',
  `status` int(11) NULL DEFAULT 0 COMMENT '0.訂單成立\r\n1.已派司機\r\n2.抵達現場\r\n3.完成服務',
  `disconut_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '優惠代碼',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `car_brand` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '廠牌',
  `car_brand_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '廠牌名稱',
  `car_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '車型',
  `car_type_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '車型名稱',
  `created_at` timestamp(0) NOT NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES (2, 2, '1646036936', '管理者', '0973011400', 'ABC-1234', '244台灣新北市林口區中正路344號', 0, 'DIS1234', NULL, '026', 'DAF', '02606', 'DAF FAG 6x2 大貨車', '2022-02-28 16:29:06', '2022-02-28 16:29:06', NULL);
INSERT INTO `orders` VALUES (3, 2, '1646037390', '管理者', '0973011400', 'ABC-1234', '244台灣新北市林口區中正路344號', 0, 'DIS1234', NULL, '026', 'DAF', '02606', 'DAF FAG 6x2 大貨車', '2022-02-28 16:36:32', '2022-02-28 16:36:32', NULL);
INSERT INTO `orders` VALUES (4, 2, '1646037524', NULL, NULL, '-', NULL, 0, NULL, NULL, NULL, '請選擇廠牌', NULL, '請選擇車型', '2022-02-28 16:39:40', '2022-02-28 16:39:40', NULL);
INSERT INTO `orders` VALUES (5, 2, '1646037789', NULL, NULL, '-', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2022-02-28 16:43:12', '2022-02-28 16:43:12', NULL);
INSERT INTO `orders` VALUES (6, 2, '1646037998', NULL, NULL, '-', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2022-02-28 16:46:42', '2022-02-28 16:46:42', NULL);
INSERT INTO `orders` VALUES (7, 2, '1646038309', NULL, NULL, '-', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2022-02-28 16:51:52', '2022-02-28 16:51:52', NULL);
INSERT INTO `orders` VALUES (8, 2, '1646038559', NULL, NULL, '-', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2022-02-28 16:56:02', '2022-02-28 16:56:02', NULL);
INSERT INTO `orders` VALUES (9, 2, '1646038587', NULL, NULL, '-', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2022-02-28 16:56:30', '2022-02-28 16:56:30', NULL);
INSERT INTO `orders` VALUES (10, 2, '1646038887', NULL, NULL, '-', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2022-02-28 17:01:30', '2022-02-28 17:01:30', NULL);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for pays
-- ----------------------------
DROP TABLE IF EXISTS `pays`;
CREATE TABLE `pays`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `platform_id` int(11) NOT NULL,
  `enable` int(11) NOT NULL DEFAULT 1,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pays
-- ----------------------------
INSERT INTO `pays` VALUES (1, 2, 1, '<ul>\r\n	<li>\r\n	<h3>新車會員免費權益</h3>\r\n\r\n	<p>－車輛因品質問題以致無法行駛，自車輛故障地點起算，拖至HONDA最近授權經銷商維修廠 30 公里內免費，超過部份每公里加收 50 元，由您自付。</p>\r\n\r\n	<p>－如為非品質因素所致之拖吊費用則由您自付。</p>\r\n	</li>\r\n	<li>\r\n	<h3>非新車會員免費權益</h3>\r\n\r\n	<p>10公里內拖吊費用-- 一般道路1500元、國道1500元( 夜間及假日加收500元 )、每超過一公里加收50元，特殊處理作業之收費則另計之。 ( 若有費用標準變更時, 係依高公局公佈為準 )</p>\r\n	</li>\r\n</ul>', '2022-02-02 12:05:23', '2022-02-28 16:17:36');
INSERT INTO `pays` VALUES (2, 2, 1, NULL, '2022-02-02 12:44:54', '2022-02-02 12:44:54');

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `last_used_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `personal_access_tokens_token_unique`(`token`) USING BTREE,
  INDEX `personal_access_tokens_tokenable_type_tokenable_id_index`(`tokenable_type`, `tokenable_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for platforms
-- ----------------------------
DROP TABLE IF EXISTS `platforms`;
CREATE TABLE `platforms`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '名稱',
  `contact` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '聯絡人',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `enable` int(11) NOT NULL DEFAULT 1 COMMENT '是否啟用',
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '系統編碼',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `code`(`code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of platforms
-- ----------------------------
INSERT INTO `platforms` VALUES (2, 'Honda', 'Sam', '1115-555-555', 1, 'RT166', '2022-02-01 22:18:12', '2022-02-01 23:14:56', NULL);
INSERT INTO `platforms` VALUES (4, '熊貓', 'test', '0973-011-400', 1, 'FFB264', '2022-02-01 22:32:00', '2022-02-01 23:15:34', NULL);

-- ----------------------------
-- Table structure for rules
-- ----------------------------
DROP TABLE IF EXISTS `rules`;
CREATE TABLE `rules`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `platform_id` int(11) NOT NULL,
  `enable` int(11) NOT NULL DEFAULT 1,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rules
-- ----------------------------
INSERT INTO `rules` VALUES (1, 2, 1, '<h2><strong>服務流程規範</strong></h2>\r\n\r\n<p>申告本服務時，請如實填寫申告內容，後續由服務人員與您確認身分並預報抵達時間、服務車車號、服務人員姓名。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2><strong>服務範圍</strong></h2>\r\n\r\n<p>係於臺灣本島、澎湖、金門及馬祖地區且救援車輛所能行駛及作業之道路者，皆可享有本服務。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2><strong>收費標準</strong></h2>\r\n\r\n<ul>\r\n	<li>平面道路 10公里內1,200元，超過每公里加收50元</li>\r\n	<li>高速公路(不含國五) 依高工局規定，視新車價與車身高度不同，10公里內1,500~20,000元</li>\r\n</ul>', '2022-02-02 11:41:07', '2022-02-20 17:15:54');
INSERT INTO `rules` VALUES (2, 2, 1, '<ol>\r\n	<li>測試服務條款</li>\r\n	<li>第二筆測試</li>\r\n	<li>第三筆測試</li>\r\n</ol>', '2022-02-02 11:53:44', '2022-02-02 11:53:44');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
